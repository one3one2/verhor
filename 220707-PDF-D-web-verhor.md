# Projet-Evasions: Wie die Polizei verhört und wie wir uns dagegen verteidigen können

Ein Verhör ist kein friedlicher Austausch zwischen zwei Menschen. Es ist ein Konflikt.

# Einführung

Comprendre pour se défendre / Unsere Unwissenheit macht sie stark.

Dieser Satz fasst sehr gut zusammen, worauf ein Polizeiverhör basiert: auf unserer Unkenntnis. Unkenntnis darüber, wie die Polizei arbeitet, Unkenntnis über die zur Anwendung kommenden Manipulationsstrategien, Unkenntnis über den juristischen Rahmen und schlussendlich, die Unkenntnis über unsere Verteidigungsmöglichkeiten. Ein Verhör ist kein friedlicher Austausch zwischen zwei Individuen, die sich ebenbürtig sind. Es ist ein Konflikt. Im Gegensatz zu einem physischen Konflikt, bei dem eine Person ihre eigene Kraft nutzt um die andere Person anzugreifen, wird die Polizei bei einem Verhör unsere Schwächen gegen uns verwenden und uns damit angreifen.

Zu unseren Schwächen zählen die Informationen, die wir beim Verhör liefern und aus denen sich die Kraft der Polizei und der Justiz zusammensetzt, um uns unter Druck zu setzen – sei es um ihre Strategien und Manipulationen für zukünftige Verhöre zu stärken oder in Form von Beweisen und Indizien vor dem Gericht. Hier kommen wir zu einem zentralen Punkt, um zu verstehen, wie wir uns verteidigen können: Um ihre Arbeit gut machen zu können, ist die Polizei auf unsere Mitarbeit beim Verhör angewiesen.

Mit der Zeit habe ich etwas festgestellt: Die Mehrheit der Menschen mit denen ich mich ausgetauscht habe, die der Polizei Informationen lieferten und somit der Polizei ermöglichten, ihre Arbeit zu tun, fühlten sich nicht von der Polizei ausgenutzt. Vielmehr denken sie, sie hätten gar nichts Wichtiges gesagt, hätten nur von sich gesprochen, müssten sich nichts vorwerfen oder glauben sogar, es wäre ihnen gelungen die Polizei anzulügen und zu verarschen.

Darum geht es in diesem Buch: Die beste Verteidigung bei einem Polizeiverhör ist es, sich durch Schweigen zu weigern daran teilzunehmen.  Das ist etwas, was ich auf den kommenden Seiten häufiger wiederholen werde. Aber diese Wiederholung ist nötig, wieder und wieder: Weil die Polizei, auf der Gegenseite, ein ganzes Arsenal an Techniken und Strategien zur Verfügung hat, um unsere Schwächen auszunutzen, sowie die Möglichkeit uns einzusperren um uns zu ermüden und zu brechen. Dazu kommt der gesellschaftlich verankerte Glaube, wir MÜSSTEN antworten, wenn die Polizei oder andere Autoritätsfiguren Fragen stellen.

**Um ihre Arbeit gut machen zu können, ist die Polizei auf unsere Mitarbeit beim Verhör angewiesen.**

*Bevor wir nun fortfahren noch ein Hinweis:* Dieses Buch ist nicht als juristischer Führer gedacht

Es richtet sich an ein Publikum aus verschiedenen Ländern, wobei sich die Gesetzgebung oftmals unterscheidet. Diese Unterscheidungen in der Gesetzgebung betreffen den Inhalt den ich hier überbringen möchte allerdings sehr wenig und beeinflussen diesen nicht. Die Mechanismen und Strategien eines Verhörs, die von verschiedenen Polizeibehörden entwickelt wurden, haben sich über die Jahre und im Austausch mit anderen Polizeibehörden und Staaten angeglichen. Heutzutage debattieren und verfeinern Inspektor:innen weltweit gemeinsam ihre Manipulationsmethoden in Kongressen, Polizeikolloquien oder in spezialisierten Magazinen.  Nichtsdestotrotz wurden die in den kommenden Seiten vorgestellten und analysierten Techniken und Strategien grösstenteils von der Polizei westlicher Länder entwickelt, weshalb dieses Buch eher die westliche Realität von kapitalistischen Demokratien reflektiert.

*Und noch ein zweiter Hinweis:* Dieses Buch zeigt auf was die Polizei im Bezug auf Verhörstrategien lernt und entwickelt.

Dies beschreibt eine grundsätzliche Praxis und keine detaillierte Beschreibung wie sich deine Erfahrung im Falle einer Konfrontation mit der Polizei tatsächlich abspielen wird. Was Inspektor:innen lernen wird nicht immer genau das sein, was sie auch umsetzen werden. Trotzdem kann davon ausgegangen werden, dass es in groben Zügen dem ähnelt, was hier beschrieben wird.

*Der hier präsentierte Inhalt, setzt sich vorallem aus folgenden Quellen zusammen:*

- Meine persönlichen Erfahrungen und die Erfahrungen meines nahen Umfeldes durch Verhöre und Haft die wir selber erlitten haben. 
- Polizeiliteratur und forensische Literatur, insbesondere Unterlagen der Polizeiakademie, spezifische Magazine oder Sachbücher von Inspektoren geschrieben (alle Werke die mir in die Hände kamen, wurden von Männern verfasst).
- Studium und Analyse von konkreten Repressionsfällen, sowie laufende oder abgeschlossene Ermittlungsakten

### Zur verwendeten Sprache

Auch wenn die Polizei eine Institution ist, die auf zu tiefst patriarchalen Mitteln (Bestrafung, Zwang, Kontrolle und Überwachung) aufbaut und ein patriarchales System verteidigt, rekrutieren die meisten Polizeieinheiten auch gezielt Frauen. Um nicht die Dominanz des generischen Maskulins (die Verwendung rein männlicher Bezeichnungen für Menschengruppen) über die anderen Genderidentitäten zu reproduzieren, verwende ich in diesem Text eine gendersensible Sprache. Falls du das nicht gewohnt bist, mag das auf den ersten Blick komisch erscheinen. Aber du wirst sehen, dass du dich schnell daran gewöhnst. In der deutschen Sprache wird nicht nur das generische Maskulin verwendet und somit das männliche über das weibliche gestellt, sondern gleichzeitig der Welt eine gewaltvolle Binarität auferlegt. Nichts existiert ausserhalb der beiden Kategorieren “männlich” und “weiblich”. Die Anwendung des: (wie zum Beispiel beim Inspektor:innen) soll ein Versuch sein, die männliche und binäre Dominanz in der deutschen Sprache zu brechen.

Am Ende des Buches findest du ein Lexikon für die technischen Begriffe. Im Text werden diese Begriffe bei der ersten Erwähnung unterstrichen.

### Zur Polizei

Dieses Buch ist als Werkzeug zur Selbstverteidigung gegenüber polizeilichen Verhören gedacht. Es wurde aus einer anarchistischen Perspektive geschrieben. Ich stehe ein für die Idee, dass jede Autorität illegitim ist und einem freien Leben, das nach den Bedürfnissen und Lüsten von jedem Individuum definiert wird, im Wege steht.  Dementsprechend ist auch die Polizei eine essentielle Struktur, auf die sich autoritäre Systeme stützen.

In allen Epochen in denen es sie gab, war es die Polizei, die mit Gewalt, alle Versuche für radikale und emanzipatorische Veränderungen, unterdrückte. Die Polizei und die Justiz sind bis tief in ihre Fundamente reaktionäre und anti-emanzipatorische Institutionen. Wo Menschen angesichts von Bedrohungen, die sie betreffen, Selbstverteidigung[^Zum Thema Selbstverteidigung als emanzipatorische Praxis: Elsa Dorlin, Selbstverteidigung: Eine Philosophie der Gewalt, Suhrkamp Verlag, 2020] praktizieren wollen, entwaffnet der Staat sie und drängt sich als - meist wirkungsloser - Beschützer auf. Wenn Menschen, die von einem Konflikt oder einer Unterdrückung betroffen sind, nach eigenen Lösungen suchen, drängt sich die Justiz als Schiedsrichter auf und beansprucht das Recht, allein über die Lösungen zu entscheiden.

Durch die soziale Funktion der Polizei setzt der Staat auf Kontrolle, Bestrafung und Abhängigkeit von staatlichen Institutionen. Dies verhindert die Kreation von anderen Dynamiken die sich z.B. auf Vertrauen, Autonomie und Transformation stützen. Die Polizei und Justiz sind nicht nur eine ungenügende Antwort auf Agressionen und Unterdrückung unter Menschen, sondern reproduzieren und nähren diese auch. Deshalb geht es nicht darum die Polizei zu bekämpfen, um eine alternative Art von Autorität zu bevorzugen (mafiöse Führungspersonen, Unterdrücker:innen und Angreifer:innen, autoritare politische Strukturen), sondern alle Arten von Autoritäten zu bekämpfen.

# Vor dem Verhör

Dieses Kapitel zeigt die Bedeutung des Verhörs, im Bezug auf den ganzen Prozess der Justiz, auf und was dabei auf dem Spiel steht.

## 1. Kontexte eines Verhörs

Es gibt verschiedene Kriterien, die den Ablauf eines Polizeiverhörs beeinflussen. Das erste Kriterium ist das Land in dem du dich befindest. Die Polizei hat nicht überall die gleichen gesetzlichen Rahmenbedingungen und Spielräume. Ein weiteres Kriterium ist die Ebene der zu verhandelnden Tat. Ist es ein leichtes Vergehen oder fällt es möglicherweise unter ein Antiterrorgesetz? Vieleicht lassen die Ermittler:innen die Anklage fallen oder aber sie nehmen diese im Gegenteil sehr ernst, möglicherweise auch, weil die Ermittler:innen unter Druck ihrer Vorgesetzten stehen. Wichtig ist auch der Fakt, dass sich die verhörenden Polizeikräfte persönlich betroffen fühlen können, wenn es sich beim Fall z.B. um Gewalt gegen Polizeibeamt:innen im Rahmen einer Demonstration handelt, während ihnen der Kassendiebstahl in deiner Firma wahrscheinlich emotional nicht sehr nahe geht. All diese Kriterien, sowie auch die Launen der verhörenden Polizeibeamt:innen oder deren Erfahrungen werden den Verlauf des Verhörs beeinflussen. Deshalb kann ein Polizeiverhör genauso ein langweiliger administrativer Moment sein wie eine Situation von enormer Anspannung.

Ein weiteres nicht zu vernachlässigendes Kriterium ist der unterschiedliche soziale Status. Ob du als Mann, Frau oder Transperson identifiziert wirst, ob du weiß bist oder BIPoC[^BIPoC ist die Abkürzung von Black, Indigenous, People of Color und bedeutet auf Deutsch Schwarz, Indigen und der Begriff People of Color wird nicht übersetzt. All diese Begriffe sind politische Selbstbezeichnungen. Das bedeutet, sie sind aus einem Widerstand entstanden und stehen bis heute für die Kämpfe gegen diese Unterdrückungen und für mehr Gleichberechtigung. ], ob du aus einer wohlhabenden oder armen sozialen Schicht stammst, usw. Durch systemische Unterdrückung machen Machtstrukturen bestimmte Körper verletzlicher als andere. Das ist der Vorteil der Person, die sich in die Normen der Gesellschaft einfügt: Nicht die tägliche mentale Last der Diskriminierung tragen zu müssen. Diese Verwundbarkeiten können auch bei der Konfrontation, die ein Verhör darstellt, eine Rolle spielen. Rassismus, Islamophobie, Transphobie, Sexismus oder andere Formen von Unterdrückung zu erleben, kann die psychische Belastung deutlich erhöhen, welche mit einem solchen Moment wie einem Verhör verbunden ist.

Nicht alle Menschen sind der Polizei und dem Justizapparat gegenüber gleichgestellt.  Die herkömmlichen gesellschaftlichen Hierarchien werden ohne Überraschung im Verhalten der Polizei und in der gesamten Justiz reproduziert. Spoilerwarnung: die Institutionen der Polizei reproduzieren die strukturelle und systematische Gewalt in Form von z.B.  Rassismus, Sexismus und Homophobie. Die Chance ist also groß, dass sich die Polizeikräfte die du antreffen wirst rassistisch, antisemitisch, sexistisch, homophob und transfeindlich verhalten werden.  Warum? Weil die Gesellschaften, die die Polizeikräfte verteidigen, strukturell rassistisch, antisemitisch, sexistisch, homophob und transfeindlich sind und es demnach logischerweise Menschen zur Polizei hinzieht, die sich mit diesen gesellschaftlichen Ansichten identifizieren können.[^Ein Beispiel unter vielen, das Rassismus, Antisemitismus und Sexismus innerhalb der Polizei aufzeigt, findet ihr im Podcast Gardien de la paix von Arte Radio (auf französisch). Dieser Podcast enthüllt einen Whatsapp Chat zwischen mehreren Polizeibeamt:innen in dem white supremacy verteidigt wird. In den zwei letzten Jahren wurden weitere Fälle von Zusammenschlüssen rechtsextremer Polizeibeamt:innen publik gemacht. Zum Beispiel wurde, im Zuge der Entdeckung, dass rund zwanzig Polizist:innen der Eliteeinheit der Frankfurter Polizei mit der Neo-Nazi-Szene verbunden sind, diese Einheit aufgelöst.  2021 haben Teile der Elitepolizei Zürich und Basel an einem Schießtraining teilgenommen, dass durch Mitglieder von Neo-Nazi-Gruppierungen in Deutschland organisiert wurde, was zu einer parlamentarischen Interpellation führte.]

**Demzufolge gibt es einen zusätzlichen Druck auf gewisse Personengruppen, während eines Verhörs oder einer Konfrontation mit der Polizei und dies kann zu einer erheblich höheren mentalen Belastung führen.**

Schließlich spielen auch die Umstände, unter welchen du festgehalten wirst, eine Rolle dabei, wie du das Verhör bewältigen kannst.  Dein emotionaler Zustand wird sicherlich nicht derselbe sein, ob du auf der offenen Straße voller Adrenalin in Gewahrsam genommen wirst oder ob du einige Tage im Voraus eine Vorladung per Post erhalten hast. Bei einer Hausdurchsuchung mitten in der Nacht plötzlich aus dem Schlaf gerissen und verhört zu werden, kann eine verstörende Orientierungslosigkeit auslösen, vorallem wenn du dich gerade in der Tiefschlafphase befunden hast. Ähnlich kann es dir ergehen, wenn du mehrere Stunden oder Tage in Untersuchungshaft verbringen musst. Beides kann deine Fähigkeit zum Widerstand erheblich schwächen und dein Reaktionsvermögen einschränken.  Umgekehrt kann es dich stärken dich zu verteidigen, wenn du Kenntnisse über die polizeilichen Abläufe und Verhörstrategien hast.  All diese Faktoren umreißen den Kontext, in welchem dein Verhör stattfindet.

## 2. Funktionsweisen der Justiz

- Richter:in
- Staatsanweltschaft
- Polizei

Um zu verstehen, welche Bedeutung das Verhör im juristischen Prozess spielt, ist es wichtig die Rolle der Polizei im Justizapparat zu verstehen. In den meisten Ländern setzt sich die Justiz aus drei Akteuren zusammen: der Polizei, der Staatsanwaltschaft[^Je nach Land kann dieser Name anders sein.] und den Richter:innen. Jede dieser Institutionen hat eine eigene Funktion und einen anderen Platz in der Hierarchie des Justizapparates.

*Polizei:* Die Polizei ist Hauptakteurin als Sicherheitsbeauftragte im Sinne des staatlichen Ordnungsrechts[^Dieser Polizeibegriff beschreibt alle professionellen Arbeiten, die mit dem Ziel geführt werden, die momentane Ordnung (definiert durch die Verfassung, Gesetze und Reglemente) zu verteidigen, beschützen, aufzudrängen und zu erhalten. An der Seite der Polizei stehen auch weitere Sicherheitsbeauftragte wie private Sicherheitsfirmen, Nachrichtendienste oder auch die forensischen Psychiatrien und Gefängnisverwaltungen.]. Die Aufgabe der Polizei ist die Aufrechterhaltung der Staatsordnung und die Überwachung von potentiell kriminellen Menschen. Zusätzlich sammelt die Polizei Informationen für die Gerichte. Diese Informationen dienen später dem Gericht dazu, zu urteilen ob eine Person ein Gesetz gebrochen hat und mit welchem Urteil die Person bestraft wird. In diesem Prozess befindet sich die Institution Polizei auf der untersten Stufe der Hierarchie, auf die Feldarbeit verbannt, um Informationen zu sammeln. Die Polizist:innen erarbeiten ein Ermittlungsakte mit möglichst vielen Informationselementen, um ein möglichst breites und genaues Bild der Taten, ihren Abläufen, dem Kontext, den beteiligten Personen und ihren Absichten, Rollen und Zielen abzubilden.

Wenn die Polizei zum Schluss kommt, kein weiteres Material mehr sammeln zu können wird das Dossier abgeschlossen und der Staatsanwaltschaft übergeben. Eine Ermittlungsakte, die nicht genügend gefüllt ist, steht für eine schlechte Arbeit der Polizei. Dies zeigt auf, dass die Ermittlung sowie das Verhör nicht effizient genug war, um einem:r Richter:in zu erlauben ein Urteil zu fällen. Dies wiederum ist positiv für die Person auf der Anklagebank.

*Staatsanwaltschaft:* Sobald die Ermittlungsakte geschlossen ist, wird sie der Staatsanwaltschaft übergeben. Diese muss dann erörtern, ob die Ermittlungsakte genug Elemente für eine Verurteilung / Gerichtsurteil enthält. Je nach Land kann die Staatsanwaltschaft bei leichteren Delikten direkt ein Gerichtsurteil vorschlagen, ohne dass der Fall vor Gericht verhandelt wird. Basierend auf der Akte wird der beschuldigten Person in diesem Fall eine Strafe vorgeschlagen, welche die Person akzeptieren oder dagegen Einspruch einlegen kann. Bei einem Einspruch, wird der Fall vor Gericht gebracht. Diese Praxis wurde eingeführt, um die Gerichte zu entlasten.

Der:die Staatsanwält:in kann sich auch entscheiden selbst ein Verhör zu führen, um einen präziseren und direkteren Eindruck zu gewinnen, als nur durch die Lektüre der Ermittlungsakte. Sie:er könnte versuchen, neue Informationen zu bekommen und deine zukünftige Verteidigungstaktik, die du bei der Gerichtsverhandlung anwenden wirst, zu erahnen. Falls die Staatsanwaltschaft die Ermittlungsakte als unvollständig erachtet, kann sie den Fall entweder ablegen oder die Akte zur Polizei mit dem Auftrag zurückschicken, die Informationen zu vervollständigen. Oft arbeitet die Staatsanwaltschaft schon während der Ermittlung mit den Polizeibeamt:innen zusammen, um die Ermittlung in eine gewünschte Richtung zu lenken oder um spezifische Methoden (wie Überwachungsmaßnahmen, Hausdurchsuchungen, Ausweitung des Falles auf andere laufende Fälle, usw.) anzuordnen.

*Richter:in:* Wenn die Staatsanwaltschaft beschließt, dass die Akte, auch ihrer Ansicht nach, ausreichend Informationen enthält, wird es dem Gericht übergeben, wo ein:e Richter:in sich dem Fall annimmt und einen Prozess vorbereitet. Erst zu diesem Zeitpunkt hast du die Möglichkeit, deine Ermittlungsakte einzusehen und zu erfahren, welche Informationen im Prozess gegen dich angewendet werden können.

Während dem Prozess wird sich die:der Richter:in (oder die Jury je nach Land) für das Urteil auf die Ermittlungsakte stützen und wird dich, sowie gegebenenfalls Mitangeklagte und / oder Zeug:innen erneut verhören. Das Urteil wird anhand der Gesetze, Rechtsprechung und anhand des Kontextes des jeweiligen Falles (und anhand der Tagesform des / der Richter:in) gefällt. Je nach Land ist es möglich, das Gerichtsurteil anzufechten und den Fall neu zu verhandeln. Daraufhin wird das Ermittlungsdossier einem anderen Gericht übergeben. In der Zeit bis zur erneuten Verhandlung, können dem Dossier neue Elemente zur Verteidigung und zur Anklage hinzugefügt werden.

**Die Polizei fällt nicht das Urteil ob du schuldig oder unschuldig bist. Dies ist weder in ihrem Zuständigkeitsbereich, noch haben sie die Macht dazu.**

Dieser Ablauf des Justizapparates zeigt zwei Aspekte auf, die wichtig sind um den Stellenwert des Verhörs innerhalb der Anklage zu verstehen. 

- Die Arbeit der Polizei ist es, eine Ermittlungsakte mit so vielen Informationen wie möglich zu füllen. Diese Informationen werden vorwiegend durch Verhöre gesammelt. 
- Die Polizei fällt nicht das Urteil ob du schuldig oder unschuldig bist. Dies ist weder in ihrem Zuständigkeitsbereich, noch haben sie die Macht dazu.

Ein Fehler den ich oft beobachtet habe, ist, dass Menschen während des Verhörs versuchen die Polizei von ihrer Unschuld zu überzeugen, in der Hoffnung sich aus dem Fall rauswinden zu können. Und dies ist genau die Falle, in die sie tappen.

Das Bedürfnis sich zu erklären, Entschuldigungen und Lügen zu finden - kurz: das Bedürfnis die Polizei von einer gewissen Version des Tatherganges zu überzeugen - führt zur Mitarbeit mit der Polizei. Es werden Antworten (ob Lüge oder Wahrheit) gegeben, Erklärungen (falsche oder richtige) geliefert und Halbwahrheiten zur Verfügung gestellt. Alles Elemente, die der Polizei erlauben ihre Arbeit zu machen: Verhöre zu führen, Nachforschungen anzustellen, Erklärungen der verhörten Personen zusammenführen, Analysen und Hypothesen zu konstruieren, mit denen zukünftige Nachforschungen neu ausgerichtet werden können. Es gehört nicht zur Aufgabe der Polizei über deine Schuld oder Unschuld zu urteilen.

Wenn eine Ermittlungsakte eröffnet wird, wird sie entweder die hierarchische Leiter hochgereicht um einen Prozess zu eröffnen oder wenn zu wenig Hinweise gesammelt werden konnten, ohne Folge zu den Akten gelegt. Falls du eine:n Akteur:in im Prozess von deiner Unschuld überzeugen willst, spare dir das alleine für den:die Richter:in in Anwesenheit deines:r Anwält:in auf. Alles andere bringt dich in Gefahr.

\[Polizei-Zitat\]

> Wenn du eine Verhaftung durchführst, hast du grundsätzlich ein Minimum an Beweisen. Diese Beweise reichen jedoch oft nicht aus, um den Menschen anzuklagen. Zudem verlangt die Anklage mindestens eine Anhörung der beschuldigten Person, sofern dies möglich ist.[^Zeug:innenenaussage von Verhörenden, gesammelt von Diane Boszormenyi, für ihre Arbeit Der Einfluss polizeilicher Verhörtechniken auf den Wert eines Geständnisses. Studie im Lichte der Theorie der drei Dimensionen der öffentlichen Gewalt von Monjardet, Rechts- und Kriminologiefakultät, katholische Universität Louvain, 2019. Alle Zitate von Polizist :innen sind aus dieser Studie entnommen und werden daher nicht mehr referenziert. Sie werden nur mit dem Piktogramm \[Polizei-Zitat\] gekennzeichnet.]

### Unschuldsvermutung

Die Unschuldsvermutung ist ein Grundprinzip nachdem jede Person, die verdächtigt wird, einen Gesetzesbruch begangen zu haben, als unschuldig zu betrachten ist, solange ihre Schuld nicht juristisch bewiesen ist.

Da in den meisten Fällen einzig der:die Richter:in die Autorität hat über die Schuld oder Unschuld eines Individuums zu entscheiden, bedeutet dies, dass du erst in dem Moment juristisch schuldig bist, wenn ein:e Richter:in innerhalb eines Prozesses dieses Urteil fällt. Davor bist du lediglich angeklagt und somit verdächtigt einen Gesetzesbruch begangen zu haben. Dieses Konzept basiert auf Artikel 11 der Menschenrechtskonvention von 1948 der UNO und wird dort folgendermassen formuliert:

> “1. Jeder, der einer strafbaren Handlung beschuldigt wird, hat das Recht, als unschuldig zu gelten, solange seine Schuld nicht in einem öffentlichen Verfahren, in dem er alle für seine Verteidigung notwendigen Garantien gehabt hat, gemäß dem Gesetz nachgewiesen ist.(…)”

Heutzutage haben von Russland über die USA und Frankreich, bis zum Iran fast alle Staaten auf die eine oder andere Weise diesen Artikel in ihre Strafgesetzbücher und Verfassungen integriert. Auf welche Art die Länder sich daranhalten ist Bestandteil von Interpretationen. Konkret bedeutet dies, dass der Staat (die Staatsanwaltschaft und Polizei) die Aufgabe hat, deine Schuld zu beweisen und nicht du deine Unschuld beweisen musst. Die Arbeit der Polizei ist es, deine Schuld (oder die anderer) nachzuweisen. Und jedes Element, jede Information die du ihnen gibst, hilft ihnen mit ihrer Arbeit voranzukommen.

\[Polizei-Zitat\]

> “Bevor wir mit der Anhörung beginnen, haben wir meistens schon einen Inhalt, der sich am Verdacht orientiert, darum ist es ein Stück weit so, dass du dir, wenn du eine Anhörung startest schon sagst: “Er ist der Schuldige.”. Aber wir werden trotzdem das Prinzip der Unschuldsvermutung respektieren, weil wir ihm das Recht zusprechen, nicht nur der Verdächtige zu sein. Aber in unserem Kopf sind wir, in dem Moment in dem wir mit ihm sprechen, schon beim Verdacht und den Indizien.”

\[Polizei-Zitat\]

> “Die Unschuldsvermutung ist eine juristische Wahrheit und nicht die Wirklichkeit des Feldes. Sobald ich im Besitz von wichtigen Indizien bin, verhalte ich mich so als ob er schon schuldig wäre, das muss ich zugeben, ja. Das hindert mich nicht daran, respektvoll und korrekt zu sein, aber sicher ist er höchstwahrscheinlich schuldig. Aber wenn wir auch nur ein wenig Zweifel haben, arbeiten wir auch in die andere Richtung. Wir können in beide Richtungen verhören, das machen wir: 95 % zu Lasten und 5 % zur Entlastung. Die Unschuldsvermutung hat kein Interesse für die Polizeiarbeit. Andere Dinge schon: die Rechte zu respektieren, das Recht auf Integrität der Person, aber für die Unschuldsvermutung gibt es kein pragmatisches Interesse. Ein rechtliches Interesse, aber nichts mehr.”

[Box]

> **Die Parallelkonstruktion**
> 
> Stell dir vor, ein von der Polizei rekrutierter Informant erzählt der Polizei von zwei Menschen, die eine Straftat begangen haben.
> 
> Daraufhin veranlasst die Polizei Hausdurchsuchungen bei den beiden Menschen, findet belastende Indizien und nimmt deswegen das Duo in Untersuchungshaft. Im Umfeld der beiden werden Telefone abgehört, infolgedessen die Polizei rausfindet, dass eine dritte Person bei der Straftat involviert war. Allerdings wird in der ganzen Eile keine richterliche Verfügung (oder staatsanwaltschaftliche Verfügung, je nach Land) zur Bewilligung der Abhörung beantragt. Bei den Verhören verleitet die Polizei die Beiden dazu, die Identität der dritten Person zu verraten, ohne ihnen mitzuteilen, dass sie schon um die Existenz dieser Person wissen.
> 
> Nach dem Abschluss der Ermittlung möchte die Polizei nicht preisgeben, dass sie illegal Telefone abgehört hat und dass sie über einen Informant verfügt (welcher der Polizei ja in Zukunft auch noch von Nutzen sein kann). Also wird die Polizei die Akte abändern und diese beiden Informationen verschwinden lassen. Demnach werden zwei Dossier erstellt. Das erste, in dem der ganze, wahre Ablauf der Ermittlungen steht, bleibt in den Büros der Polizei. Im zweiten Dossier, das extra für die Nutzung im Gerichtsprozess geschrieben wird, werden die sensiblen Daten durch “öffentliche “ Informationen ersetzt. Die Existenz des Informanten wird verschwiegen, für die Hausdurchsuchungen wird ein anderes Motiv genannt und das Wissen über die dritte Person wird so konstruiert, als wäre dies dank der Antworten während den Verhören klar geworden und nicht dank den illegalen Lauschangriffen.
> 
> Diese Praxis nennt sich Parallelkonstruktion (aus dem englischen von Parallel Construction). Dies ist eine Handhabung die im Dunkeln gehalten wird und keine Polizeieinheit
> 
> kommuniziert offiziell darüber. Nichtsdestotrotz wurden dank den Nachforschungen von Investigativjournalist:innen mehrere Fälle von Parallelkonstruktionen publik gemacht[^Siehe Human Rights Watch Repport “US: Secret Evidence Erodes Fair Trial Rights” Januar 2018]. Die Mehrheit der interviewten (Ex-)Polizist:innen haben erklärt, dass dies eine häufig genutzte Methode ist und haben diese mit der Begründung verteidigt, dass sie notwendig sei, um die Arbeit der Polizei effizient zu gestalten. Obwohl der größte Teil der medial bekannt gewordenen Fälle in den USA stattgefunden haben, kann meiner Meinung nach davon ausgegangen werden, dass diese Methode überall angewendet wird. Sei es aus persönlicher Initiative von Ermittler:innen oder systematisch und etabliert von der ganzen Institution.
> 
> Wie dem auch sei, das Verhör ist ein praktisches Werkzeug, um Löcher im Dossier zu stopfen oder Quellen zu verstecken. Informationen, die der Polizei bereits bekannt sind können rein gewaschen werden, indem die verhörten Personen dazu gebracht werden, dieselbe Information nochmals zu äußern, womit die eigentliche Quelle versteckt werden kann.


## 3. Der Ablauf einer Anklage

Ausgangspunkt jeder Ermittlung, ist ein mutmaßlicher Gesetzesverstoß, zu dem die Polizei Informationen sammelt. Sobald eine Untersuchung eingeleitet wird, werden ihr Straftaten zugeordnet (z. B. Hausfriedensbruch, Sachbeschädigung, Hehlerei usw.). Die Polizei wird anschließend versuchen, die Verantwortung für diese Straftaten einzelnen Personen zuzuweisen. Im Laufe der Ermittlungen können die Straftaten angepasst werden (was als Ermittlung wegen Hausfriedensbruchs begann, kann sich zu Einbruch oder Diebstahl entwickeln). Es kommt immer wieder vor, dass bei einer Untersuchung neue Straftaten aufgedeckt werden und somit neue Untersuchungen eingeleitet werden. Wenn diese Untersuchungen Gemeinsamkeiten aufweisen (z. B. mehrere Einbrüche, die der gleichen Gruppe zugeordnet werden können), können diese verschiedenen Ermittlungen als “Ermittlungsnetz” oder “parallele Ermittlungen” behandelt werden. Die verschiedenen

beteiligten Polizist:innen tauschen sich regelmäßig über die jeweiligen Fälle aus. Darüber hinaus, verfügen viele Polizeidienststellen über miteinander verbundene Datenbanken. Wenn ein:e Ermittler:in über jeden neuen Eintrag zu einer Person, einem Gegenstand, einer Waffe oder einem Fahrzeug auf dem Laufenden gehalten werden möchte, kann er:sie eine E-Mail erhalten, sobald ein neuer Eintrag über sein:ihr Interesse in den verschiedenen Datenbanken gemacht wird.

Während der Untersuchung tragen die Inspektor:innen die gesammelten Informationen in der Untersuchungsakte zusammen.

Ziel dieser Akte ist es, ein klares Bild vom Kontext des Falls, den beteiligten Personen, dem Tathergang, den Absichten usw. zu vermitteln.

Wenn die Polizei glaubt, alle möglichen Informationen gesammelt oder alle zur Verfügung gestellten Ressourcen (Zeit und Budget) genutzt zu haben, wird die Akte geschlossen und an die nächsthöhere

Ebene weitergeleitet, wo entschieden wird, ob ein Verfahren eingeleitet werden kann.

Wie die Geheimdienste, betreibt auch die Polizei außerhalb der Ermittlungen Überwachungsarbeit: Sie sammelt, verarbeitet und analysiert Daten über Einzelpersonen, Gruppen, Netzwerke und soziale Zusammenhänge. Diese Daten sind nützlich, um Straftaten aufzudecken und hilfreich bei zukünftigen Ermittlungen. .

### Ermittlungsakte

Die Ermittlungsakte enthält den gesamten Verlauf der Ermittlungen, Beweisstücke, gefundene und analysierte Spuren und materielle Beweise (Fingerabdrücke, Videoüberwachung, DNA, Fußspuren usw.), Zeugenbefragungen und natürlich die Vernehmungsprotokolle. Diese Akten sind oft chronologisch aufgebaut und zeigen, welchen Weg die Inspektor:innen bei ihren Ermittlungen eingeschlagen haben, welche Hypothesen in Betracht gezogen wurden und welche Thesen bestätigt oder verworfen wurden.

Die endgültige Schlussfolgerung bleibt jedoch der Beurteilung der Staatsanwält:in/Richter:innen überlassen. Die Qualität der Polizeiarbeit wird auf der Grundlage dieses Ermittlungsberichtes beurteilt. Das Ziel der Erstellung dieser Akte ist es, ein breites und genaues Bild des Kontextes der Straftat, der beteiligten Personen, der Verbindungen (interindividuelle Zusammenhänge), der Motive, der Verknüpfungen und des Tatherganges zu zeichnen.

Ganz am Anfang eines Rechtsverfahren, wenn du von der Polizei befragt wirst, dich in Polizeigewahrsam oder in Untersuchungshaft befindest, hast du keine Möglichkeit, die Ermittlungsakte einzusehen. Das bedeutet, dass du nur wenig über den Hintergrund der Ermittlungen weißt, über das, was die Polizei interessiert, über die Elemente und Indizien, die sie bereits gesammelt haben und über die Aussagen, die mögliche Mitangeklagte gemacht oder nicht gemacht haben. In diesem Ungleichgewicht liegt die größte Gefahr bei Aussagen. Du kannst nicht wissen, ob du Informationen lieferst, die der Polizei bereits vorliegen oder nicht, ob du den Aussagen einer anderen Person widersprichst, ob die Polizei Hinweise darauf hat, dass du lügst, etc. Unter diesen Umständen ist es einfach nicht möglich, sich für eine wirksame und solide Verteidigungsstrategie zu entscheiden, die über das Schweigen hinausgeht.

Erst wenn der Fall einem:r Richter:in vorgelegt wird, hast du und deine Anwält:innen die Möglichkeit, die Ermittlungsakte einzusehen.

Von diesem Zeitpunkt an, müssen dir alle neuen Elemente, die in die Akte aufgenommen werden, mitgeteilt werden, häufig passiert dies über deine Anwält:innen[^Dies kann sich je nach den rechtlichen Verfahren des Landes, in dem du dich befindest, ändern.]. Wenn du Einsicht in die Ermittlungsakte erhalten hast, weißt du, auf Grundlage welcher Informationen der:die Richter:in den Fall beurteilen wird. So kannst du in voller Kenntnis der Sachlage eine Verteidigungslinie vorbereiten, die dir so wenig wie möglich schadet. Wenn die Ermittlungsakte nur sehr wenig Material enthält, macht es vielleicht sogar Sinn, weiterhin zu schweigen, anstatt das Risiko einzugehen, durch eine geschickte Frage der Richter:in oder der Staatsanwält:innen in eine Falle zu tappen.

### Beweise und Indizien

Indizien sind Informationen, die von der Polizei während einer Untersuchung gesammelt werden. Zum Beispiel:

- Indiz 1: Die Information, dass Herr X einen roten Honda besitzt. 
- Indiz 2: Am Tatort gefundene Reifenspuren, die mit dem Auto von Herrn X übereinstimmen. 
- Indiz 3: Eine Zeugin, die behauptet, einen roten Honda am Tatort gesehen zu haben. 
- Indiz 4: Eine zweite Zeugin gibt an, dass sie den Freitagabend mit Herrn X in einer Bar verbracht hat. 
- Indiz 5: Die Aussagen von Herrn X bei seiner Vernehmung, dass seine Tochter Auto fahren kann, obwohl sie noch keinen Führerschein gemacht hat.

Diese Hinweise werden nun miteinander in Verbindung gebracht und von der Polizei als Hypothesen dargestellt. Wenn die Indizien 1, 2 und 3 zusammengefasst werden, kann die Hypothese aufgestellt werden, dass Herr X mit seinem Auto am Tatort war. Eine andere Hypothese, die auch Hinweis 4 berücksichtigt, würde die These aufstellen, dass das Auto von Herrn X am Tatort war, aber nicht Herr X, da dieser zur gleichen Zeit in einer Bar gesehen wurde. Hinweis 5 könnte schließlich die neue Hypothese aufstellen, dass es die Tochter von Herrn X war, die mit dem Auto ihres Vaters zum Tatort gefahren ist.

Anhand der gesammelten Informationen versucht die Polizei, die Fakten zu ermitteln, indem sie verschiedene Hypothesen aufstellt, die aus einem Bündel von Indizien und übereinstimmenden Elementen bestehen. Bestimmte Elemente können die Hypothesen später entkräften, so dass die Polizei nach dem Ausschlussprinzip arbeitet.

In jedem Fall arbeitet die Polizei ausschließlich mit Indizien. Der:die Richter:in entscheidet dann, welches Indiz als Beweismittel verwendet werden kann, je nachdem, wie der gesetzliche Rahmen aussieht und welche seine:ihre eigene Interpretation von den Fakten ist. Kann diese Zeugenaussage allein als Beweis verwendet werden? Hat das Bild einer Überwachungskamera mehr rechtliches Gewicht als die Aussagen des:r Verdächtigen? Diese und viele andere Fragen werden auf dem Schlachtfeld zwischen Richter:innen, Anwält:innen und Staatsanwält:innen ausgetragen. Letztendlich sind es die Richter:innen, die die Entscheidung treffen, je nach Gesetz, Rechtssprechung, aber auch je nach Laune und Überzeugungen. Wenn die Verteidiger:innen mit der richterlichen Einschätzung nicht einverstanden sind, können sie Berufung einlegen und den Fall von einer höheren Instanz neu verhandeln lassen.

In diesem Kapitel werden zwei wichtige Punkte hervorgehoben.

In einem Gerichtsverfahren geht es nicht darum, ob du schuldig oder unschuldig bist, sondern darum, ob es genügend Beweise gibt oder nicht, um dich für das zu verurteilen, was dir vorgeworfen wird. Auch hier zeigt sich wieder, wie wichtig diese Gleichung ist: Je weniger Material (einschließlich deiner eigenen Aussagen) in deiner Ermittlungsakte enthalten ist, desto besser geht es dir im Prozess.

Die Arbeit der Polizei beschränkt sich darauf, Informationen zu sammeln und diese in Form von Hypothesen darzustellen. Dieser Punkt ist wichtig, weil er den Irrglauben aufzeigt, dass Polizist:innen über deine Schuld oder Unschuld urteilen und dass es von Vorteil sein kann, wenn du versuchst, sie von deiner Unschuld zu überzeugen. Das Bedürfnis, sich gegenüber der Polizei zu erklären und zu rechtfertigen, wird geschickt ausgenutzt, um dir Informationen zu entlocken, die letztlich gegen dich oder andere Personen verwendet werden können.

### Bedeutung des Verhörs in der Ermittlung

Die Bedeutung der Befragung für die Ermittlungen hängt von den jeweiligen Untersuchungen ab. Bei manchen Ermittlungen sammeln die Polizist:innen schnell eine große Menge an Spuren und Indizien (Fingerabdrücke, Überwachungsmaterial, Zeugenaussagen) oder nehmen eine Festnahme auf frischer Tat vor. In diesen Fällen sind die Informationen aus den Vernehmungen für die Lösung der Ermittlungen nicht ausschlaggebend. Bei manchen Untersuchungen machen die Aussagen des:r Verdächtigen nur noch einen geringen Unterschied in der Beurteilung des Falls durch den:die Richter:in. Hier wird die befragte Person wahrscheinlich weniger unter Druck gesetzt, da der Fortschritt der Ermittlungen nicht von ihren Aussagen abhängt.

**Ermittler:innen werden niemals mitteilen, dass sie in ihren Ermittlungen keine brauchbare Informationen gefunden haben.**

Andererseits gibt es Ermittlungen, die nur auf einem geringfügigen Verdacht beruhen, ohne dass es dafür irgendwelche Indizien gibt. Dabei kann es sich um einen einzigen Hinweis handeln, der den Verdacht auf die befragte Person lenkt und sie zu einer Vernehmung bei der Polizei führt. Hier ist es von entscheidender Bedeutung, durch die Befragung Informationen zu erzwingen. Ohne Antworten deinerseits werden die Ermittlungen nicht vorankommen und schließlich eingestellt werden. Dadurch wird der Druck während des Verhörs wahrscheinlich sehr hoch sein.

Stattdessen können sie dich glauben lassen, dass sie viel über dich wissen, um dir ein Gefühl der Überlegenheit zu vermitteln, obwohl ihre Akte in Wirklichkeit fast leer ist. Es gibt kaum etwas frustrierenderes, als den Moment in dem der:die Richter:in, einen Menschen nur aufgrund seiner eigenen Aussagen verurteilt. Dies kommt leider häufig vor.

Die Befragung dient auch dazu, die Ermittlungen in eine bestimmte Richtung zu lenken; Sie kann Hinweise darauf geben, wer überwacht werden muss (Abhören, Beschattung, Hausdurchsuchung) oder welche Spuren an bestimmten Orten zu suchen sind. Wenn die Ermittler:innen durch dein Verhör z. B. die Identität deiner Kompliz:innen erfahren, ist es sehr wahrscheinlich, dass die Wohnungen dieser Personen durchsucht werden. Vielleicht werden auch Werkzeuge gefunden, die zu den Spuren am Tatort passen. So können die Ermittlungen voranschreiten.

### Das Geständnis

*Das Geständnis ist die Königin der Beweise (Polizeisprichwort)*

Bei einem Geständnis erzählt eine Person ihre Version der Ereignisse, ohne nur auf eine gezielte Frage zu antworten. Es ist dieser besondere Moment, in dem ein Mensch Tatsachen zugibt und/ oder gesteht. Ein Geständnis kann vollständig (der:die Befragte gibt alle Informationen preis, die für die Polizei von Interesse sind) oder teilweise (der:die Befragte gibt einen Teil der Fakten zu, während ein anderer Teil verschwiegen oder geleugnet wird) sein.

Dennoch wird einem abgelegten Geständnis, immer auch ein gewisses Misstrauen von Seiten der Polizei und Richter:in entgegengebracht.Eine Person kann lügen, um jemanden zu schützen, oder nur einen Teil der Wahrheit gegenüber den Richter:innen preisgeben, um andere Informationen zu verbergen. Ein Geständnis hat daher kein anderes rechtliches Gewicht als knappe Antworten auf bestimmte Fragen.

Bei der Durchsicht der Kriminalliteratur fallen zwei Strömungen, im Bezug auf die Bedeutung des Geständnisses, bei der Entwicklung von Verhörstrategien, auf. Die klassische und älteste Tendenz stellt das Geständnis in den Mittelpunkt des Verhörs. Das Verhör wird mit dem Ziel aufgebaut den:die Befragte:n zu einem Geständnis zu bewegen, das der Wahrheit der Tatsachen so nahe wie möglich kommt. Die Ermittler:innen überprüfen die Aussagen und kontrollieren die Alibis, um zwischen Geständnis und Lüge unterscheiden zu können. Alle Aussagen der befragten Person werden in diese beiden Kategorien eingeteilt: Geständnis oder Lüge.

**Alle Aussagen der befragten Person werden in diese beiden Kategorien eingeteilt: Geständnis oder Lüge.**

Ein Geständnis setzt voraus, dass die befragte Person seine:ihre Schuld zumindest teilweise einräumt. Eine Theorie dieser Strömung geht davon aus, dass ein verdächtiger Mensch, der ein Geständnis ablegt, in den meisten Fällen damit beginnt, die Fakten und seine Beteiligung durch ein Teilgeständnis herunterzuspielen. Die Ermittler:innen werden daher die Informationen überprüfen, um ein vollständiges Eingeständnis der Fakten zu erzwingen, Element für Element. Dazu drängen sie die befragte Person jedes Detail des Falls zu vertiefen, bis sie genügend Material haben, um die Kohärenz der Aussagen zu überprüfen oder mögliche Widersprüche zu finden, die auf eine Lüge hindeuten. Eine Strategie, um eine Person zu einem Geständnis zu bewegen, besteht darin, eine innere Angst in Form von Schuld oder Scham hervorzurufen. Nach außen gerichtete Ängste wie Wut, Misstrauen oder Verachtung werden die Bereitschaft zu einem Geständnis hingegen einschränken. Strategien wie die “ emotionale Ansteckung “ \[Seite 69\] oder die “ Vermenschlichung der Beziehung “ werden hierbei bevorzugt \[Seite 66\].

Die Polizeibeamt:innen gehen davon aus, dass die verdächtige Person einen Verteidigungsmechanismus anwenden wird, um ihre Taten zu rechtfertigen und ihr Selbstvertrauen zu erhalten. Die Vernehmungsbeamt:innen versuchen, den Widerstand zu brechen, indem sie die psychologischen Schwachstellen des:der Verdächtige:n identifizieren und ausnutzen (Schuldgefühle, Trauer, Stolz, Naivität usw.). Es können auch Faktoren wie Krankheit, Müdigkeit, Stress, soziale Isolation oder Nahrungsentzug genutzt werden.

Die zweite Strömung befasst sich weniger mit dem Geständnis, sondern konzentriert sich mehr auf die Suche nach bestimmten Elementen, die für die Ermittlungsakte benötigt werden. Die Vernehmung steht nicht mehr im Mittelpunkt der Ermittlungen, sondern wird auf die gleiche Ebene wie andere Beweismittel (Sachbeweise, Spurensicherung, Zeugenaussagen) gestellt. Diese Strategien zielen darauf ab, die:den Befragte:n dazu zu bringen, über bestimmte Themen zu sprechen, zu denen die Polizei Informationen benötigt, um ihre Ermittlungen voranzutreiben. Dabei kann es darum gehen, Lügen oder Widersprüche zu sammeln, die der:dem Verdächtige:n zur Last gelegt werden, oder Aussagen zu erhalten, die der Polizei technische Hinweise liefern (Anzahl der beteiligten Personen, interindividuelle Verbindungen, Modus Operandi). Die Strategien des “ Treibsands, good cop, bad cop und Schuldzuweisungen “ werden verwendet \[Seite 60; 57;78\].

Die Untersuchung baut hier in erster Linie auf materiellen Beweisen auf und erst in zweiter Linie auf den Aussagen oder Geständnissen des:r Befragten. Die Strategien zielen darauf ab, die Denk- und Entscheidungsfähigkeit zu schwächen, indem sie die Angst und Unsicherheit der Person erhöhen, insbesondere durch Einsperrung und/oder Isolation.

\[Polizei-Zitat\]

> “Man spürt von Anfang an, ob es möglich ist, ihn zu einem Geständnis zu bringen, aber ohne eine vorgefasste Meinung zu haben, je nach den Elementen und der Sensibilität die man hat. Es geht nicht um Manipulation, aber wir werden versuchen, ihn in die Richtung zu lenken, die wir uns wünschen.”


# Das Verhör

Dieses Kapitel befasst sich mit der spezifischen Praxis des Verhörs: Vorbereitung, Techniken und Strategien.

## 4. Vorbereitung

### Profiling

Vor jeder Vernehmung erstellen die zuständigen Inspektor:innen ein Profil der zu vernehmenden Person. Je nach Bedeutung der Untersuchung, kann dieses Profil sehr detailliert und präzise sein oder im Gegenteil nur aus einigen groben Charakterzügen bestehen.

Um eine Vorstellung davon zu bekommen, wie du dich während der Befragung verhalten könntest, ist jede Information über dich hilfreich: finanzielle Situation, Schulbildung, soziales Umfeld, familiäre und berufliche Beziehungen, Leidenschaften, Empfindlichkeiten und moralische Werte. Wenn du schon einmal mit der Polizei zu tun hattest, werden die Protokolle deiner bisherigen Vernehmungen durchgesehen, um deine Reaktionen vorherzusehen. Wenn du vor deiner Vernehmung festgenommen und in Polizeigewahrsam genommen wurdest, werden die Beamt:innen darauf achten, wie du dich ihnen gegenüber verhältst, wie viel Stress und Angst der Freiheitsentzug in dir auslöst, wie leicht es dir fällt, dich auszudrücken und welche Art von Vokabular du verwendest. Informationen über deinen medizinischen Zustand (Alkoholismus, Drogenmissbrauch, chronische Krankheiten usw.) sind ebenfalls hilfreich, sowohl für die Ermittlungen als auch für das Verhör. Einige Polizeieinheiten bieten psychiatrische Grundausbildungen an, damit die Ermittler:innen in der Lage sind, ein psychologisches Profil des:r Befragten zu erstellen, indem sie psychische Störungen wie Depressionen, bipolare Störungen oder Schizophrenie ausnutzen können.

Du weißt nichts über die Polizist:innen, die dir gegenüberstehen, aber sie haben eine ziemlich genaue Vorstellung davon, wer du bist.

**Das ist das Wesen der Arbeit der Beamt:innen: Informationen zu sammeln, um einen strategischen Vorteil zu erlangen und den:die Gegner:in unter Kontrolle zu bringen.**

### Informationsklassifizierung

*Ich weiß, dass du weißt, dass ich weiß, was du weißt (polizeiliches Denken).*

Im Gegensatz zu dir kennen die Inspektor:innen die Ermittlungsakte, zu der sie dich befragen. Das verschafft ihnen einen nicht zu unterschätzenden Vorteil gegenüber dir. Bei der Festlegung ihrer Verhörstrategie werden die Ermittler:innen ihr Wissen in drei Stufen einteilen.

- Informationen, die an dich weitergegeben werden können/müssen. 
- Informationen, die an dich weitergegeben werden können, wenn du dich dadurch zu einer Gegenleistung veranlasst fühlst. 
- Informationen, die unter keinen Umständen an dich weitergegeben werden dürfen.

Informationen der zweiten Kategorie werden an dich weitergegeben, wenn die Polizist:innen der Meinung sind, dass sie dadurch neue Informationen zurückbekommen. Im Klartext: Wenn sie glauben, dass es dabei hilft, dich zum Reden zu bringen.

Ich habe oft Leute gehört, die behaupten, die Fragen der Polizei mit der Absicht zu beantworten, um Informationen über den Stand der Ermittlungen herauszufinden, ohne selbst welche preis zu geben. Das ist eine Sichtweise, die ich für gefährlich optimistisch halte. Vor allem, wenn bedacht wird, dass die Inspektor:innen sich bemühen, die Informationen aufzulisten, die sie den:r Verdächtigen nicht geben dürfen. Eine ihrer Strategien besteht darin, ein zu großes Selbstvertrauen der Befragten auszunutzen.

\[Polizei-Zitat\]

> “Du musst nicht alles auf einmal preisgeben, was du hast. Du nutzt das, was du hast. Du hast sowas wie einen Werkzeugkasten. Also manchmal hast du nichts in deinem Werkzeugkasten und es ist ein Pokerspiel. Manchmal befinden sich darin nützliche Informationen, aber du musst nicht alle gleichzeitig brauchen. Du musst sie im richtigen Moment hervorholen. Die Arbeit besteht darin, die Werkzeuge im richtigen Moment herauszuholen und sie geschickt zu benutzen”.

Beispiel:
- Informationen, die an dich weitergegeben werden können/ dürfen: Du wirst wegen Landfriedensbruchs, unerlaubter Demonstration und Sachbeschädigung angeklagt. 
- Informationen, die an dich weitergegeben werden können, wenn dies dazu führen kann, dass du eine Gegenleistung erbringst: Du wirst ausdrücklich verdächtigt, an der Plünderung eines Geschäfts während der betreffenden Demonstration beteiligt gewesen zu sein. 
- Informationen, die unter keinen Umständen an dich weitergegeben werden dürfen: Dein Telefon wird abgehört, wodurch die Polizei erfahren hat, mit wem du bei der Demonstration warst. Es sind Hausdurchsuchungen und Festnahmen dieser Personen geplant.

### Verteidigunsstrategien voraussehen

Der letzter Faktor bei der Vorbereitung auf ein Verhör: Deine Verteidigungsstrategien nach dem Studium deines Profils vorwegnehmen. Besteht die Gefahr, dass du ein Alibi vorweisen kannst, das vor der Fortsetzung des Verfahrens überprüft werden muss? Wirst du dich auf eine Lüge einlassen? Wirst du versuchen, Freund:innen zu decken, oder wirst du stattdessen eine:n Kompliz:in beschuldigen? Wirst du ein Teilgeständnis ablegen, in der Hoffnung, einen Teil der Wahrheit verbergen zu können? Wirst du die gute Idee haben, dich durch Schweigen zu schützen? Wie wirst du reagieren, wenn du mit deinen Lügen, den Beweismitteln, den Aussagen von Mitangeklagten oder Zeug:innen konfrontiert wirst?

Auf der Grundlage all dieser Umstände werden die Inspektor:innen entscheiden, welche Strategien und Verhörtechniken sie gegen dich anwenden und welche sie weglassen.

## 5. Generelle Manipulationsstrategien

> “Manipulation besteht darin, ein Bild der Wahrheit zu konstruieren, das so aussieht, als wäre es die Wahrheit “.

-- Philippe Breton

Ich definiere den Begriff Manipulation als die Absicht, die Eindrücke, Gedanken und Entscheidungen einer Person zum eigenen Vorteil zu beeinflussen und zu kontrollieren. Eine Person zu manipulieren bedeutet, ihr die Freiheit einer freien und bewussten Entscheidung zu verweigern. Ein Antagonismus also, zu der anarchistischen Idee, dass jedes Individuum legitimiert ist, ein freies Leben durch und für sich selbst zu führen.

Seit einigen Jahren benennt die Sozialpsychologie wiederkehrende Manipulationsmuster, die je nach Kontext, in den sie eingebettet sind, einen anderen Namen haben. “Belästigung” in einem Kontext patriarchaler Unterdrückung, “Mobbing”, wenn die Manipulation in einem beruflichen Umfeld stattfindet, “Missbrauch” und “toxische Beziehung”, wenn es sich um eine emotionale Beziehung handelt.

Die Praxis des polizeilichen Verhörs fügt sich nahtlos in dieses Feld unterschiedlicher Manipulationskontexte ein. Ein Verhör, ist eine Interaktion, die unter Zwang erlebt wird und auf einem ungleichen Machtverhältnis beruht. Die Polizei bedient sich in vollen Zügen der verschiedenen Manipulationstechniken, die in allen anderen oben genannten Kontexten üblich sind. Es gibt jedoch eine Nuance zwischen Manipulationstechniken und -strategien. 

- Techniken sind konkrete und kurze Manipulationselemente (der Aufbau eines Satzes, eine Intonation). 
- Strategien beziehen sich auf einen längeren Zeitrahmen, der die gesamte Befragung oder sogar mehrere Befragungen umfassen kann.

Im Folgenden wird eine Reihe von Manipulationstechniken aufgelistet, die häufig in verschiedenen Formen der Manipulation, zu denen auch Verhöre gehören, eingesetzt werden.

### Sympathie erzeugen

Ein Teil der Polizeistrategien erfordert, dass die befragte Person Sympathie für die Polizist:innen empfindet. Dies erfordert eine 180°Wendung gegenüber der Realität. Obwohl sie gegen dich ermitteln, dich überwachen, dich einsperren und aktiv nach Beweisen suchen, die es der Justiz ermöglichen dich zu bestrafen, versuchen sie dich davon zu überzeugen, dass sie es in Wirklichkeit gut mit dir meinen und dich respektieren. Ziel ist es, dein Misstrauen zu senken, um dich durchlässiger für Strategien zu machen, die auf der zwischenmenschlichen Beziehung zwischen Polizei und Verdächtige beruhen.

Wie macht ein Mensch sich sympathisch? Soziolog:innen, die sich mit dieser Frage beschäftigt haben, haben mehrere Faktoren festgestellt, die bei den meisten von uns einen bestimmten, aber unbewussten Einfluss haben: das Aussehen, Gemeinsamkeiten (der eine Polizist hat auch einen Sohn, die Polizistin ist Fan von demselben Eishockeyverein wie du), eine gewisse Vertrautheit, positive Assoziationen (der:die Polizist:in, der:die dir den “Gefallen” tut, dir etwas zu essen zu geben, obwohl sie dich hungern ließen, wird mit der Freude in Verbindung gebracht, endlich etwas essen zu können).

Schmeicheleien gehören ebenfalls zum Manipulationsarsenal von Ermittler:innen. Im Gegensatz zu einem Kompliment zielt die Schmeichelei darauf ab, dich zu einer bestimmten Sache zu verführen. Das gibt dir Vertrauen, lässt deine Deckung schwächen und versetzt dich in eine positive Stimmung für den weiteren Verlauf.

### Das Prinzip der Gegenseitigkeit

> “Wenn dir jemand etwas anbietet, musst du auch etwas zurückgeben “.

-- Soziale Norm, 2022

Das Prinzip der Gegenseitigkeit baut auf der sozialen Norm auf, die besagt, dass du, wenn du etwas erhältst, verpflichtet bist etwas zurückzugeben. Wir alle lernen diese Norm durch unsere Erziehung.

Wenn du akzeptierst etwas zu nehmen, musst du auch etwas zurückgeben. Wenn du von diesem Gebot abweichst, setzt du dich negativem sozialem Druck und starker Verurteilung aus. Du kannst zum Beispiel als egoistisch, profitorientiert, parasitär, unhöflich oder undankbar bezeichnet werden. Bei dieser Manipulationstechnik wird das Gefühl der Schuld ausgenutzt, das dadurch entsteht, dass du etwas erhalten hast, auch wenn du gar nicht darum gebeten hast.

Bei der Vernehmung, wird diese Technik in einem besonders ungleichen Machtverhältnis eingesetzt. Die Gefälligkeiten, die dir von einigen Inspektor:innen “angeboten” werden, gleichen in Wirklichkeit den Mangel aus, den diese Inspektor:innen durch deine Inhaftierung geschaffen haben. Sie bringen dir ein Glas Wasser, erlauben dir, einen Anruf zu tätigen, Besuch zu empfangen oder ein Buch zu lesen. All diese “Gefälligkeiten” dienen dazu, in dir ein Gefühl der Abhängigkeit und Dankbarkeit zu erzeugen. Sobald du zögerst, ihre Fragen zu beantworten, wirst du an diese “Gefälligkeiten” erinnert, in der Erwartung, dass du nun die Höflichkeit erwiderst.

### Aversives Zuhören

Aversives Zuhören ist, wenn die Polizist:innen, die dich verhören, wegschauen oder etwas anderes tun, während du mit ihnen sprichst oder dass sie nicht zu dir aufschauen, wenn du in den Verhörraum kommst. Die Idee hinter dieser Haltung ist einfach: Sie soll dich verunsichern, ein unangenehmes Gefühl erzeugen und dir das Gefühl geben, dass das was du sagst, uninteressant ist und dass dieses Verhör nichts Wichtiges ist, sondern nur ein langweiliges Routineprotokoll, das es zu erledigen gilt. Aber auch, dass ihre Vorstellungen von dir bereits vorgefertigt sind. Als Reaktion auf diese Haltung ist erhofft, dass du vielleicht um jeden Preis ihre Aufmerksamkeit erregen willst und dich für das Geschehene rechtfertigen wirst. Dadurch gibst du vielleicht viel mehr Informationen preis, als die Polizei bei einer konfrontativen Haltung aus dir hätte herausbekommen können.

### Das Falsche predigen, um das Wahre zu erfahren

Diese Technik besteht darin, eine Frage zu stellen, die wissentlich ein falsches Element enthält. Das Ziel ist, dass dein Wunsch, die Wahrheit wiederherzustellen, dich dazu bringt, mehr Informationen zu geben, als wenn dir die Frage neutraler gestellt worden wäre.

Betrachten wir die beiden Fragen:

1. Was haben Sie in Berlin gemacht?
1. Sind Sie nach Berlin gefahren, um eine:n Liebhaber:in zu treffen?

Die erste Frage scheint keine besondere emotionale Herausforderung darzustellen, weder in der Frage, noch in der Antwort. Es ist eine offene und eher neutrale Frage. Die Polizei möchte offensichtlich nur wissen, was du in Paris gemacht hast.

In der zweiten Frage deutet die Polizei bereits an, dass sie weiß, was du in Berlin gemacht hast. Dadurch entsteht die Unterstellung, dass du tatsächlich eine:n Liebhaber:in hast. Die Polizeibeamt:innen, die dir diese Frage stellen, wissen jedoch genau, dass du nicht aus diesem Grund nach Berlin gefahren bist. Sie kennen jedoch den wahren Grund für deinen Besuch nicht und hoffen, dass deine Antwort auf die Frage - getrieben von dem Wunsch, dich zu rechtfertigen oder etwas Falsches richtig zu stellen - vollständiger ist, als wenn sie neutral formuliert worden wäre.

\[Polizei-Zitat\]

>  “\[Das Verhör ist wie\] ein Schach oder Pokerspiel also darfst du bluffen. Du hast Spieler vor dir, du hast Leute, die dir nicht unbedingt die Wahrheit sagen, die ganze Wahrheit, oder die sie auf ihre Weise arrangieren. Du hast Karten in der Hand und der Mann weiß nicht unbedingt, welche Karten du in der Hand hast, also kannst du bluffen, du kannst das Falsche predigen, um das Wahre zu erfahren “Es hängt alles vom Inspektor ab, manche spielen mit Drohungen, andere predigen das Falsche, um das Wahre zu erfahren. Ich bleibe in der Realität, ich bin ehrlich”.

### Misstrauen erwecken


> “Das ist aber nicht das Gleiche, was Ihr Freund uns erzählt hat”.

-- Polizeiliche Unterstellung

Das Erzeugen von Misstrauen in einer Gruppe ist eine gute Methode, um diese Gruppe zu schwächen, Zwietracht zu säen, sich von den anderen zu distanzieren und zu verhindern, dass ein kollektives Gefühl gestärkt wird. Es gibt viele verschiedene Methoden der Manipulation, die dazu dienen, die Saat des Zweifels zu säen. Sie reichen von harten Lügen bis hin zu scheinbaren Andeutungen darüber, was deine Freund:innen gesagt oder getan haben könnten.

Auch wenn du nicht darauf achten willst, wird die Botschaft von deinem Gehirn aufgenommen und verarbeitet. Da diese Nachricht in der Regel mit emotionalen Affekten behaftet ist, wird es nicht leicht sein sie wieder zu vergessen, auch wenn du nicht daran glaubst. Das Gefährliche an solchen Bemerkungen ist, auch wenn du ihnen im Moment keinen Glauben schenkst, dass sie beim ersten Signal, das diese Aussagen zu bestätigen scheint, oder in Momenten der Schwäche und emotionalen Erschöpfung, wieder auftauchen können. Die Botschaft wurde gehört und registriert.

Ein Element der Verteidigung gegen diese Technik ist es, alle Anschuldigungen, die direkt oder indirekt von der Polizei gegen eine Person, die dir nahesteht, vorgebracht werden, ganz und gar abzulehnen. Wenn du nicht in der Lage bist, eine von den Ermittler:innen gemachte Aussage selbst zu überprüfen, gehe davon aus, dass sie nicht wahr ist. Du kannst später, wenn die Gefahr des Verhörs vorüber ist, immer noch darauf zurückkommen. Vergiss nicht, dass die Polizist:innen, die dir gegenübersitzen, im Gegensatz zu deinen Verwandten oder Mitangeklagten keine Freund:innen sind und dir nichts Gutes wollen. Ihre Aufgabe ist es, dich emotional zu schwächen und zu Aussagen zu bewegen, um ihre Ermittlungsakte zu füllen..

\[Buch\]

> — Große Liebe was? Der Leutnant Knut machte ein spöttisches Gesicht.
> 
> Lenz zuckte die Achseln.
> 
> — So was soll‘s geben.
> 
> Wieder sah der Leutnant ihn an, dann schüttelte er den Kopf.
> 
> — Wenn ich mir Ihren Werdegang ansehe und die kriminelle Energie betrachte, die Sie aufbrachten, um von uns wegzukommen, wirkt Ihre Flucht geradezu lächerlich. Wie geht das Sprichwort? Wenn es dem Elefanten zu gut geht, tanzt er auf dem Eis.
> 
> Der hätte lieber Zigaretten bereitlegen sollen, als ihm mit solchen Weisheiten zu kommen.
> 
> — Wissen Sie was ich vermute? Sie wollten uns nur Ihrer Frau zuliebe verlassen.
> 
> — Das muss keine falsche Schlussfolgerung sein”. So lautete ihre Absprache: Wenn sie wider Erwarten doch gefasst werden sollten, wollten sie aussagen, das keinerlei politische Motive hinter ihren Fluchtabsichten steckten, sondern allein der Wunsch nach Familienzusammenführung. Doch ob Hannah sich noch daran hielt? Vielleicht hatte sie ja längst die Wahrheit gesagt.
> 
> — Also tatsächlich: Die große Liebe! Nur hat Ihre Frau leider etwas anderes erzählt.
> 
> Das mit den Zigaretten war wohl doch Absicht, er wollte ihn heute mal wieder zwiebeln, dieser Knut.
> 
> — Sie waren doch oft zur Leipziger Messe. Man weiß ja, wie es da so zugeht zwischen Männlein und Weiblein — Könnten Sie ein wenig deutlicher werden?
> 
> — Bitte schön: Ihre Frau hat uns einige Zweifel an Ihrer “großen Liebe” geäußert. Und erst recht an Ihrer Treue.
> 
> Lenz musste lächeln.
> 
> — Nennt man so etwas psychologische Kriegsführung?
> 
> — Sie glauben mir nicht?
> 
> — Nicht, solange meine Frau diese Äußerung in meiner Gegenwart nicht wiederholt hat.
> 
> — Denken Sie etwa, wir wollen Sie und Ihre Frau gegeneinander ausspielen?
> 
> — Der Verdacht liegt nahe.
> 
> — Sie trauen uns ja allerhand zu. Er machte ein überhebliches Gesicht, der Genosse Leutnant, zog die Schublade auf und warf eine angebrochene Packung Zigaretten auf den Tisch.
> 
> — Hab ganz vergessen, dass Sie Raucher sind.
> 
> So wird man es ihnen beigebracht haben, Genossen zu vernehmen: Behandle den zu Vernehmenden mal freundlich und großzügig, mal brause über die geringste Kleinigkeit auf; sei mal der mitfühlende Mitmensch, mal der strenge Untersuchungsrichter.
> 
> Sie wissen, dass du jedes Wort, das hier gesprochen wird, in deiner Zelle tausendmal wiederholst, und rechnen damit, dass sich auch die kleinste, wie nebenbei hingestreute Bemerkung in deinem Kopf festsetzt und irgendwann Zweifel auftauchen: Kann es denn nicht doch sei, das Hannah dir zutraut, in Leipzig den Don Juan gespielt zu haben?

Auszug aus dem Buch, Krokodil im Nacken, Klaus Kordon, 2008


### Erniedrigen und Abwerten

Eine Person erniedrigen, damit sie sich abgewertet fühlt, ihre Fähigkeiten anzweifelt, kein Selbstvertrauen hat und eine emotionale Abhängigkeit entwickelt, ist ein Klassiker in jeder toxischen Beziehung – und auch bei Verhören. Diese Technik steht jedoch im Widerspruch zu Strategien, die auf der Humanisierung der Beziehung zwischen Ermittler:in und Verdächtige:m basieren. Sie wird daher nur dann eingesetzt, wenn die Inspektor:innen der Meinung sind, dass diese offensive Methode eher zum Erfolg führen wird, als die Versöhnliche.

Um dich zu erreichen, werden ihre moralischen und kritischen Urteile auf Themen gerichtet, von denen sie wissen, dass sie für dich sensibel sind. Sie können dich mit deinen Widersprüchen und Zweifeln konfrontieren und dir die Schuld für deine Fehler in der Vergangenheit und deine schwierige Situation geben. Sie können dich systematisch dazu bringen, zu denken, dass du diese oder jene Entscheidung nicht hättest treffen sollen und du dumm gehandelt hast. Das führt zu starken Schuldgefühlen und Abwertungen, die vielleicht schon in dir vorhanden sind, aber durch die polizeiliche Manipulation noch verstärkt werden.

Generell solltest du Kritik von einer manipulativen Person nicht als bedenkenswert betrachten. Ich spreche hier nicht von konstruktiver, empathischer und wohlwollender Kritik von denjenigen, die es gut mit dir meinen. Und hier ein Knüller: Polizist:innen sind nicht deine Freund:innen, sie kümmern sich nicht um dein Wohlbefinden oder deine intellektuelle Entwicklung und haben kein Interesse daran, konstruktive Kritik zu äußern. Sie interessieren sich nicht dafür, wer du bist, was für dich wichtig ist oder welche Empfindlichkeiten du hast. Sie haben ihre eigenen Interessen, die nichts mit deiner Person zu tun haben, sondern nur mit ihrer täglichen Arbeit und der Ermittlungsakte. Und wenn sie dir das Gegenteil erzählen, erinnere dich daran, wer dich in diesem Raum eingesperrt hat und dich gegen deinen Willen dort festhält.

### Glaubenssätze und Sensibilitäten ausnutzen

Wir alle haben ein System von Überzeugungen, Werten und Empfindlichkeiten. Dieses wurde durch unsere Erziehung, religiösen und spirituellen Überzeugungen, sowie durch die Muster und Vorbilder, die in der Gesellschaft vorgelebt werden, aufgebaut.

Einige Sensibilitäten rühren von unseren Erfahrungen und/oder der Dekonstruktionsarbeit her, die wir unternehmen, um uns unser Leben nach unseren eigenen Wünschen wieder anzueignen, indem wir mit den uns umgebenden gesellschaftlichen Normen brechen.

Mit Ausnahme dieser Dekonstruktionsarbeit verankern sich unsere Überzeugungen schon früh in unserem Leben und werden nur selten in Frage gestellt.

**Bei einem Verhör, ist die Identifizierung deiner Empfindlichkeiten und deines Wertesystems für die Polizei von großer Bedeutung. Dies ermöglicht ihnen, sie als Hebel zu benutzen, um deine Emotionen und Gefühle zu beeinflussen.**

Sie werden beispielsweise versuchen, in dir Schuldgefühle oder mangelndes Vertrauen zu wecken, um dich davon zu überzeugen, dass deine Handlungen im Widerspruch zu deinen eigenen Werten stehen.

Hier sind einige der klassischen Glaubenssätze unserer westlichen Gesellschaft, die unbewusst übernommen und selten hinterfragt werden. Es handelt sich um absolute Wahrheiten, die die Nuancen, Zusammenhänge und Umstände, in denen sie ausgesprochen werden, nicht berücksichtigen. 

- Du musst alles wissen, sonst bist du unwissend und dumm. 
- Du darfst keine Fehler machen. Fehler zu machen ist keine normale Praxis in einer Lerndynamik sondern wird mit Dummheit gleichgesetzt. 
- Du musst den Anderen zeigen, dass du gebildet, intelligent und interessant bist, sonst bist du wertlos. 
- Um wertgeschätzt zu werden, musst du in jeder Situation kompetent sein. 
- Du darfst deine Meinung nicht ändern, sonst bist du instabil und nicht glaubwürdig. 
- Nur Dummköpfe ändern ihre Meinung nicht (Gegenteil der vorherigen Norm). 
- Wenn du eine Verpflichtung eingehst, musst du unbedingt Wort halten und die Sache zu Ende bringen, auch wenn du deine Meinung änderst. 
- Du solltest nie undankbar sein sondern immer dankbar für das sein, was du bekommt, auch wenn du nicht darum gebetet hast. 
- Wenn dir jemand etwas gibt, solltest du automatisch etwas zurückgeben, sonst bist du undankbar. 
- Du musst großzügig sein. 
- Du musst nett und freundlich sein, sonst bist du gemein, unsensibel und aggressiv, egal unter welchen Umständen. 
- Menschen müssen für ihre Boshaftigkeit oder wenn sie sich nicht an die Regeln halten, bestraft werden. 
- Du musst immer die richtigen Entscheidungen treffen, sonst bist du dumm.

Diese gesellschaftlichen Werte können sich leicht gegen dich richten. Wenn die Ermittler:innen beispielsweise aufzeigen, dass du zu einem bestimmten Thema keine Ahnung hast, können sie in dir das Gefühl wecken, dass du nicht intelligent bist und so dein Selbstbewusstsein senken.

### Rezenzeffekt

Die Technik des Rezenzeffektes versucht, eine Person durch die Art und Weise, wie ein Satz aufgebaut ist, unbewusst zu konditionieren. Unser Gedächtnis merkt sich leichter die Wörter, die am Anfang und am Ende von Sätzen stehen. Vor allem, wenn diese Sätze absichtlich lang und kompliziert sind. Vor allem, wenn du von einer stundenlangem Verhör erschöpft bist. Vor allem, wenn die Inspektor:innen versuchen, dich kurz vor der Frage durch eine plötzlich aggressive Haltung oder durch das kommunizieren einer neuen Information aus dem Konzept zu bringen.

Beispiel für eine positive Konditionierung: Es ist natürlich ihre freie Entscheidung. Auch wenn sie dadurch von dem:r Richter:in als verdächtig erscheinen werden und bekanntlich nur kriminelle schweigen, können sie von Ihrem Recht gebrauch machen.

Beispiel für eine negative Konditionierung: Auch wenn Sie dadurch vor dem Richter verdächtig wirken, ist es Ihre freie Entscheidung, vom Recht zu Schweigen Gebrauch zu machen, auch wenn bekannt ist, dass nur Kriminelle schweigen.

### Harpunierung

Stell dir vor, du hebst in der Bank Dollar ab und der:die Angestellte sagt: “Es kommt in letzter Zeit nicht oft vor, dass Leute Dollar abheben”. Daraufhin sagst du, ohne darauf zu achten: “Ja, ich fliege für zwei Wochen nach Florida, um meine Familie zu besuchen”.

Ohne dass dir eine Frage gestellt wurde, gibst du zwei Informationen über dich weiter: Du gehst für zwei Wochen nach Florida und du hast dort Familie. Diese harmlose Situation kann im Kontext eines Verhörs weitaus problematischer sein. Die Formulierung einer einfachen Behauptung, anstelle einer gerichteten Frage, erweckt den Eindruck, dass es sich um eine Diskussion handelt, bei der nichts auf dem Spiel steht, oder sogar, dass die Polizei nicht unbedingt nach Informationen über diese Frage sucht. Die Behauptung kann auch mit Argwohn formuliert werden, um zu einer Rechtfertigung zu drängen.

### Hoffnung und Enttäuschung

Ein Versprechen in Aussicht zu stellen, wird in einer Person einen positiven Vorstellungs- und Projektionsprozess auslösen und ist mit der Hoffnung auf die Erfüllung des Versprechens verbunden. Wenn dieses schließlich nicht eingehalten wird, erzeugt dies Desillusionierung und Enttäuschung. Durch wissentlich falsche Versprechungen soll eine emotionale Erschöpfung herbeigeführt werden. Polizei- oder Untersuchungshaft ist ein besonders günstiger Nährboden für diese Technik. Da du eine Vielzahl von Entbehrungen erleidest, kann die Polizei dir viele Gefälligkeiten in Aussicht stellen (Telefonanruf, Recht auf ein Buch in der Zelle, Recht auf Besuch usw.), um schließlich Enttäuschungen zu erzeugen, indem sie dir diese verweigert. Noch bösartiger wird es, wenn die Polizist:innen dich beschuldigen, für ihre Ablehnung (und damit für deine Enttäuschung) verantwortlich zu sein. “Wenn Sie sich kooperativer gezeigt hätten, hätten wir etwas für Sie tun können.”

\[Buch\]

> Im Treppenhaus, war plötzlich ein weiteres noch sehr fernes Schlüsselscharren zu hören. Sofort führte der Tempelaffe Lenz in den Zellengang im ersten Stock und schob ihn in einen offensichtlich für solche Fälle der unverhofften Begegnungen frei gehaltenen Verwahrraum. Zwei, drei Minuten musste er warten, dann ging es noch eine Treppe höher und in den Vernehmertrakt mit dem roten Teppich hinein. Jetzt aber konnte er sich denken, dass in diesem Augenblick hinter der Türen rechts und links von ihm vernommen, geleugnet, zugegeben, Reue bekannt oder Mut aufgebracht wurde. Und er ? Wie sollte er sich denn nun verhalten ?
> 
> Der Tempelaffe führte ihn vor die Tür, vor der er schon einmal gestanden hatte, und auch der Vernehmer, der ihn erwartete, war derselbe.
> 
> Nur trug der Klassensprecher diesmal Leutnantuniform. Grinsend wartete er ab, bis Lenz auf dem Häftlingshocker Platz genommen hatte, dann fragte er, als hätten sie sich nur etwa zwei Tage nicht gesehen.
> 
> — Na? Wie geht‘s denn so?
> 
> — Den Umständen entsprechend – gut — Freut einen zu hören!
> 
> Ich glaub dir nicht, sollte das heißen. Ich weiß, wie beschissen dir zumute ist und wie sehr du dich darüber freust, dass endlich wieder Notiz von dir genommen wird. Lenz wandte den Blick ab. Mit Worten zu lügen war einfach, die Augen mitlügen zu lassen viel schwieriger.
> 
> Auf dem Tisch vor dem Schreibtisch lag eine bereits geöffnete, aber noch volle Packung Zigarette, Marke “Kabinett”. Rauchte der Klassensprecher?
> 
> Oder hatte er die Stäbchen seinetwegen dort hingelegt?
> 
> — Haben Sie irgendwelche Beschwerden?
> 
> Das hatte ihn der Sofioter Chef auch gefragt, lernten sie diese Hotelportiersfragen in der Ausbildung?
> 
> — Ja.
> 
> — Und welche?
> 
> — Ich hätte gern was zu lesen. Sie werden doch eine Bibliothek im Haus haben.
> 
> Ein belustigtes Lachen war die Antwort.
> 
> — Na, wissen Sie! Sie kooperieren nicht mit uns und sind so unbescheiden, zur Belohnung auch noch Lektüre zu verlangen!

Auszug aus dem Buch, Krokodil im Nacken, Klaus Kordon, 2008

### Die Tür vor der Nase zu knallen

\[Film\]

> — Also, ich höre dir zu! Ich warte nicht, wenn du mich warten lässt, stecke ich dich ins Loch \[...\].
> 
> — Ich bin bereit, mich zu erklären, aber ich werde keine Namen nennen.
> 
> — Dann spielen wir das X-Spiel. Jedes Mal, wenn du über jemanden sprichst, wirst du ihn X1 nennen, dann X2, X3 und so weiter.
> 
> — Im Sommer 97 traf ich einen ehemaligen nationalistischen Genossen, X1, ...

Zitiert aus dem Film Les Anonymes, Pierre Schoeller, 2014

Bei dieser Technik wird ein Vorschlag formuliert, die im Vergleich zu den Möglichkeiten und Wünschen der Zielperson exorbitant hoch ist und von dieser konsequent abgelehnt wird. Diese erste Formulierung erhöht jedoch die Chancen, dass ein zweiter Vorschlag danach angenommen wird.

In dem oben genannten Beispiel fordert der Inspektor den Befragten auf, die Namen seiner Kompliz:innen zu nennen, was dieser strikt ablehnt. Als er jedoch später aufgefordert wird, die Ereignisse zu schildern und die Namen durch Spitznamen (X1, X2, ...) zu verschleiern, antwortet dieselbe Person positiv. Dies sind praktisch die gleichen Informationen, die gegeben werden, wodurch die Polizei mit hoher Wahrscheinlichkeit die Identitäten der Betroffenen ermitteln wird.

### Falsche Versprechungen

Diese Technik der Falschen Versprechungen wird häufig von der Polizei eingesetzt, um Informationen zu erpressen. Falsch, weil die Begriffe irreführend sind und weil die Versprechungen oft die Möglichkeiten der Ermittler:innen übersteigen. Wenn Polizist:innen versprechen, dass sie im Austausch für Informationen dem:der Richter:in ein Wort über deine Ehrlichkeit sagen werden, dass dir das Sorgerecht für dein Kind nicht entzogen wird, dass dir deine Sachen zurückgegeben werden, die während der Ermittlungen beschlagnahmt wurden, dass jemand anderes nicht über deine Taten informiert wird, dann ist das alles gelogen. Die Polizei hat in diesen Bereichen keine Handlungs- und Entscheidungsbefugnis und auch nicht darüber, was im weiteren Verlauf des Gerichtsverfahrens mit dir geschehen wird. Diese Vorrechte liegen bei der Staatsanwaltschaft oder sogar bei den Richter:innen.

Es ist nicht ungewöhnlich, dass die Inspektor:innen, nachdem sie dir mit falschen Versprechungen Informationen entlockt haben, die Bedingungen eurer Vereinbarung ändern und dir weitere Fragen stellen und damit drohen, den Deal zu brechen, wenn du sie nicht beantwortest. Du lässt dich auf einen Deal ein, weil dir bestimmte Bedingungen akzeptabel erscheinen. Im letzten Moment ändern sich die Bedingungen, aber du akzeptierst die Situation weiterhin, weil du ein Gefühl der Verpflichtung hast. Da du bereits ein Stück des Weges zurückgelegt hast, scheint es einfacher weiterzumachen, als umzukehren. Du hättest den Deal aber nicht angenommen, wenn er dir von Anfang an in seiner endgültigen Form präsentiert worden wäre.

Ein Vertrag mit der Polizei ist ein sehr unsicheres Unterfangen, da du keine Kontrolle über die äußeren Umstände hast und kein Druckmittel besitzt, damit sie ihre Versprechen einhalten.

### Ausweichvorschlag

Diese Technik dient dazu, herauszufinden, ob ein Thema für dich sensibel ist oder nicht. Die Ermittler:innen befragen dich zu einem Thema, bei dem sie sich vorstellen können, dass du etwas zu verbergen hast. Nach mehreren anklagenden Fragen zu diesem Thema gehen sie plötzlich auf ein anderes, völlig irrelevantes Thema über, um deine Reaktion zu testen. Wenn du erleichtert reagierst und dich ebenfalls auf das neu aufgetauchte Thema einlässt, wird das als Zeichen dafür gewertet, dass du dem ersten Thema ausweichen willst und möglicherweise etwas zu verbergen hast.

*Beispiel*

> − Warst du letzten Montag in Paris?
> 
> - Nein.
> 
> - Komm schon, hör auf zu lügen, wir wissen, dass du dort warst.
> 
> - Aber nein, ganz und gar nicht.
> 
> - Warum sollten wir lügen? Wir wissen, dass du dort warst, um Louis zu sehen. Übrigens, warte mal, was ist noch mal die Fußballmannschaft von Paris? St-Gervains? St-Germain?
> 
> Ausweichen
> 
> - Nein, das ist Paris-St.Germain, die PSG.
> 
> - Ah ja. Die sind ziemlich gut in der Tabelle, oder?
> 
> - Ja ja, das stimmt.
> 
> Nicht ausweichen
> 
> − Was? Aber das ist doch jetzt egal, oder? Ich sage Ihnen, dass ich an diesem Tag nicht in Paris war.

## 6. Verhörstrategien

Im Gegensatz zu den oben erwähnten Manipulationstechniken sind die folgenden Strategien, in einem längeren Zeitrahmen angesiedelt. Sie werden über die gesamte Vernehmung oder sogar über mehrere Vernehmungen hintereinander entwickelt. Während die Manipulationstechniken, je nach Interaktion zwischen Polizei und verdächtigter Person spontan eingesetzt werden, werden die Verhörstrategien je nach Profil der befragten Person im Voraus ausgewählt und vorbereitet.

### Good cop, bad cop

\[Situationsbeschreibung\] 

> Du sitzt in einem kleinen, betonierten Büro und hast einen besonders aggressiven Inspektor vor dir, der gestikuliert, die Stimme erhebt, dich beschimpft und bedroht. Plötzlich unterbricht ihn die Polizistin hinter ihm, setzt sich dir gegenüber und schaut dich ruhig an. Sie sagt mit beruhigender Stimme, dass alles gar nicht so schlimm ist, wie es scheint, dass es bald vorbei ist, dass du nur noch ein paar kleine Fragen beantworten musst und dass du dann gehen kannst, versprochen. Gibst du nach? Nicht? Dann klopft der erste Polizist auf den Tisch, funkelt dich an, droht damit, dich zurück in die Zelle zu bringen und dich dort eine Woche lang festzuhalten, und stellt dir dann sehr spezifische Fragen, auf die du keine Lust hast zu antworten. Als die “nette” Polizistin merkt, dass dir das Thema unangenehm ist, unterbricht sie ihren Kollegen wieder und wirft dich auf ein anderes Thema, das harmlos erscheint und auf das du dich gerne einlässt, sei es nur, damit der “böse” Polizist sich fernhält und heikle Themen meidet. Doch nach und nach führen dich die Fragen wieder zu dem unerwünschten Thema zurück und der “böse”” Bulle wartet nur auf eine Gelegenheit, um dich wieder zu attackieren. Kannst du das aushalten?
> 
> Willkommen bei der Strategie “good cop, bad cop”, einem Klassiker jeder Krimiserie.

Bei dieser Verhörstrategie tritt eine:r der Polizist:in aggressiv und bedrohlich auf und greift frontal mit unangenehmen und unbequemen Themen an. Im Gegensatz dazu nimmt der:die andere Polizist:in eine beschwichtigende, ruhige, fast wohlwollende Haltung ein. Zwischen ihnen bist du wie ein Pingpongball, der von einem zum anderen geschickt wird, bis du zerbrichst. Die Rolle des:r “bösen” Polizist:in besteht darin, dich unter Druck zu setzen, dich an deine Grenzen zu bringen, dich zu zermürben und dir Angst einzujagen.

Wenn der:die zweite Inspektor:in der Meinung ist, dass du bereit bist, zusammenzubrechen, oder wenn ein besonders heikles Thema angesprochen wird, übernimmt er:sie die Befragung, beruhigt dich, bietet dir ein Glas Wasser an, schlägt dir eine Pause vor und macht mit ruhiger Stimme Versprechungen, bevor er:sie die Fragen wieder aufnimmt: “Wir wollen nur eine Antwort auf diese Frage, dann können Sie nach Hause gehen”.

Um sich abzusprechen und zu wissen, wann sie das Ruder übergeben sollen, verwenden die Polizist:innen bestimmte Signale, z.B. ein Wort, ein Körperzeichen oder sogar eine bestimmte Tonlage. Beide Rollen sind nicht unbedingt gleichzeitig anwesend. Es kann sein, dass mehrere Gespräche zunächst nur mit Polizist:innen in der “bösen” Rolle stattfinden. Dann kommen zwei Inspektor:innen, die ruhig und beschwichtigend sind. Du kannst dir denken, dass die aggressiven Polizist:innen wiederkommen werden, wenn du nicht kooperierst.

Um sich abzusprechen und zu wissen, wann sie das Ruder übergeben sollen, verwenden die Polizist:innen bestimmte Signale, z.B. ein Wort, ein Körperzeichen oder sogar eine bestimmte Tonlage. Beide Rollen sind nicht unbedingt gleichzeitig anwesend. Es kann sein, dass mehrere Gespräche zunächst nur mit Polizist:innen in der “bösen” Rolle stattfinden. Dann kommen zwei Inspektor:innen, die ruhig und beschwichtigend sind. Du kannst dir denken, dass die aggressiven Polizist:innen wiederkommen werden, wenn du nicht kooperierst.

Das schnelle Wechseln von einer Emotion zur nächsten führt zur emotionalen Erschöpfung. Dieser Versuch, deine Emotionen durch ein bestimmtes Verhalten zu beeinflussen, wird als emotionale Ansteckung bezeichnet. Tatsächlich beeinflusst der emotionale Zustand einer Person, die uns gegenübersitzt, unsere eigenen Emotionen. Die Begegnung mit einer aggressiven Person kann uns in einen Zustand der Wut, der Angst oder des Stresses versetzen, während die Begegnung mit einer ruhigen und sanften Person Ruhe, aber vielleicht auch Misstrauen erzeugt. Mit diesem Mechanismus können wir einen emotionalen Zustand beeinflussen, der sich mit dem Rhythmus der Gesprächspartner:in und ihrem Verhalten ändert. Das ist der Grund für eine starke geistige Erschöpfung. In Verbindung mit dem Stress des Verhörs und der Angst, wieder mit den “bösen” Polizist:innen konfrontiert zu werden, ist die Gefahr groß, dass du den “guten” Polizist:innen leichter nachgibst.

Versöhnliche Atmosphäre

```
--- i
  v
```

Feindselige Atmosphäre

```
i i
---
 v
```

(Inspektor:innen: `i`, Verdächtige:r `v`)

Je nachdem, welche Atmosphäre die Polizei schaffen möchte, werden die Stühle anders angeordnet. Gegenüber stehende Stühle, wenn sie eine konfrontative Atmosphäre schaffen wollen – wie bei dieser Strategie – und an der Seite des Tisches stehend, wenn sie dich in eine tröstende, kollaborative Position bringen wollen.

Um sich zu schützen, gibt es nichts Besseres, als zu schweigen oder immer wieder zu sagen: “Ich mache keine Aussage”. Je schneller die Polizist:innen verstehen, dass du dich nicht emotional auf ihre Strategie einlässt, desto schneller werden sie dich in Ruhe lassen.

### Treibsand

\[Situationsbeschreibung\]

> Du sitzt immer noch in demselben kleinen Büro mit Betonwand und sitzt zwei Inspektor:innen gegenüber, die dir eine Frage stellen, auf die du nicht mit der Wahrheit antworten möchtest. Du lügst, ohne zu wissen, dass sie die wahre Antwort bereits kennen. Diese Frage ist eigentlich nur ein Test, um zu sehen, ob du dich auf eine Lüge einlassen wirst oder nicht. Jetzt wissen sie es. Also bringen sie dich dazu, immer weiter zu lügen. Jede Lüge wirft eine neue Frage auf, für die du dir schnell eine Antwort ausdenken musst, die mit dem Rest deiner Geschichte übereinstimmt. Es ist nicht leicht, sich daran zu erinnern, was du ihnen in der Vergangenheit genau erzählt hast. Plötzlich erklärt eine:r der Polizist:innen mit triumphierendem Gesicht, dass sie wissen, dass du lügst, dass sie etwas haben, das beweist, dass das, was du sagst, nicht stimmt, dass du dir selbst widersprochen hast. Du fühlst, dass du nicht mehr glaubwürdig bist, dass der:die Richter:in wissen wird, dass du versucht hast zu lügen, dass dich dieses Verhalten verdächtig macht. Ob du nun in der Hoffnung gestehst, zu retten, was noch zu retten ist, oder ob du weiterhin leugnest - die Beweise für deine Lügen stehen fest und werden im Prozess gegen dich verwendet. Die Versuchung ist stark, nun ein volles Geständnis zu liefern.

Die Strategie des “Treibsands” zielt darauf ab, dich lügen zu lassen oder dich sogar darin zu bestärken. Sie beginnt immer mit einer Testfrage, deren Antwort die Polizei bereits kennt, um zu sehen, ob du im weiteren Verlauf des Gesprächs versuchen wirst, sie anzulügen. Wenn das der Fall ist, wirst du dazu gedrängt, immer mehr falsche Antworten zu geben. Und jedes Mal, wenn du dir etwas Neues ausdenkst, stellen dir die Ermittler:innen neue Fragen. Im Klartext: Du verstrickst dich immer tiefer, in deine eigenen Lügen. Je mehr Lügen du erzählst, desto größer ist die Gefahr, dass du dich in Widersprüche verwickelst oder dass deine Lügen im Gegensatz zu bereits gesammelten Informationen (Zeugenaussagen, Spuren, Indizien usw.) stehen.

**Lügen aus dem Stegreif erfordert eine hohe Konzentrationsfähigkeit, viel Fantasie und ein sehr gutes Gedächtnis.**

Die Polizei schreibt das gesamte Verhör mit, während du nur selten die Möglichkeit hast, dir Notizen zu machen. Und wenn dir zwei oder drei Wochen später dieselben Fragen noch einmal gestellt werden, musst du bis ins kleinste Detail ähnlich antworten.

Wenn du dir selbst widersprichst, verlierst du an Kohärenz und Glaubwürdigkeit, bis schließlich deine Lüge in Stücke zerfällt. Da du die Beweise nicht kennst, welche die Polizei gegen dich gesammelt hat, kannst du auch nicht wissen, ob du dich mit deiner Lüge rettest oder dir Schaden zufügst. Das Ziel der Polizei ist es, dich zum Lügen zu bringen und dann deine Lüge zu zerschlagen. Dann werden dir die Polizist:innen zeigen, dass sie wissen, dass du lügst und nicht mehr glaubwürdig bist. Ob du nun gestehst- um zu retten, was noch zu retten ist - oder weiterhin leugnest, deine Lügen sind bewiesen und werden im Prozess gegen dich verwendet.

Schweigen ist eine bessere Form der Selbstverteidigung als Lügen. Der Versuch, die Wahrheit durch eine Lüge zu verbergen, bedeutet, das Risiko einzugehen, dass viel mehr Informationen ans Licht kommen, als wenn du durch Schweigen geschützt bleibst.

### Trichter & Verpflichtung

\[Situationsbeschreibung\] 

> Die beiden Inspektor:innen des Tages, sitzen in demselben Raum, mit betonierten Wänden und haben eine freundliche, offene und leichte Haltung. Sie stellen dir zunächst offene Fragen, die weit von dem Thema entfernt sind, das dich in ihren Räumen gefangen hält. Das klingt nach einem freundlichen Gespräch, das in einem Café geführt wird, und nicht nach einem Verhör unter Zwang. Ihre Fragen scheinen unverbindlich zu sein, sind nicht bedrohlich, du kannst sie wahrheitsgemäß beantworten, ohne befürchten zu müssen, dass die Antworten gegen dich verwendet werden. Und du hast Angst, dass, wenn du dich weigerst zu antworten, ihre freundliche Haltung verschwindet und die Dinge kompliziert werden. Nur wird das Thema, nach und nach, geschickt an sensible Punkte herangeführt. In dir läuten die Alarmglocken und du zögerst mit der Antwort und wirst ausweichend. Schnell wird dir das Gefühl gegeben, dass deine Verhaltensänderung erkannt wurde und diese dich verdächtig macht. Als du dich weigerst zu antworten, sind die Inspektor:innen erstaunt über dein Schweigen und weisen dich darauf hin, dass du bisher immer geantwortet hast und wenn du jetzt schweigst, bedeutet das, dass du etwas zu verbergen hast und du somit schuldig bist. Die Falle schnappt zu.

Offene Fragen ohne besondere Herausforderungen zu Beginn eines Verhörs zu stellen, ist unabhängig von deinem Profil eine ganz gewöhnliche Praxis. Wenn du sie beantwortest, haben die Inspektor:innen bereits ein Druckmittel gegen dich in der Hand: “Warum haben Sie unsere Fragen bisher beantwortet, weigern sich aber, dies jetzt zu tun? Haben Sie in diesem Thema etwas zu verbergen?”

Mit dieser Strategie versucht die Polizei, das sogenannte “Verpflichtungs-Phänomen” zu erzeugen: Eine emotionale Beteiligung deinerseits am Verhörprozess, eine partizipative Haltung. Je mehr Fragen du beantwortest und Informationen du preisgibst, desto schwieriger wird es, deine Haltung zu ändern und zu erklären, dass du die Diskussion nicht mehr fortsetzen willst. Eine Änderung der Einstellung erfordert, dass du deine bisherigen Entscheidungen in Frage stellst und umkehrst, was schwierig sein kann.

Wie bei den vorherigen Strategien ist es auch hier das Schweigen, das dir in dieser Situation am meisten hilft. Wenn du dich von Anfang an weigerst, auf Fragen der Polizei zu antworten, auch wenn sie harmlos erscheinen, nimmst du den Inspektor:innen, die dich mit der “Trichterstrategie” in die Falle locken wollen, den Wind aus den Segeln. Ohne eine Antwort deinerseits gibt es keine Falle, die sie dir stellen können.

\[Buch\]

> Der Genosse Knut machte nur weiter Notizen, bis er sich endlich laut aufseufzend zurück lehnte und wieder die Rolle des Verwunderten spielte: Na ja, sei es, wie es sei, er jedenfalls könne nicht verstehen, dass jemand allein einer Frau zuliebe alle Brücken hinter sich abbrechen wolle. Sicherlich, die DDR sei kein Schlaraffenland, man müsse zupacken, wolle man sich einen gewissen Wohlstand schaffen, aber das sei ja schließlich überall so. Anderseits jedoch gebe es in der DDR keinerlei Ausbeutung und keine ungewisse Zukunft für den, der arbeiten wolle. In der Ellenbogengesellschaft des Westens, das bestätigten ja selbst westliche Gesellschaftskritiker, versuche doch jeder, den anderen beiseite zu drücken, nur um selbst vorwärts zu kommen. Das sei ja schon ein richtiger Krieg, jeder gegen jeden, der sich da in der Bundesrepublik abspiele. Ob er, Lenz, sich ein solches Leben denn wünsche, ob er wolle, das die Welt in diesem Stadium stehen bleibe?
> 
> — Haben Sie im Studium denn nicht gelernt, dass der Mensch im Kapitalismus nur nützliches Werkzeug der Produktionsmittelbesitzer ist und bloß deshalb ernährt wird, damit man ihn weiter ausbeuten kann? Wir hingegen schaffen eine Welt, in der der Mensch die gestalterische Kraft ist, wir bauen ein wahrhaft demokratisches und sozialistisches Deutschland auf. Ist daran mitzuarbeiten denn keine lohnende Sache?
> 
> Vorsicht Manne! Das ist wieder mal so Abgeklopfte. Wer ist wer? Sie sind noch nicht zufrieden mit dem, was sie bisher über dich in Erfahrung gebracht haben.
> 
> — Sie schweigen?
> 
> Lenz schwieg. Es gab für alles eine Grenze; über die eine hatte die Stasi ihn nicht gelassen, über die andere ließ er die Stasi nicht. Er war doch kein Automat, oben steckst du ein paar Zigaretten rein, unten plappert es los.
> 
> — Also finden Sie alles wunderbar bei uns? Die Frage ist nur: Weshalb wollten Sie dann weg?
> 
> Lenz wollte weiter schweigen, aber dann juckte es ihn doch, den Mund aufzumachen.
> 
> — Vielleicht bin ich das blöde Känguru, das aus dem Zoo hüpft und damit alles aufgibt – Fressen, Ruhe, Geborgenheit -, nur weil es eine ferne Ahnung von Australien hat.
> 
> — Zoo! Aha! Der Leutnant notierte das Wort.
> 
> — Sie haben sich bei uns also “gefangen” gefühlt?
> 
> — Siehst, Lenz, so leicht verrät man sich “Sagen wir mal so, eingeengt”.
> 
> — Und was hat Sie eingeengt?

Auszug aus dem Buch, Krokodil im Nacken, Klaus Kordon, 2008

### Mechanismus der unbewussten Akzeptanz

> − Sie heißen Georges Jackson?[^Hommage an Georges Jackson (1940-1970), Afroamerikaner, der mit achtzehn Jahren wegen eines Bagatelldelikts zu einem Jahr Gefängnis inhaftiert wurde. Er kam nie wieder aus dem Gefängnis heraus und starb dort im Alter von 30 Jahren, ermordet von einem Wärter. George Jackson ist eine Symbolfigur für den Kampf von Gefangenen gegen das Gefängnissystem und gegen Rassismus. Um seine Texte zu lesen, siehe: 1/ Soledad Brother, The Prison Letters of George Jackson, 1970 und 2/ Blood In My Eye. 1972.]
> 
> − Ja − Haben Sie in der Vergangenheit schon einmal mit der Polizei zu tun gehabt?
> 
> - Ja.
> 
> − Wenn ich mir Ihre Akte ansehe, stelle ich fest, dass Sie verheiratet sind und zwei Kinder haben, ist das richtig?
> 
> - Ganz genau.

Diese Fragen, die wie eine protokollarische Einleitung zum Verhör gestellt werden, wirken völlig harmlos oder sogar unnötig.

Dennoch sind sie das Herzstück einer Manipulationsstrategie, die aus dem Bereich des Marketings stammt: Der unbewusste Akzeptanzmechanismus. Diese Technik wird ganz zu Beginn eines Verhörs eingesetzt, um den Dialog mit der befragten Person einzuleiten. Die Polizist:innen kennen die Antworten bereits und haben kein Interesse daran, dass du sie bestätigst. Der Nutzen dieser Strategie liegt darin, dass sie dich dazu bringt, eine Frage zu bejahen. Indem du scheinbar unbedeutende Dinge mit “Ja” beantwortest, bringst du dich unbewusst in die Lage, “Ja” zu weitaus wichtigeren Tatsachen zu sagen.

Im Verkauf wird diese Technik häufig angewandt, z. B. Beim Verkauf am Telefon oder auf der Straße. Die Marketingtheorie besagt, dass es eine:m Verkäufer:in leichter fällt sein Produkt zu verkaufen, wenn er:sie das “Verkaufsopfer” dazu bringt, dreimal “Ja” zu sagen. Hier, wie auch bei einem Verhör, ist das Ziel, die angehörte Person zu einer positiven, kooperativen Haltung für den weiteren Verlauf der Ereignisse zu ermutigen.

Um zu verhindern, dass du von dieser Strategie beeinflusst wirst, ist es am besten, gegenüber den Polizist:innen, die die Befragung durchführen, von Anfang an deine nicht kooperative Haltung zu

verdeutlichen. Am einfachsten ist es, bereits auf die allererste Frage mit dem Satz “Ich mache von meinem Recht zu schweigen Gebrauch” zu antworten und diesen Satz bei jeder Frage zu wiederholen.

### Beziehung humanisieren & der “Rettungsring”

\[Situationsbeschreibung\]

> Du bist seit Stunden oder gar Tagen in einer unbequemen und kalten Gefängniszelle eingesperrt, bist gestresst, unter Druck gesetzt, zweifelst an deiner Zukunft und sorgst dich darum, wie es deinen Angehörigen geht. Die Polizist:innen, mit denen du in Kontakt gekommen bist, haben sich kalt, aggressiv und feindselig verhalten. Die Einsamkeit und der Mangel an sozialen Kontakten belasten dich. Plötzlich lächelt dich ein:e Polizistin: an, spricht freundlich und wohlwollend mit dir, zeigt Verständnis und beruhigt dich hinsichtlich deiner Situation und bietet dir sogar bis dahin abgelehnte “Gefälligkeiten” an (ein Glas Wasser, Essen, ein Telefonanruf, ein Buch). Doch schon bald stellt dir der:dieselbe Polizist:in unangenehme Fragen und als du dich weigerst, sie zu beantworten, zeigt er:sie sich persönlich enttäuscht und macht dir Schuldgefühle “nach allem, was ich für Sie getan habe – wo ich doch an Ihre Aufrichtigkeit glaubte”. Wenn du deine unkooperative Haltung beibehältst, werden die Gefälligkeiten und die Freundlichkeit so schnell verschwinden, wie sie aufgetaucht sind.

In Polizeigewahrsam oder Untersuchungshaft hast du fast ausschließlich sozialen Kontakt mit den Inspektor:innen, die dich verhören, und den Polizist:innen, die dich in der Zelle festhalten. Diese Situation wird mit einer Strategie ausgenutzt, die zynischerweise als “Rettungsring”-Phänomen bezeichnet wird. Wenn inmitten einer feindseligen Umgebung plötzlich jemand die Hand ausstreckt und sich freundlich verhält, ist es schwierig sich dieser Person gegenüber nicht verpflichtet zu fühlen. Dieses Gefühl wird dazu benutzt, dich emotional zu erpressen. Der:die Polizist:in wird seine:ihre tiefe Enttäuschung zum Ausdruck bringen, vor allem nachdem er:sie sich persönlich und emotional engagiert hat, in der Hoffnung, das Schuldgefühl der Person zu verstärken, die sich weigert, mit ihm:ihr zusammenzuarbeiten.

Je nach deinem emotionalen und sozialen Profil, kann diese Strategie extrem verwirrend und destabilisierend sein. Die Angst, die einzige Person zu enttäuschen, die in den letzten Tagen dir gegenüber ein wenig Menschlichkeit gezeigt hat, und das Gefühl, ihr für ihre “Gefälligkeiten” etwas schuldig zu sein, drängen dich in die von dieser Strategie beabsichtigte Richtung: Geständnis und Mitarbeit beim Verhör. Behalte in einem solchen Moment die völlige Asymmetrie dieser Situation im Auge. Nachdem die Polizei dich aus der Freiheit gerissen und in einen von der Außenwelt isolierten Raum gesperrt hat, versucht sie dich emotional zu erpressen, indem sie dir lächerliche Gefälligkeiten wie ein Glas Wasser oder eine Zigarette erweist. Vergiss nicht, dass die Ermittler:innen, die sich dir gegenüber “nett” verhalten, dies nicht aus Zufall oder Menschlichkeit tun, sondern weil es Teil einer Manipulationsstrategie ist, deren Ziel du bist.

[Box]

> **Stockholm Syndrom**
> 
> Der Begriff “Stockholm-Syndrom” entwickelte sich in der Psychologie in Anlehnung an einen Banküberfall mit Geiselnahme in Stockholm im Jahr 1973. Nachdem sie sechs Tage in der Gewalt der Bankräuber verbracht hatten, zeigten die Geiseln eine starke Solidarität mit den Bankräubern. Sie schützten die Banditen während des Polizeiangriffs mit ihren Körpern, verweigerten die Aussage im Prozess, verteidigten sie, legten Geld für ihre Anwaltskosten zusammen und besuchten sie im Gefängnis.[^Die Tatsache, dass die Geiseln nicht nur aus Verbundenheit und Identifikation mit ihren Entführern handelten, sondern auch aus Feindseligkeit gegenüber den Polizeibehörden aufgrund ihres Verhaltens während den sechs Tagen, wird nur in wenigen psychologischen Studien aufgegriffen und analysiert, ist aber dennoch eine Überlegung wert.]
> In der Psychologie bezeichnet der Begriff “Stockholm-Syndrom” seither ein Phänomen der paradoxen Bindung von Opfern von Freiheitsberaubung an ihre Angreifer:innen. Die Tatsache, dass du die Objektivität der Situation aus den Augen verlierst und dich mit der Person identifizierst, die dich dennoch unterdrückt. Das Gefühl der Dankbarkeit des Opfers, nicht misshandelt zu werden (oder nicht noch mehr misshandelt zu werden als in der aktuellen Situation), überwiegt das negative Gefühl, seiner Freiheit beraubt und faktisch unterdrückt zu werden. Das Stockholm-Syndrom kann als unbewusste Manifestation des Überlebens gesehen werden: Die betroffene Person kann, indem sie die Sympathie des:der Angreifer:in auf sich zieht, glauben, teilweise außer Gefahr zu sein oder sogar die Gefühle des:der Angreifer:in beeinflussen zu können.
> 
> Manipulationsstrategien und -techniken, die auf der Vermenschlichung der Beziehung zwischen Polizei und Beschuldigten und der emotionalen Ansteckung aufbauen, versuchen, dieses Phänomen zu erzeugen. Wenn du Polizist:innen dankbar dafür bist, dass sie dir Wasser und ein Buch gebracht haben, nachdem sie dich in eine Zelle gesperrt haben (und dir somit das Wasser und die frei verfügbaren Bücher vorenthalten haben), handelt es sich um eine Form des Stockholm-Syndroms.


### Emotionale Ansteckung

\[Situationsbeschreibung\]

> Du sitzt auf einem unbequemen Stuhl und hörst dem Polizisten zu, der dir von den Kollateralschäden erzählt, die deine Taten verursacht haben, von der Verwirrung und der Not der Menschen, deren Schaufenster zertrümmert wurden. Du spürst, wie Schuldgefühle, Scham und Zweifel in dir aufsteigen. Was, wenn er die Wahrheit gesagt hat? Du hattest nicht die Absicht, Menschen zu verletzen, als du den Stein in die Auslage eines Metzgereibetriebs geworfen hast, sondern wolltest gegen Tierausbeutung protestieren. Der Inspektor ändert seinen Tonfall und wird wütend. Jetzt wirft er dir vor, ein Feigling zu sein, nicht zu deinen Überzeugungen zu stehen und ein:e Mitläufer:in in der Gruppe zu sein. In dir steigt nun Wut und Empörung auf. Du spürst den Drang, dich zu rechtfertigen, dich zu erklären und dich zu verteidigen. Aber du kannst dich gerade noch zurückhalten. Da wird der andere anwesende Polizist hart und droht dir: “du wirst einen hohen Preis zahlen, deine Familie wird dein Handeln nicht verstehen, du könntest sogar deinen Job verlieren. Und wenn du dich weiterhin in Schweigen hüllst, wirst du noch eine Woche hier eingesperrt bleiben”. Seine Worte schüren Ängste, die bereits in dir vorhanden sind. Du bekommst Angst, die Emotionen sprudeln und die Tränen fließen, während du anfängst, ihre Fragen zu beantworten.

Das Hin- und Herwechseln von einer Emotion zur nächsten führt zu emotionaler Erschöpfung. Diese polizeiliche Strategie besteht darin, deine Emotionen durch das Verhalten der Ermittler:innen zu beeinflussen. In der Psychologie wird dies als emotionale Ansteckung bezeichnet. Der emotionale Zustand der Personen, die uns gegenüberstehen, beeinflusst unseren eigenen emotionalen Zustand.

Wenn du zum Beispiel eine aggressive Person triffst, kann dich das in einen Zustand der Wut, der Angst oder des Stresses versetzen, während die Begegnung mit einer ruhigen und sanften Person Ruhe, aber vielleicht auch Misstrauen auslöst. Mit diesem Mechanismus ist es möglich, deinen emotionalen Zustand zu beeinflussen, der sich mit dem Rhythmus deiner Gesprächspartner:innen und deren Verhalten ändert.

Die Polizist:innen werden herausfinden, welcher Gefühlszustand für dich die größte Verletzlichkeit darstellt und dich aus der Bahn werfen wird. Sind es Scham und Schuld, die den Wunsch nach Wiedergutmachung und Beichte durch ein Geständnis wecken? Ist es die Wut, in der du nicht mehr die Nerven behalten kannst? Oder die Angst, die dich zu einer Aussage drängen wird? Die Untersuchung deines Verhaltens während des Polizeigewahrsams/ der Untersuchungshaft, die Interaktionen, die du während der gesamten Zeit deiner Festnahme und Inhaftierung hast, können der Polizei wertvolle Hinweise auf diese Frage geben.

Die emotionale Ansteckung wird durch verbale und nonverbale Kommunikation übertragen. Eine offene und fast fröhliche Haltung eines:r Inspektor:in (Lächeln, offene Arme) wird dich anders beeinflussen als ein distanziertes und feindseliges Verhalten (verschränkte Arme, Seitenblick).

\[Polizei-Zitat\]

> Es gibt also echte Verärgerung, die aber eher selten vorkommt, und es gibt falsche Verärgerung. Es ist wie im Kino, es gibt Momente, in denen es zur Show wird. Das ist ein Bluff. Das heißt, du wirst wütend, du wirst lauter, du schreist, du überrennst sie.

### Naive Polizist:innen

\[Situationsbeschreibung\]

> Als der:die Inspektor mit der Befragung beginnt, denkst du dir schnell, dass er wahrscheinlich noch nicht viel Erfahrung in diesem Bereich hat. Er stellt Fragen, die dir wenig relevant erscheinen oder sogar völlig am Thema vorbeigehen. Offensichtlich ist er auf der ganzen Linie falsch und nicht nur unerfahren, sondern auch inkompetent. Das beruhigt dich und gibt dir die Zuversicht, dass du es schaffen wirst. Du versuchst es mit einer Lüge, dann mit einer weiteren und gefangen in deinem Optimismus gibst du eine Information zu viel preis.
Die Haltung des Inspektors ändert sich; er zeigt dir einen Widerspruch in deinen Lügen auf und gibt dir zu verstehen, dass er weiß, dass du lügst, und dass er das beweisen kann. Die Fragen werden sehr präzise, indem sie sensible Themen berühren. An seinem veränderten Gesichtsausdruck erkennst du, dass er mit dir gespielt hat und dich Vertrauen gewinnen ließ, damit du Fehler machst..

Die Strategie der Naivität, wird zu Beginn des Verhörs eingesetzt, wenn du zum ersten Mal mit den Polizist:innen in Kontakt kommst. Bei dieser Strategie ist es oft ein:e junge:r Polizist:in, der:die aufzeigt, dass er:sie wenig Ahnung hat, neben der Spur ist oder sogar ganz und gar dumm. Das Ziel ist es, dich in Sicherheit zu wiegen und dich dazu zu bringen, Fehler zu machen. Du könntest z. B. eine wichtige Information preisgeben, weil du unterschätzt hast, dass die Inspektor:innen diese gegen dich verwenden könnten. Oder du lügst selbstbewusst in der Annahme, dass deine Lüge von den unerfahrenen Ermittler:innen nicht durchschaut wird.

Diese Strategie erzeugt bei dir nicht nur einen Mangel an Misstrauen und Abwehr, sondern kann dich auch verunsichern, wenn du merkst, dass die Polizist:innen mit dir gespielt haben, dass die Situation, wie du sie eingeschätzt hast, nicht der Realität entspricht und dass du dich selbst in Gefahr gebracht hast. Das kann deine Sichtweise plötzlich umkehren. Nachdem du gedacht hast, dass du den Polizist:innen aufgrund ihrer sichtbaren Naivität überlegen bist, fühlst du dich nun unterlegen, da du vielleicht über den Tisch gezogen wurdest.

\[Polizei-Zitat\]

> **Es ist sehr gut, Polizist mit diesem autoritären Charakter zu sein, solange man damit spielen kann. Zuerst bin ich autoritär und dumm, aber dann plötzlich wirke ich offen und freundlich. Genau das ist destabilisierend und eröffnet neue Möglichkeiten.**


### Synchron-Verhör

\[Situationsbeschreibung\] 

> Du und deine Freund:innen wurden alle gleichzeitig verhaftet, drei Tage nachdem ihr gemeinsam eine Sabotage bei dem Sitz eines Großkonzern durchgeführt habt. Die Polizei verdächtigte euch offensichtlich. Glücklicherweise habt ihr euch auf eine gemeinsame Version geeinigt, die ihr im Falle einer Verhaftung erzählen würdet. Während des Verhörs beantwortest du die Fragen, indem du die Geschichte erzählst, die ihr euch ausgedacht habt. Kurze Zeit später kommen die Polizist:innen wieder und berichten dir, dass deine Geschichte ein paar kleine Abweichungen von der Geschichte deiner Freund:innen aufweist. Du reagierst darauf, indem du versuchst, die Stücke so gut es geht zusammenzusetzen. Aber es entsteht eine Spannung in dir. Wie kannst du sicher sein, dass deine Aussagen mit dem übereinstimmen, was die anderen erzählen? Noch später, beim dritten Verhör, wird dir durch die Fragen der Polizist:innen klar, dass es sich nicht mehr um einfache Diskrepanzen zwischen euren Versionen handelt, sondern um echte Widersprüche, die auf eine Lüge hindeuten. Zu spät erkennst du, dass deine Freund:innen genau das gleiche Verhör in der gleichen Reihenfolge durchlaufen haben wie du und dass es den Inspektor:innen auf diese Weise gelungen ist, aus den zunächst geringfügigen Unterschieden in der gleichen Geschichte, zwei gegensätzliche Versionen zu machen.

Synchronisierte Befragung ist eine Strategie, die speziell dann eingesetzt wird, wenn die Polizei mehr als eine Person zu einem bestimmten Ereignis befragt. Dabei wird geprüft, ob die verschiedenen Verdächtigen zuvor eine gemeinsame falsche Geschichte vereinbart haben, um die Wahrheit zu verschleiern. In einem solchen Fall werden du und deine Freund:innen getrennt befragt, aber nach einem genau gleichen Schema. Die Fragen werden auf die gleiche Weise und in der gleichen Reihenfolge gestellt, um die gleiche Interpretation zu ermöglichen. Es wird leicht zu erkennen sein, wo es Abweichungen gibt, an welche kleinen Details ihr nicht gedacht habt, welche Fragen ihr nicht vorhergesehen habt und welche euch überrumpelt haben.

Nach dem Vergleich der verschiedenen Versionen, wird eine zweite Befragung wieder gleichzeitig und identisch bei allen Befragten durchgeführt, ohne dass sie sich untereinander absprechen konnten. Auf diese Weise ist es für die Polizei einfach, die Unterschiede zwischen den Aussagen zu vertiefen, bis eure Geschichte an Glaubwürdigkeit verliert und sie durch den Vergleich beweisen können, dass es sich um eine vorbereitete Version handelt.

Zu große Ähnlichkeiten in einer Geschichte, die von mehreren Personen erzählt wird, können ebenso verdächtig sein wie zu große Lücken. Zwei Personen, die einen gemeinsamen Moment erlebt haben, werden ihn jeweils aus ihrer eigenen Erinnerung heraus erzählen, die sich durch die Gefühle und Empfindlichkeiten gebildet hat, die jede Person auf unterschiedliche Weise erlebt hat. Das Ergebnis würde deutlich anders aussehen, als wenn beide Personen die gleiche, auswendig gelernte Geschichte erzählen.

### Körperliche Veränderungen aufzeigen

\[Situationsbeschreibung\]

> Das Verhör beginnt mit offenen Fragen zu Themen, bei denen nicht allzu viel auf dem Spiel steht. Um dich unschuldig dar zustellen, hast du dich entschieden, sie zu beantworten. Plötzlich stellt dir eine Polizistin eine Frage zu einem Thema, das dir sichtlich unangenehm ist. Während du dein Unbehagen überspielst, versuchst du zu antworten, indem du dem Thema ausweichst. Die Fragen werden weniger bedrohlich, bis die Ermittlerin dir eine Reihe konkreter Fragen stellt, die sich immer noch auf das Thema beziehen, das dir bereits peinlich war und an das du dich nicht heranwagen möchtest. Du versuchst wieder auszuweichen, aber diesmal machen die Polizist:innen dir klar, dass sie dein verändertes Verhalten bei jedem Thema bemerkt haben und wissen, dass du etwas verbergen willst. Du spürst, wie du rot wirst. Du zögerst, stotterst und sagst ihnen schließlich, dass du diese Frage nicht beantworten möchtest. Sie antworten: “ Das ist Ihr gutes Recht, aber Sie müssen wissen, dass dies vor Gericht als Hinweis auf eine Schuld gewertet wird. Warum hast du sonst bei den ersten Fragen zugestimmt und bei diesen plötzlich nicht mehr? Sie haben etwas zu verbergen, und wir wissen das. Ein Geständnis wird Ihnen Erleichterung verschaffen und allen Zeit sparen.”

Bei einem Verhör achten die Inspektor:innen sowohl auf deine Körpersprache als auch auf deine verbalen Antworten. Das gibt ihnen viele Hinweise darauf, welche Themen für dich sensibel sind, ob du nervös oder gestresst bist. Indem sie dir deine Verhaltensänderungen zeigen, z.B. wenn sie von Personen sprechen, die als Kompliz:innen verdächtigt werden, versuchen die Polizist:innen, dir das Gefühl zu geben, dass dein Körper dich verrät und du nicht mehr glaubwürdig bist. Warum wirkst du aufgeregt, wenn du auf diese Frage antwortest, obwohl du kurz zuvor noch ruhig und entspannt gewirkt hast? Das Ziel ist es, dich zu verunsichern, dich an dir selbst zweifeln zu lassen, an deiner Fähigkeit, Dinge vor der Polizei zu verbergen und dir das Gefühl zu geben, dass “alles schon gelaufen ist”. Die Tatsache, dass du auf keine ihrer Fragen antwortest, schützt dich stark vor dieser Strategie. Ohne einen Vergleich zwischen deinen verschiedenen Antworten und Reaktionen auf die Fragen ist es nicht möglich, deine potenzielle Verhaltensänderung gegen dich zu verwenden \[Seite 94\].

### Maximierung und Minimierung

Seit Beginn deines Polizeigewahrsams zerbrichst du dir den Kopf darüber, welche Folgen eine Verurteilung haben könnte.

\[Situationsbeschreibung\]

> Droht dir eine hohe Geldstrafe? Eine Gefängnisstrafe? Wenn ja, wie lange und unter welchen Bedingungen? Werden deine Angehörigen Verständnis zeigen? Was ist mit deinem Arbeitgeber? Unbewusst konzentriert sich dein Gehirn auf die schlimmste aller Möglichkeiten und du stellst dir vor, wie du eine Verurteilung zu mehreren Monaten Gefängnis, verbunden mit der Ablehnung deines sozialen Umfelds, erleben würdest. Dein Stress und deine Ängste steigen. Doch während des Verhörs hörst du erleichtert, wie die Inspektor:innen sagen, dass dir nur eine Geldstrafe droht, dass sie selbst in deinem Alter schon viel Schlimmeres getan haben, dass das, was dir vorgeworfen wird, zwar illegal, aber im Grunde ziemlich banal ist und dass “Dealer die wahren Kriminellen sind”, und was du im Vergleich dazu getan hast “Das ist nicht so schlimm”. Das zu hören entspannt dich und der Druck in dir sinkt. Nachdem du dir das Schlimmste ausgemalt hast, erscheint dir die Aussicht auf eine einfache Geldstrafe weniger gefährlich, fast schon lebensrettend. Du fühlst dich weniger herausgefordert, dich zu schützen, du lässt deine Wachsamkeit sinken und beantwortest ihre Fragen, bis du wieder in die Zelle gebracht wirst. In deinem Kopf tauchen Zweifel auf. Was, wenn sie gelogen haben?

Die Polizei listet drei Ängste auf, die die Befragten davon abhalten können, ein Geständnis abzulegen:

- Angst vor rechtlicher Repression, vor der drohenden Strafe. 
- Angst vor sozialer Repression, vor der Ablehnung durch Familie und Verwandte und vor dem Verlust des Arbeitsplatzes. 
- Angst vor den eigenen Schuldgefühlen, davor, gegen die eigenen Moralvorstellungen verstoßen zu haben und sich zu schämen. Dies kann verhindern, dass man sich selbst und damit auch anderen gegenüber seine Schuld eingesteht.

Indem sie die Tat, die Reaktion des Umfelds und die Schwere der Tat herunterspielen, versuchen die Inspektor:innen, die Ängste, die du haben könntest, zu mindern und dich zu beruhigen, damit du weniger Hemmungen hast, deine Tat zuzugeben. Dir selbst, aber natürlich auch gegenüber den Polizist:innen, die dich verhören. Sie können dir erzählen, dass du rechtlich gesehen nicht viel zu befürchten hast, Verständnis für die Tat zeigen und vor allem für die Gründe, warum du sie begangen hast. Sie werden dir auch versichern, dass deine Familie und Freunde verstehen, was du getan hast, und dir verzeihen werden. Sie werden deine Tat mit “wirklich” schweren Verbrechen vergleichen und dir sagen, dass “du etwas viel Schlimmeres hättest tun können”.

**Es ist normal und legitim, Ängste zu empfinden und ihnen Raum zu geben.**

Dennoch ist es besonders gefährlich (und unnötig), sich diesen Ängsten im Rahmen eines Verhörs in Gegenwart von Polizist:innen zu stellen, die diese Schwächen ausnutzen wollen, um dich zu manipulieren.

Wenn du dich mit den rechtlichen Konsequenzen auseinandersetzen musst, kann dir dein:e Anwält:in oder eine Rechtsberatungsgruppe viel bessere Ratschläge geben. Denk daran, dass du zwischen deinem Besuch bei der Polizei und dem Prozess genug Zeit hast, um dich darüber zu informieren, was dir bei einer Verurteilung droht, sowie Kenntnis von der Ermittlungsakte zu erhalten.

Was die Reaktionen deiner Angehörigen angeht: Wer kann besser als du einschätzen, wie sie auf deine Verurteilung reagieren werden?

Sicherlich nicht die Polizist:innen mit den Scheuklappen einer Institution, die für die moralische Ordnung zuständig ist. Wenn du mit deinen Angehörigen im Vorfeld über die Bedeutung von Begriffen wie “schuldig und unschuldig”, “legal und illegal”, die Funktionsweise der Justiz und die Bedeutung deiner Lebensentscheidungen, auch wenn sie illegal sind, diskutierst und debattierst, kann dies zu gegenseitigem Verständnis beitragen.

Vielleicht hast du Schuldgefühle und bereust die Taten, die du begangen hast und wegen derer die Polizei nun gegen dich ermittelt. Jede:r von uns macht Fehler oder begeht Handlungen, die er:sie später bereut. Die Herausforderung liegt in der Transformation, die durch die Übernahme der Verantwortung für diese Taten, durch ein Gefühl der Empathie und Ehrlichkeit ermöglicht werden kann.

Die institutionelle Justiz zielt nicht darauf ab, den angerichteten Schaden zu beheben oder das erlebte Leid in eine emanzipatorische Veränderung zu verwandeln. Sie zielt in erster Linie darauf ab, Menschen zu bestrafen, die sich schuldig gemacht haben, weil sie gegen die Gesetze verstoßen haben. Eine autoritäre und bürokratische Institution ist keine günstige Grundlage für einen Wiedergutmachungsansatz. Die Wiedergutmachung eines Schadens oder Leids ist viel wahrscheinlicher, wenn sie mit den Betroffenen von den Taten, in individueller Interaktion oder innerhalb einer Gemeinschaft stattfindet (siehe Kapitel “Die Polizei und Justiz überwinden “ \[Seite 145\]).

Diese Strategie kann auch in umgekehrter Richtung angewendet werden. Die Polizist:innen können die Schwere der Tat übertreiben, und dich zu beeinflussen, eine weniger schwere Straftat als die ursprünglich dargestellte zu akzeptieren.

\[Polizei-Zitat\]

> Du erklärst ihm, dass er in einen Fall hineingeraten ist, der ihn überfordert. \[Der Verdächtige\] denkt, dass er in einem gewöhnlichen Fall steckt, aber die Telefone, die er gestohlen hat, wurden für Terrorismus benutzt, und das wusste er nicht. Jetzt sagst du ihm, entweder er erklärt mir, warum er diese gestohlen hat, wie er sie gestohlen hat, mit wem er sie gestohlen hat und das war’s, oder er wird in einen Terrorismusfall verwickelt, das ist seine Entscheidung. Du nutzt das, um dort zu drücken, wo es weh tut, und um an Informationen zu kommen, und dann setzt du ihn unter psychologischen Druck..

### Die Schuld Anderen zuschieben

\[Situationsbeschreibung\]

> Als du besorgt in den Verhörraum kommst, zeigen die Ermittler:innen großes Verständnis für deine Situation. Sie sprechen von deinen Kompliz:innen, die dich unter Druck gesetzt hätten, damit du ihnen beim Hanfanbau hilfst, und die einen größeren Teil der Verantwortung tragen würden als du. Sie sprechen von Gesetzen, die schlecht gemacht sind, da in anderen Ländern der Hanfanbau bereits straffrei sei. Sie sprechen von der Gesellschaft, die Zeit braucht, um zu verstehen, dass Cannabis letztlich nicht gefährlicher ist als Alkohol. Auch sie haben als Teenager Gras geraucht. Sie sagen dir, dass du wenigstens nicht mit Heroin dealst, dass “das echter Dreck ist”. Diese Aussagen bestärken Gedanken, die bereits in dir vorhanden sind. Die Umgebung in diesem Verhörraum, in dem du eingesperrt bist, wird weniger feindselig, du fühlst dich verstanden und gehört, dass du nicht wirklich eine große moralische Verantwortung für das trägst, was dir vorgeworfen wird. In dir entsteht ein unbewusstes Gefühl der Dankbarkeit gegenüber den Polizist:innen, die dazu beigetragen haben, dir die Schuld abzunehmen, die du vielleicht getragen hast. Da du dich in einer günstigen Position fühlst, öffnest du dich und gibst zu, was dir vorgeworfen wird.

Die Schuld auf andere zu schieben (auf die Gesetze, die Gesellschaft, die Kompliz:innen oder – für die zynischsten Polizist:innen – sogar auf die Opfer) zielt darauf ab, die angehörte Person in eine Haltung zu bringen, in der sie die Verantwortung für ihre Taten ablehnt. Wenn du nicht wirklich verantwortlich bist, ist es plötzlich weniger wichtig, die Vorwürfe zu leugnen.

Die Ermittler:innen lassen dich glauben, dass du, da du nicht die volle moralische Verantwortung für deine Taten trägst, bei deiner Verurteilung von mildernden Umständen profitieren wirst. Bei dieser Gelegenheit möchte ich daran erinnern, dass die Arbeit der Polizei lediglich darin besteht, eine Akte mit Informationen zusammenzustellen, die dazu dienen können, dich in einem Prozess zu verurteilen. Alles, was mit der Verurteilung selbst zu tun hat (Art der Strafe, Länge, Milde, mildernde Umstände), geht völlig über ihren Aufgabenbereich hinaus. Für diese Entscheidungen ist das Urteil eines:r Richter:in maßgeblich.

Bei Ermittlungen gegen mehrere Personen ist es eine übliche Strategie, dich glauben zu machen, dass deine Kompliz:innen schuld daran sind, dass die Polizei gegen dich ermittelt. Eine:r von ihnen hat nicht gut genug aufgepasst und eine Information durchsickern lassen oder dich während des Verhörs verpfiffen. Es wird betont, dass es nicht an dir liegt, für einen Fehler zu bezahlen, den jemand anderes begangen hat. Das bringt dich dazu, dich von deinen Kompliz:innen zu distanzieren, indem du der Polizei Informationen gibst.

Eine parallele Strategie besteht darin, dir eine Version der Ereignisse zu präsentieren, in der du von deinen Kompliz:innen mehr oder weniger gegen deinen Willen in die Falle gelockt wurdest und deshalb weniger Verantwortung trägst als die anderen (was zu der Annahme führt, dass du eine geringere Strafe erhältst). Die Gegenleistung für diesen “Deal” ist, dass du die Fragen beantworten musst. Denn wenn du nur unter Druck an der illegalen Aktion teilgenommen hast, warum willst du dann diejenigen schützen, die dich dazu gezwungen haben?

### Ermutigen die Wahrheit zu sagen

\[Situationsbeschreibung\]

> Seit deiner Verhaftung hast du Zweifel und Schuldgefühle wegen der Taten, die du begangen hast. Vielleicht hast du ein bisschen zu schnell und unüberlegt gehandelt. Jetzt bereust du es. Im Idealfall würdest du zu den Menschen gehen, die von deinen Taten betroffen sind, um dich bei ihnen zu entschuldigen und gemeinsam zu überlegen, wie du das Geschehene wieder gutmachen kannst. Aber die Justiz hat sich bereits in die Geschichte eingemischt und anstatt über Wiedergutmachung nachzudenken, wird es wohl eher um Bestrafung gehen. Was werden deine Mitmenschen von all dem denken, welches Bild werden sie von dir haben? Du hast das Bedürfnis, dich zu rechtfertigen, deine Tat und deine Absichten zu erklären. Leider bist du isoliert. Dir gegenüber sitzen nur die üblichen Inspektoren. Sie versuchen Schuldgefühle zu schüren, zeigen aber gleichzeitig Verständnis für deine Situation. Sie wissen, dass du gerade eine schwere Zeit durchmachst und versichern dir, dass das alles bald der Vergangenheit angehören wird. Wenn du gestehst, zeigst du Reue, dein Umfeld versteht, dass du das Geschehene bereust, und du fühlst dich erleichtert, dass du den Weg der Wiedergutmachung einschlagen kannst. Dafür gibt es nichts Besseres, als ein Geständnis abzulegen.

Die banalste aller Verhörstrategien. Nachdem sie Schuldgefühle und Reue in dir geschürt haben, stellen die Inspektor:innen das Geständnis als ersten Schritt zur Erlösung dar. Sie beschwören die Erleichterung, die dir ein Geständnis bringen wird und stützen sich dabei auf das religiöse Modell der Beichte. Deine religiösen Überzeugungen, moralischen Prinzipien und die Vorstellung von Gut und Böse werden hier geschickt ausgenutzt, um Widersprüche zu deinen Taten zu finden und dich in die Position eine:s Schuldige:n zu drängen, der:die um Vergebung bittet. Hier werden Ausdrücke wie “das wird Sie befreien” oder “hol es raus, es wird dir gut tun” verwendet.

Ein weiteres Argument, das vorgebracht wird, lautet: “Je schneller du gestehst, desto schneller wird das alles hinter dir liegen”. In Wirklichkeit ist eher das Gegenteil der Fall. Wie viele zwischenmenschliche Konflikte wären schon längst gelöst worden, wenn die Justiz mit all ihrer Schwerfälligkeit und Langsamkeit nicht ins Spiel gekommen wäre?

### Schmeicheln und Verhöhnen

> “Es ist normal, dass eine Null wie du so schnell geschnappt wird.”
> “Sie müssen wirklich sehr leichtsinnig und naiv gewesen sein, um zu glauben, dass ein solcher Plan eine Chance hat.”
> “In meiner fünfzehnjährigen Karriere habe ich so etwas noch nie erlebt.”
> “ Eine solche Idee ist wirklich gewagt.”
> “Sie haben Ihren Plan sehr klug und weitsichtig ausgearbeitet, so etwas sieht man nicht oft.”

Eindruck zu korrigieren, den sie von dem Gegenüber haben.

Umgekehrt werden Schmeicheleien eingesetzt, um die befragte Person dazu zu bringen, Dinge zuzugeben oder sich selbst zuzuschreiben, die als besonders gewagt oder clever dargestellt werden.

Diese Arbeit der Aufwertung/Abwertung kann über einen längeren Zeitraum hinweg durch subtile Andeutungen erfolgen, die zwischen den Interaktionen mit der Polizei innerhalb und außerhalb des Verhörs verteilt werden. Diese Strategie wird häufig bei Personen angewandt, die als narzisstisch gelten, zu viel oder zu wenig Selbstvertrauen haben oder von einem Gefühl der Überlegenheit erfüllt sind.

Eine Möglichkeit sich zu schützen, besteht darin, sich nicht auf einen emotionalen Kampf mit den provozierenden Polizist:innen einzulassen. Egal, was sie über dich denken oder wie sie die Situation einschätzen, du musst ihnen nichts beweisen. Sie sind Fremde, die nur für einen sehr kurzen Moment in deinem Leben auftauchen und dir gegenüber feindselige Absichten haben. Wenn du unbeeinflussbar bist und dir über deine Lebensentscheidungen im Klaren bist, egal ob sie legal oder illegal sind, kannst du verhindern, dass du dich provozieren lässt.

### Über die Mittel der Ermittlung spekulieren

\[Situationsbeschreibung\]

> Du kommst ziemlich selbstbewusst zum Verhör. Du hast schon einiges erlebt. Die Polizei hat sicherlich nichts in ihrem Besitz, was deine Schuld beweisen könnte. Sie verdächtigen dich, das ist sicher, aber glücklicherweise reicht ein Verdacht nicht aus, um eine Person zu verurteilen. Du brauchst nur nichts zu sagen und solltest damit davonkommen. Die Polizist:innen die dich verhören bemerken schnell dein großes Selbstvertrauen und die Tatsache, dass du dir keine großen Sorgen über die Risiken zu machen scheinst. Dennoch setzen auch sie einen zuversichtlichen Gesichtsausdruck auf. Sie sagen dir, dass es keine Rolle spielt, ob du redest oder nicht. Ist dir nicht bewusst, dass sie Videoaufnahmen von Überwachungskameras haben, auf denen du zu sehen bist? Du hattest zwar den Eindruck, dass du keine Kamera gesehen hast, aber vielleicht hast du dich geirrt und nicht gründlich genug hingeschaut? Die Inspektor:innen sagen dir, dass sie in Kürze die Ergebnisse der Fingerabdrücke, der Schuhabdrücke und der DNA-Proben vom Tatort vorliegen haben werden. Du spürst, wie dir die Hitze ins Gesicht steigt und deine Zuversicht dem Zweifel weicht: Du hattest Handschuhe an und hast deine Schuhe weggeworfen, aber die DNA? Wie kommt die Polizei noch einmal an solche Spuren?
> Hast du dich ausreichend geschützt? Die Polizist:innen fahren fort, sie erwähnen, dass sich bereits mehrere Personen auf ihren Zeugenaufruf im Radio gemeldet haben und dass die Aufzeichnungen deines Telefons ebenfalls recht vielversprechend aussehen. Verdammt, dein Handy! Daran hast du gar nicht mehr gedacht. Hast du alles gelöscht? Und hatte dir nicht mal wer erzählt, dass es Möglichkeiten gibt, ehemals gelöschte Dateien wiederzufinden?
>
> An diesem Punkt hast du dein Vertrauen in dich endgültig verloren. Was ist, wenn sie sowieso die ganze Wahrheit herausfinden? Die Versuchung ist groß, alles zu gestehen, dich kooperativ und reumütig zu zeigen, sowie zu hoffen, dass du bei der Verhandlung ein wenig Gnade erlangst.

Ich habe bereits erwähnt, dass du als Befragte:r keinen Zugang zu den Ermittlungsakten hast und daher nicht weißt, was sich darin befindet. Außerdem hast du, sofern du dich nicht ernsthaft damit auseinandergesetzt hast, wahrscheinlich keine klaren Vorstellungen von den Mitteln und dem rechtlichen Rahmen der Polizei. Kurz gesagt: Du weißt nicht, was die Polizei weiß und welche Kompetenzen sie haben, um ihre Arbeit zu tun. Diese Unwissenheit wird ausgenutzt. Die Inspektor:innen versuchen, dich glauben zu machen, dass ihnen alles möglich ist, dass sie über unbegrenzte Mittel, Ressourcen und Zeit verfügen, dass sie bereits eine ganze Reihe von materiellen Indizien besitzen oder diese mit Sicherheit finden werden. Das Ziel dieser Strategie ist es, dich in eine der beiden folgenden Haltungen zu bringen:

- Resignation: “Alles ist verloren, die Bullen wissen schon alles, es macht keinen Unterschied mehr, ob ich jetzt mit ihnen rede, also was nützt es, sich zu wehren”. 
- Rechtfertigung: Die Polizist:innen wollen den Eindruck erwecken, dass es nicht wirklich wichtig ist, was du sagst (oder nicht sagst), dass sie dich nur aus Routine verhören, aber dass ihre Version bereits feststeht. Dieses Gefühl kann zu einer starken Motivation führen, dich zu rechtfertigen und die Fakten richtig zu stellen, indem du ihnen eine Erklärung lieferst. Und damit mehr Informationen preisgibst, als sie bereits haben.

## 7. Die verschiedenen Fragearten

Es gibt verschiedene Arten, eine Frage zu formulieren. Abhängig von der angestrebten Strategie oder je nachdem, was die Verhörenden für Informationen übermitteln wollen, wird die Art der Fragen angepasst. Die folgende Liste umfasst die Haupttypen der Frageformulierung.

### Offene Fragen

Offene Fragen decken ein Thema auf besonders breite Weise ab. Sie zielen darauf ab, fliessende und nicht konkrete Informationen zu sammeln. Sie fördern eine freie Ausdrucksweise, die eher einer Diskussion als einer konkreten und beschränkten Antwort gleicht. Die Verhörenden verwenden sie, um den Anschein zu erwecken, wirklich am Thema interessiert zu sein und um einem konfrontativen Schema auszuweichen. Ganz im Gegenteil, offene Fragen ermutigen zu einer kollaborativen Haltung deinerseits.

Während einem Verhör wird diese Art der Fragen eher am Anfang gestellt, um sich ein Bild von der Person, die einem gegenübersitzt und deren Denkschemen zu machen. Zudem ermöglichen offene Fragen, die Konturen der nachkommenden Fragen zu zeichnen, welche viel präziser formuliert sind. Diese Fragen sind somit Teil der “Trichterstrategie”»Trichter & Verpflichtung», page 62\].

*Beispiele:*

- Was denken Sie über diese Situation?
- Was können Sie mir über dieses Thema sagen?
- Mögen Sie Ihre Arbeit? Kino? Sport?
- Wie läuft normalerweise Ihr Tag ab?

### Geschlossene Fragen

Hierbei handelt es sich um Fragen, die auf spezifische und präzise Informationen abzielen. Sie lenken das Verhör auf einen bestimmten Punkt hin. Häufig können sie mit kurzen Antworten wie ja oder nein beantwortet werden. Wenn diese Fragen gezielt zwischen offenen Fragen gestellt werden, schaffen die geschlossenen Fragen einen Überraschungseffekt, der dich destabilisieren und dich an deine Grenzen bringen wird. Im Rahmen der “Good cop, bad cop” Strategie werden diese Fragen vom bösen Cop gestellt \[Seite 57\]. Es sind auch diese Typen von Fragen, welche dich dazu verleiten sollen, mit Ja zu Antworten und so die Strategie des “Mechanismus der unbewussten Akzeptanz” zu aktivieren \[Seite 64\].

*Beispiele:*

- Wer hat Ihnen dieses Geld gegeben?
- Haben Sie gestern X gesehen?
- Waren Sie letzte Woche in Paris?

### Spekulative Fragen

Diese Fragen werden verwendet, um dir den Eindruck zu geben, dass die Polizist:innen bereits einen Teil der Antwort kennen oder zumindest um dich über ihren Wissensstand spekulieren zu lassen. Es sind Fragen, die dir dank dem Wort “oder” oftmals zwei Antwortmöglichkeit geben. Sie orientieren sich somit spezifisch auf die erwartete Antwort, was ein gutes Werkzeug für die Neuauslegung des Gesprächs darstellt.

*Beispiele:*

- Haben Sie Herrn Bertrand vor oder nach seiner Abreise gesehen?
- Gibt es einen Grund weshalb ein Nachbar ausgesagt hat, dass Ihr Auto in der Nähe des Tatorts geparkt war?

### Testfragen

Wenn die Verhörenden wissen wollen, ob du vor hast dich auf Basis von Lügen zu verteidigen, werden sie dir Testfragen stellen, um ihre nachfolgende Strategie auswählen zu können. Es handelt sich dabei um eine Frage, deren Antwort sie bereits kennen, aber davon ausgehen, dass du dies nicht weisst. Es ist schwierig, diese Fragen als Testfragen zu erkennen. Die Strategie des “Treibsands” beginnt meist mit einer solcher Testfrage \[Seite 60\].

*Beispiele:*

- Wo waren Sie gestern Abend?
- Um wieviel Uhr haben Sie Ihre Wohnung aufgeräumt?
- Warum sind Sie gestern nicht zur Arbeit gefahren?

### Suggestivfragen

Suggestivfragen haben eine klare Ausrichtung. Sie zielen darauf ab, dich an deine Grenzen zu bringen und dir ein schlechtes Gefühl zu geben. So als ob die Polizist:innen schon sehr genau wüssten, was passiert ist, sie dies aber noch in deinen Worten hören möchten. Der gewünschte Effekt kann sein, dich zu einem Geständnis zu drängen, die Strategie der “Verpflichtung” zu verstärken \[Seite 62\] oder dass du ihnen mehr Informationen gibst, weil du das Bedürfnis hast, dich zu rechtfertigen und dich gegen die Unterstellungen zu verteidigen, die gegen dich vorgebracht werden. Um dies zu erreichen, können die Fragen mit Warnhinweisen versehen werden, wie z. B. “Überlegen Sie gut, bevor Sie antworten!” oder “Sind Sie sich dessen sicher?”.

*Beispiele:*

− Warum haben Sie gelogen, als Sie uns bestätigt haben, dass Sie gestern Abend ausgegangen sind?
- Sind Sie es, die diesen Brief geschickt hat?
- Ist es eine Ihrer Kompliz:innen, die Sie das hat machen lassen?

### Projektionsfragen

Wenn die Polizei versucht, die Beziehung zwischen ihnen und dir zu vermenschlichen, dich in einen spezifischen emotionalen Zustand zu versetzen - wie z.B. Schuld und Scham - oder dich dazu bringen will, Abstand von der aktuellen Situation zu gewinnen, können Projektionsfragen verwendet werden. Dies ist auch als besonders effektiver Fragetyp wirksam, um die Befragten auf den neutralen Boden des Konjunktivs zu bringen. Auf die Frage “Wenn Sie in Not wären, würden Sie dann Geld aus der Kasse Ihres Arbeitgebers nehmen?” scheint es weniger riskant, die Frage mit “Ja” zu beantworten, als wenn sie folgendermassen formuliert wäre: “Haben Sie aus der Kasse Ihres Arbeitgebers Geld gestohlen?”

*Beispiele:*

- Wenn Sie an unserer Stelle wären, was würden Sie denken?
- Warum, glauben Sie, hat die/der Schuldige so gehandelt?
- Was würden Sie fühlen, wenn Menschen illegalerweise bei Ihnen einbrechen würden?

### Rückfragen

Dies sind kurze Fragen, die den Inspektor:innen die Möglichkeit geben, Informationen zu sammeln, die ihnen wichtig erscheinen und Rückfragen zu stellen, um die Diskussion in die gewünschte Richtung zu lenken. Sie werden auch verwendet, um dir gegenüber den Eindruck von Verständnis, Einfühlungsvermögen und Interesse der Inspektor:innen zu vermitteln.

*Beispiele:*

- Welche Farbe hatte dieses Auto? (sobald du ein Auto erwähnst) - Und welchen Sport mögen Sie? (nachdem du gesagt hast, dass du Tennis nicht magst)
- Inwiefern? Sagen Sie mir mehr.

### Spiegelfragen

Spiegelfragen werden oftmals in psychologischen Therapien verwendet. Sie zielen darauf ab, dass sich die Person, die angehört wird, ihre eigene Aussage vertieft. Die Frage basiert auf einer Reformulierung einer eben gegebenen Antwort. Diese Fragen werden für die Strategie der “Verpflichtung”, sowie bei der Strategie “humanisieren der Beziehung” häufig gebraucht \[Seite 66\].

*Beispiele:*

- Sie sagten, dass Sie sich von Ihrem Arbeitgeber nicht respektiert fühlten?
- Sie haben sich in der Präsenz Ihrer Freundin also schlecht gefühlt?

(nachdem die verhörte Person ausgesagt hat, dass sie sich in der Präsenz ihrer Freundin schlecht gefühlt habe)

**Manipulationstechniken, Verhörstrategien, Fragearten: Dies sind so viele Stücke eines Puzzles, welche die Polizei in tausend Möglichkeiten kombinieren kann, um sich an die Situation und die Person anzupassen und damit ihr Ziel zu erreichen.**


# Rund um das Verhör

Dieses Kapitel untersucht verschiedene Elemente, die einen direkten Einfluss auf ein Verhör haben.

## 8. Verhörprotokoll

Bei jedem Verhör wird von der Polizei ein Protokoll angefertigt. Dieses Dokument wird in die Ermittlungsakte aufgenommen und dadurch der Inhalt des Verhörs an den:die Richter:in im Falle einer Gerichtsverhandlung weitergegeben. Die Interaktionen zwischen der befragten Person und den Inspektor:innen werden während dem Verhör in Echtzeit zusammengefasst und schriftlich niedergeschrieben. Werden die Vernehmungen von zwei Ermittler:innen durchgeführt, so wird das Protokoll von einem:r aufgenommen, während der:die andere die überwiegende Mehrheit der Fragen stellt.

Das Protokoll hält die Dauer des Verhörs, die Fragen/Antworten, die Beiträge des:r Anwält:in, die nonverbale Kommunikation (Stottern, Verkrampfung, Stummheit, Kopfschütteln) und das Verhalten der befragten Person (Gereiztheit, Weinen, emotionale Phasen) fest. Am Ende der Befragung wird das ausgedruckte Protokoll dem:r Befragten für eventuelle Korrekturen vorgelegt. Wenn ein:e Dolmetscher:in anwesend ist, wird das Protokoll mündlich übersetzt.

**Das Unterschreiben des Protokoll bedeutet, die Befragung und ALLE Inhalte, die dabei festgehalten wurden, zu bestätigen.**

Ich persönlich sehe keinen Sinn darin, dieses Dokument zu unterschreiben und rate davon ab. Im Falle, dass du keine Aussagen gemacht hast, hast du nichts zu gewinnen, wenn du es unterschreibst, und außerdem bleibst du auf diese Art konsequent bei deiner Strategie der Verweigerung einer Zusammenarbeit. Wenn es zu einer Gerichtsverhandlung kommt, macht es keinen Unterschied, ob du dich geweigert hast, ein leeres Protokoll zu unterschreiben.

Falls du auf die eine oder andere Verhörstrategie der Polizei hereingefallen bist und Aussagen gemacht hast, verschlechtert sich deine Situation weiter, wenn du das Protokoll unterschreibst. Damit bestätigst du deine Aussagen und erschwerst jeglichen späteren Widerruf. Ein Verhörprotokoll nicht unterschrieben zu haben ist eine notwendige Bedingung, um später den Inhalt des Protokolls anfechten zu können.

Die Tatsache, dass du nicht mit der Polizei zusammenarbeitest (keine Aussagen, keine Unterschrift), bietet dir alle Möglichkeiten, dir die beste Verteidigungsstrategie für den bevorstehenden Prozess zu überlegen, ohne dich vorher selbst belastet zu haben. Dies ist eine allgemeine Regel, die bei allen Gesprächen mit Polizeibeamt:innen gilt: Das geringste Risiko besteht darin, nicht zu kooperieren, sprich nicht im Sinne der Strafverfolgungsbehörden zu handeln. Jeder Schritt, den du in Richtung Kooperation mit der Polizei machst, erhöht die Gefahr, der du dich aussetzt, ohne dir im Gegenzug einen Vorteil zu verschaffen.

\[Polizei-Zitat\]

> Die Art und Weise, wie man ein Protokoll schreibt, bedeutet bereits, Druck auszuüben.
> Wenn man beim Aufschreiben, das Wort der Person nicht respektiert, dann übt man Druck aus. Wird diese Person den Mut haben sich bei dem Polizisten zu beschweren und zu sagen: “Hey, was Sie da schreiben, ist nicht das, was ich gesagt habe”. Es braucht Mut um es zu wagen, einem Polizisten, der Autorität vor Ihnen, gegenüberzutreten.


## 9. Nonverbale Kommunikation

Wenn wir verbal kommunizieren, reagiert unser Körper auf die Emotionen, die aktiviert werden, je nach Themen welche angesprochen werden. Dies äußert sich insbesondere durch Mimik, Haltung, Gestik, Blick, Tonfall und dem Rhythmus der Stimme. Dies wird als nonverbale Kommunikation bezeichnet. Wenn du nervös an deinem Stift herumfummelst oder die Arme verschränkst, wird das nicht unbedingt als Zeichen einer Lüge interpretiert, sondern kann auf eine Sensibilität bezüglich eines Themas hinweisen.

Bei einem Verhör besteht eine starke emotionale Asymmetrie zwischen dir und den Inspektor:innen. Für sie ist es ein ganz normaler Teil ihrer täglichen Arbeit, es ist Routine. Sie haben aufgrund ihrer Ausbildung und fortwährenden Praxis viel Erfahrung mit solchen Situationen. Auch wenn du bereits einige Erfahrungen während Verhören gemacht hast, wird dein emotionaler Zustand immer intensiver sein als jener der Polizist:innen, die dir gegenübersitzen. Dein Herz wird schneller schlagen, deine Atmung wird schneller, deine Konzentration höher und deine Sinne werden geschärft sein. Es steht einfach nicht das Gleiche auf dem Spiel für Menschen, die ihre Arbeit tun und für dich, der:die riskiert, dass sich, je nachdem wie dieser Moment abläuft, sich dieser auf deine Zukunft auswirkt.

Es ist möglich, die Art und Weise wie unser Körper unseren emotionalen Zustand vermittelt zu erlernen und dies auch teilweise zu kontrollieren. Dies ist jedoch eine sehr schwierige Übung, die äußerste Konzentration und Selbstbeherrschung erfordert, was unter den Bedingungen eines Verhörs nur schwer aufrechtzuerhalten ist. Durch unsere Inhaftierung verfügt die Polizei über eine breite Palette von Mitteln, um uns psychisch und physisch zu schwächen: Störung unseres Schlafzyklus, Einschüchterung, Isolation von sozialen Kontakten, Mangel an Nahrung, etc.

Polizist:innen achten auf deine nonverbale Sprache, aber auch auf deren mögliche Verschleierung. Wenn sich in unserem Gehirn eine Idee und ein Gefühl bildet, kommuniziert unser Körper die damit verbundenen Emotionen schneller als durch die Formulierung von Sätzen. Wenn eine Person Traurigkeit zeigt, der Körperausdruck der Traurigkeit jedoch erst nach der verbalen Kommunikation in Erscheinung tritt, ist die Wahrscheinlichkeit hoch, dass diese Emotion gespielt wird.

Manche Inspektor:innen versuchen vielleicht, dich mit ihrer eigenen Körpersprache zu beeinflussen. Zum Beispiel bei der Strategie der “emotionalen Ansteckung” \[Seite 69\] versuchen Polizist:innen, dich in eine emotionale Richtung zu lenken, die sie zu diesem Zeitpunkt selbst ausdrücken. In diesem Zusammenhang wird ein:e Inspektor:in, der:die wütend wirken möchte, auch auf nonverbale Verhalten zurückgreifen, damit du glaubst, dass seine:ihre Emotion echt ist (hohe Lautstärke der Stimme und hohes Sprechtempo, verkrampfte Hände, Röte im Gesicht usw.).

Im Folgenden sind die wichtigsten Körpermerkmale aufgelistet, auf die die Polizei achtet, um darin nach Hinweisen auf deinen emotionalen Zustand zu suchen[^Diese Liste kommt aus dem Buch vom P. Ekman “Ich weiss, dass du lügst”, Rowohlt Taschenbuch, Berlin, 2015].

- Hände: verkrampft, kratzend, verdeckt, zitternd
- Sprache: Stimmlage, Lautstärke, Rhythmus, Intonation, Stottern, Husten, Räuspern, Schlucken, trockene Kehle, Lachen
- Körper: Muskelspannung, Zittern, Erröten, Schwitzen
- Atmung: Seufzen, Rhythmus, ruckartig, tief
- Blick: An- oder Abwesenheit des Blicks, Nuance, Tränen, Blickrichtung
- Herzrhythmus


## 10. Dolmetscher:innen

Wenn die befragte Person die Amtssprache(n) des Landes, in dem sie sich befindet, nicht spricht, ziehen die Polizist:innen eine:n Dolmetscher:in hinzu. In Ländern mit mehreren Amtsprachen, wie der Schweiz oder Belgien, kannst du verlangen, dass das Verhör in deiner Sprache durchgeführt wird (wenn sie zu den Amtssprachen gehört), auch wenn du dich in einem anderen Teil des Landes befindest.

Obwohl Dolmetscher:innen nicht Teil der Polizei sind, sind sie dennoch nicht deine Freund:innen oder Verbündete. Sie sind das, was die Polizei als “externe Mitarbeiter:innen” bezeichnet. In einer feindseligen Umgebung kann eine Person, die nicht von der Polizei ist und deine Muttersprache spricht, den Eindruck von Nähe vermitteln. Doch die Dolmetscher:innen , mit denen du es zu tun hast, werden von der Polizei ausgewählt, überprüft und müssen regelmäßig mit ihnen zusammenarbeiten. Achte darauf, dass du mit den anwesenden Dolmetscher:innen nicht das “Phänomen des Rettungsrings” wiederholst \[Seite 66\]. Wenn du denselben sozialen Hintergrund wie die Dolmetscher:in hast, kann es sein, dass die Inspektor:innen die:den Dolmetscher:in nach kulturellen und politischen Hintergründen zu deiner Person fragen.

**Viele der Verhörstrategien erfordern es, dass die Polizist:innen direkten Kontakt zu dir haben.**

Der Vorteil der Anwesenheit ein:er Dolmetscher:in ist ihre:seine Pufferwirkung zwischen der befragten Person und den Inspektor:innen. Darum sitzt der:die Dolmetscher:in oft hinter den Ermittler:innen, die dann direkt mit dir sprechen und eher dich als den:die Übersetzer:in anschauen. In ihren eigenen Publikationen sind Polizist:innen sich bewusst, dass der Einsatz von Strategien wie dem “Rezenzeffekt “ \[Seite 51\] oder der “humanisieren der Beziehung” \[Seite 66\] schwieriger zu realisieren sind, wenn eine Übersetzer:in anwesend ist.

Ob mit oder ohne Dolmetscher:in: Schweigen bleibt die beste Verteidigungsstrategie gegenüber der Polizei. Die Anwesenheit eines:r Dolmetschers:in kann jedoch durchaus positiv für dich sein. Sie hilft dir, die Fragen der Polizei, sowie den Kontext in dem du dich befindest, zu verstehen und erschwert den Polizist:innen die Anwendung manipulativer Effekte, die sich auf die genaue Bedeutung von Wörtern, der Satzbildung und der Intonation beruhen, weil diese bei einer Übersetzung nicht identisch wiedergegeben werden können.

## 11. Antwält:innen

Der Zeitpunkt, ab welchem du mit einem Anwält:in in Kontakt treten kannst, hängt von dem Land in dem du dich befindest, sowie von der polizeilichen Praxis ab. In einigen Ländern, wie z.B. in der Schweiz, kannst du bereits beim ersten Kontakt mit der Polizei eine:n Anwält:in verlangen, in anderen Ländern erst, wenn du in Untersuchungshaft genommen wirst und offiziell ein Ermittlungsverfahren gegen dich eingeleitet wird. Wenn du nach einem:r Anwält:in fragst, versuchen die Polizist:innen manchmal, dich davon abzuhalten. Sie erklären dir, dass dies einige Zeit dauern kann und dass du bis dahin in ihren Räumen eingesperrt bleibst. Ohne eine:n Anwält:in wäre die Sache schnell erledigt.

Am besten ist es, wenn du bereits Kontakt zu Anwält:innen hast, die du kennst und die bereit sind, dich zu verteidigen. Wenn das nicht der Fall ist, wird dir ein:e Pflichtverteidiger:in vermittelt. Es ist auch möglich, dass du nicht das Bedürfnis hast, dich während der Ermittlungen von einem:r Anwält:in begleiten zu lassen und dass du dir das für den Prozess aufsparst.

Die Anwesenheit eines:r Anwält:in ist jedoch oft ein angenehmer Puffer gegenüber der Polizei und ihren aggressivsten Strategien. Die Praxis des “good cop, bad cop” \[Seite 57\] hat nicht die gleiche emotionale Wirkung, wenn ein:e Anwält:in anwesend ist. Um diese Schutzwirkung abzuschwächen, wird der:die Anwält:in oft hinter dir platziert. Zusätzlich werden Polizist:innen viel zurückhaltender sein, wenn es darum geht, über Beweismittel oder drohende Strafen zu spekulieren, wenn der:die Anwält:in diese in Kenntnis der Sachlage widerlegen und sie damit unglaubwürdig machen kann \[Seite 75\].

Die Anwesenheit eine:r Anwält:in kann auch Gefahren mit sich bringen. Es gibt genauso viele gute wie schlechte Anwält:innen. Es ist jedoch die einzige Person, die während des gesamten Gerichtsverfahrens auf deiner Seite steht. Es ist eine Person, die dich vertreten, verteidigen und für dich Partei ergreifen soll. Diese Erwartung kann zu einem “Rettungsring-Effekt” führen \[page 66\], der nach hinten losgehen kann, wenn dein:e Anwält:in dich schlecht berät. Es kann nämlich vorkommen, dass der:die Anwält:in dir empfiehlt, bestimmte Fragen der Polizei zu beantworten oder Aussagen über deine Kompliz:innen zu machen, um dich zu entlasten. Der:die Anwält:in hat bei einem Verhör keine Autorität und ist kein Schutz gegen die Polizist:innen. Es ist nicht möglich, sich den Fallen zu entziehen, indem du dich hinter ihm:ihr zu verstecken versuchst. Wenn du Fehler machst und du dich in die Dynamik der Fragen und Antworten begibst, hat dein:e Anwält:in keine Möglichkeit dich dort herauszuholen. Die Beiträge deiner Anwält:in werden ebenso wie eure Gespräche im Protokoll festgehalten.

Neben diesen Risiken kann ein:e gute:r Anwält:in an deiner Seite viel Positives bewirken und in ein Gerichtsverfahren einbringen:

- Eine menschliche Präsenz, die nicht feindselig ist. 
- Eine mögliche Verbindung zu deinen Angehörigen außerhalb des Gefängnisses. 
- Einfluss auf die Haftbedingungen (Anrufe tätigen, Post empfangen und versenden usw.). 
- Hilfe beim Papierkram, der mit der Haft verbunden ist (Anträge auf Freilassung vor dem Prozess, Berufungen gegen Haftentscheidungen, Beweisanträge, Anträge auf Zeugenanträge, Antrag auf Einsicht in die Ermittlungsakte usw.). 
- Hilfe bei der Vorbereitung auf den Prozess und bei der Entwicklung von Verteidigungsstrategien.

\[Polizei-Zitat\]

> In den meisten Fällen ist der schlimmste Feind des Anwalts sein Klient. Oft geht der Anwalt also ein bisschen in unsere Richtung, oder er berät seinen Mandanten gut, er sagt ihm, dass es in diesem Stadium besser ist, etwas zu sagen, als Lügen zu erzählen.

## 12. Zeugenvernehmung

Das Besondere an einer Zeugenvernehmung ist, dass sie in einem anderen rechtlichen Rahmen stattfindet. Bei einer Vernehmung wirst du als Beschuldigte:r zu einer Angelegenheit befragt, die dich selbst betrifft. Du bist nicht nur legitimiert zu Schweigen, sondern hast auch das juristische Recht dazu.

Als Zeuge:in wirst du nur dazu aufgefordert, Aussagen über andere Personen und über einen Sachverhalt zu machen von dem du nicht direkt betroffen bist oder zumindest nicht verdächtigt wirst, gegen ein Gesetz verstoßen zu haben. In den meisten Ländern bist du gesetzlich verpflichtet, dich zu äußern. Wie jede gesetzliche Verpflichtung kann auch diese gebrochen werden. Wenn dir nachgewiesen werden kann, dass du lügst, kann dir in den meisten Fällen eine Geldstrafe verhängt werden. In einigen schwerwiegenden Fällen kannst du wegen Beihilfe angeklagt werden.

Es kommt vor, dass Ermittlungen gegen Zeug:innen eingeleitet werden, nachdem sie vor der Polizei oder vor dem Gericht ausgesagt und diese Aussagen den:die Zeugen:in belastet haben.

Auch hier ist es gefährlich, wenn du versuchst zu lügen, weil sich die Lage gegen dich wenden kann. Und wenn das Gesetz es dir verbietet, die Aussage zu verweigern, zwingt dich kein Gericht, dich zu erinnern ; niemand hat Einfluss auf dein Gedächtnis. Die Antwort “Ich kann mich nicht erinnern” ist eine gute Ausweichmöglichkeit, um zu vermeiden dich selbst in Gefahr zu bringen oder Informationen preiszugeben, die zur Verurteilung anderer verwendet werden könnten.

## 13. Polizeigewahrsam und Untersuchungshaft

\[Polizei-Zitat\]

> Es ist klar, dass ein Typ, der dir auf die Nerven geht, der dich provoziert, der dumm ist, nicht rauchen dürfen wird. Ich bin nicht verpflichtet, ihn Rauchen zu lassen, das ist der Punkt. Im Gegensatz dazu wird ein Typ, der kooperativ ist, rauchen dürfen.

Eine Vernehmung kann auf eine schriftliche Vorladung der Polizei hin erfolgen. Es ist aber auch möglich, dass du dich zum Zeitpunkt der Befragung bereits in Haft befindest.

Es gibt zwei Arten von möglichen Haftsituationen:

*Polizeigewahrsam:* Hierbei handelt es sich um eine Inhaftierung in den Räumlichkeiten der Polizei für einige Stunden oder sogar einige Tage (die Gesetze sind von Land zu Land unterschiedlich). Dies ist der Fall, wenn du auf frischer Tat, also beispielsweise bei einem Vergehen oder während einer Demonstration festgenommen wirst. Die Inhaftierung kann sich verlängern, wenn sie als Druckmittel oder zur Destabilisierung eingesetzt wird, um dich zu Verhörzwecken verletzlicher zu machen. Die Haft endet entweder, wenn die Polizei dich wieder frei lässt, oder wenn die Staatsanwaltschaft (oder ein:e Richter:in, je nach Rechtslage des Landes, in dem du dich aufhältst) deine weitere Inhaftierung anordnet, bis dein Prozess stattfindet. Nach der Polizeigewahrsam kann es sein, dass du eine Vorladung zu einer weiteren Vernehmung erhältst.

*Untersuchungshaft:* Wenn ein:e Staatsanwält:in oder Richter:in entscheidet, dass du nicht vor deinem Prozess freigelassen werden kannst, wirst du in Untersuchungshaft genommen. Die Rechtfertigung kann sein, das Risiko des Untertauchens zu vermeiden, um der Strafe zu entgehen, den Kontakt zwischen Mitangeklagten zu verhindern, dich zu hindern Beweise verschwinden zu lassen oder die laufenden Ermittlungen zu beeinträchtigen.

**Verschiedene Verhörstrategien bauen auf der Fragilität auf, die der Freiheitsentzug mit sich bringt.**

Die Untersuchungshaft findet nicht in den Räumen der Polizei, sondern in einem Gefängnis statt. Die Zeit, die du in Untersuchungshaft verbringen wirst, ist wesentlich länger als jene einer Polizeigewahrsam. Auch hier werden die Dauer und die Bedingungen der Haft als Druckmittel und zur Schwächung deines Widerstands gegen die Verhöre genutzt. Während du in Haft bist, laufen die Ermittlungen weiter und du kannst vor deinem Prozess noch viele Male verhört werden. Unbekannte in Uniform holen dich ab und bringen dich in das Büro, in dem das Verhör stattfindet. Es ist wichtig, diesen Prozess des Eingesperrtseins als Begleiterscheinung von Verhören und den laufenden Ermittlungen zu verstehen. Verschiedene Verhörstrategien bauen auf die Schwächung auf, die ein Freiheitsentzug mit sich bringt. Eines der Ziele des Einsperrens ist es, dich physisch und psychisch zu schwächen, um deine geistigen Fähigkeiten zu beeinträchtigen und deine Chancen zu verringern, dich zu verteidigen.

Nach Abschluss der Ermittlungen kannst du deine Ermittlungsakte einsehen, um dich vom Gefängnis aus auf den Prozess vorzubereiten, mit oder ohne Anwält:in. Die Untersuchungshaft endet entweder direkt bei der Gerichtsverhandlung oder vorher, in welchem Fall du bis zu deiner Verhandlung wieder freigelassen wirst. Bei einer Verurteilung wird dir die Zeit, die du bereits in Untersuchungshaft verbracht hast angerechnet. Im Falle eines Freispruchs kannst du je nach Gesetzlage entschädigt werden.

Ich gebe hier einen kurzen Überblick über die verschiedenen Mechanismen, die bei Eingesperrtsein zum Tragen kommen:

*Verlust der Kontrolle über die Zeit:* Vom ersten Moment an, in dem du dich in der Gewalt der Polizei befindest, verlierst du die Kontrolle über deine Zeit. Das Essen wird dir gebracht, du wirst in den Verhörraum gebracht, du wirst zum “Spaziergang” herausgeholt, egal ob es dir passt oder nicht. Manche Zellen, vor allem in Polizeigewahrsam, haben keine Toilette und das Licht wird von außen ein- und ausgeschaltet, was deine Entscheidungsmöglichkeiten noch weiter einschränkt. Ob du schläfst, liest, schreibst oder Sport treibst, jederzeit kann dein Programm unterbrochen werden. Das kann ein Gefühl der Enteignung deiner Fähigkeit zur Selbstbestimmung generieren. Wenn sich dieses Gefühl in dir festsetzt und während der Verhöre anhält, wird es deine Fähigkeit deutlich beeinträchtigen, dich gegen die Inspektor:innen zu wehren.

*Isolation:* Die Einsamkeit und soziale Isolation, die sich aus einer Haftzeit ergeben, können ebenfalls sehr verwirrend sein. Wenn du nach einer Zeit ohne menschliche Interaktionen plötzlich als einzigen Kontakt die Inspektor:innen hast, die so tun, als würden sie sich für dich interessieren, ist die Versuchung groß mit ihnen einen Dialog zu führen, und sei es nur, um endlich mit jemandem zu sprechen. Wenn die Polizist:innen merken, dass die Einsamkeit eine Auswirkung auf dich hat, können Strategien wie der “Rettungsring” oder die “Humanisierungung der Beziehung zur Polizei” bevorzugt werden \[page 66\].

*Einschüchterung:* Feindselige bis aggressive und gewalttätige Haltung der Wächter:innen, Schikanen um das Nötigste (Essen, Binden, Medikamente), die Post wird “administrativ” blockiert und so weiter. All das sind Möglichkeiten, die Polizeibeamt:innen haben, um ein Machtverhältnis aufzubauen und eine bedrohliche Aufforderungen zur Zusammenarbeit auszusprechen. Ein solches Klima der Spannung ist ein idealer Nährboden für Strategien wie “good cop, bad cop” \[page 57\] oder “emotionale Ansteckung” \[Seite 69\].

In der Mehrzahl der Fälle ist eine Verhaftung eine Überraschung für eine festgenommene Person. Der Umstand, für eine unbestimmte Zeit gewaltsam aus dem eigenen Alltag gerissen zu werden, löst oftmals eine grosse Portion Stress und Angst aus. Du wirst wahrscheinlich Verabredungen haben, die du nicht wahrnehmen kannst, und Menschen, die du nicht benachrichtigen kannst werden sich Sorgen machen, eine:n Arbeitgeber:in, welche in Schwierigkeiten gerät, wenn du nicht da bist, eine Person, die in Schwierigkeiten gerät, weil du nicht mehr da bist und dich um sie kümmern kannst, und so weiter. Eine Verhaftung ist immer ein harter Einschnitt in deinen Alltag.

Die dadurch erzeugte Angst und geistige Belastung nimmt viel geistigen Raum ein und verhindert, dass du dich auf das Verhör und seine Gefahren konzentrieren kannst. Darüber hinaus wird die Möglichkeit, freigelassen zu werden, häufig als Köder benutzt. Je größer der Stress und die Unannehmlichkeiten sind, desto mehr Gewicht hat dieses Argument. Die Unterdrückung, die in der Gefangenschaft liegt, ist ein wichtiger Faktor, der nicht zu vernachlässigen ist. Die Gefahr, welche die Inhaftierung als Druckmittel während der Ermittlungen mit sich bringt, ist jedoch viel höher. Es besteht die Gefahr, dass du ihnen die Informationen gibst, die notwendig sind, um dich zu verurteilen, was die Dauer der Haft potenziell erheblich verlängern kann.

[Box]

> **Meine Zelle?**
>
> “Zurück in deine Zelle” klingt ein wenig wie “Geh und räum dein Zimmer auf”. Doch diese Zelle ist nicht meine. Wie könnte sie das auch sein? Ich würde niemals Energie darauf verwenden, mir einen Raum anzueignen, der dazu bestimmt ist, andere einzusperren.
> Wenn andere solche Orte planen, bauen und pflegen, dann sollen sie auch die Verantwortung darüber behalten. Eine Zelle, eine Uniform oder eine Nummer eines:r Insass:in wird immer Eigentum der Gefängnisverwaltung bleiben. Diese Sprachstrategie soll mich dazu bringen, mir die kalte Logik des Strafvollzugs anzueignen und diese zu akzeptieren. In der gleichen Logik geht es nicht um mein Verhör oder meine Haftstrafe, sondern um das Verhör, dem ich unterzogen werde, und um das Strafmaß, das mir auferlegt wird. Ich bin nicht Teil des Prozesses der institutionellen Justiz. Ich erleide ihn und verteidige mich dagegen. Die Werkzeuge, die ich erfinde und mit den Menschen um mich herum entwickle, um Konflikte zu bewältigen und das Leiden, das sie verursachen, zu begleiten, sind nicht vergleichbar mit der Autorität, die der Staat ausübt.


## 14. Einige Worte über Gewalt

**Achtung!** *Dieses Kapitel enthält Beschreibungen von Praktiken, die der physischen Folter ähneln.*

Jedes Polizeiverhör enthält mindestens eine Form psychischer Gewalt. Die Techniken der Manipulation und des Drucks bei den Verhören, auf denen die Polizeiarbeit beruht, sind untrennbar mit einer bestimmten Form psychischer Gewalt verbunden. Die Anwendung physischer Gewalt wurde jedoch in vielen Ländern von der Liste der Druckmittel gestrichen, die bei einer polizeilichen Befragung rechtmäßig eingesetzt werden dürfen. In anderen Staaten jedoch ist die Anwendung dieser Form von Gewalt immer noch legal.

Nach dem 11. September beauftragte die CIA zwei Psychologen mit der Entwicklung von Verhörtechniken, die psychische und physische Gewalt miteinander verbinden. Dieses Handbuch ist zwar alles andere als innovativ, aber es überzeugte dennoch durch seine “moderne” Ausrichtung und wurde schnell weltweit zwischen verschiedenen Behörden weitergegeben.

Hier einige Auszüge aus diesen Techniken[^Für eine ausführlichere Liste siehe die deutsche Übersetzung dieses Berichts: Der CIA Folterbericht. Der offizielle Bericht des US-Senats zum Internierungs- und Verhörprogramm der CIA”. Westend Verlag, 2005]:

- *Umklammerung:* Der/die Vernehmungsbeamte packt den Befragten plötzlich am Kragen und zieht ihn zu sich heran, um einen Überraschungseffekt zu erzielen, zu schockieren, einzuschüchtern und/oder zu erniedrigen. 
- *Walling:* Der:die Beschuldigte wird zuerst schnell nach vorne gezogen und dann nach hinten zurückgeschleudert, so dass er:sie mit den Schulterblättern gegen eine Wand prallt. 
- *Ohrfeige:* Wird mit dem Ziel gegeben, den Befragten zu demütigen, ihn an der Konzentration zu hindern und ihm ein Gefühl von Minderwertigkeit durch aggressives Eindringen in die Privatssphäre zu geben.
- *Fixierung des Kopfes:* Hinter der befragten Person stehend fixiert der:die Inspektor:in den Kopf des Befragten durch den Druck seiner:ihrer Hände, während er:sie ihm:ihr Fragen stellt.
- *Waterboarding − Simulation des Ertrinken:* Die befragte Person wird auf den Rücken gefesselt, mit dem Kopf nach unten geneigt. Wasser wird auf ein Tuch gegossen, das seine Nase und seinen Mund bedeckt, was ein sehr starkes Gefühl des Ertrinkens hervorruft.
- *Schlafentzug:* Die befragte Person wird mehrere Tage lang nicht schlafen gelassen, um einen Zustand der Erschöpfung und Unkonzentriertheit zu erzeugen.
- *Einschließung:* Die befragte Person wird in einem begrenzten Raum eingeschlossen, der seine:ihre Fähigkeit, sich zu bewegen einschränkt. Je nach Phobie der Person raten die CIA-Psychologen zur Platzierung von Insekten im Raum. Die Dauer der Einschließung beträgt je nach Art der Einschließung zwischen 2 und 18 Stunden.
- *Schmerzhafte Positionen:* Die befragte Person wird gezwungen, über einen längeren Zeitraum in einer unbequemen Position zu bleiben (mit ausgestreckten Armen und Beinen auf dem Boden sitzend, auf dem Boden kniend, gegen die Wand gelehnt, wobei nur die Finger das Körpergewicht halten usw.). Parallel dazu werden ihr Fragen gestellt.
- *Hunger/Temperatur:* Verwendung von Elementen wie Hunger und Temperatur (die Person wird in einem sehr heißen oder sehr kalten Raum eingesperrt), um die befragte Person zu schwächen.
- *Nacktheit:* Die befragte Person wird gezwungen, sich zu entblößen, mit dem Ziel, sie zu erniedrigen.

Diese Techniken sind weit entfernt von dem Filmklischee des:r cholerischen Inspektors:in, der:die die verdächtige Person schlägt oder eines psychopathischen Folterers mit der Zange in der Hand. Hier wird Gewalt als ein Element der Verhörstrategie eingesetzt, ebenso wie die verbalen Manipulationen, die weiter oben beschrieben wurden.

Ihr Einsatz dient demselben Zweck: Destabilisierung, Schwächung, Erschöpfung, Einschüchterung und Brechen des Widerstands, damit sie sich in die von den Inspektor:innen gewünschte Richtung bewegt (Geständnis, Erklärungen, Denunziation, Zusammenarbeit). Es handelt sich um Strategien, die, je nach Profil der Person, durchdacht, berechnet und im Voraus vorbereitet werden.

**Die Tatsache, dass Polizist:innen gesetzlich nicht berechtigt sind, Gewalt anzuwenden, bedeutet nicht, dass sie keine Gewalt anwenden werden.**

Neben dem genau untersuchten Einsatz von Gewalt taucht diese manchmal auch auf ganz andere, weitaus weniger subtile Weise auf. Wenn dies geschieht, und in den seltenen Fällen, in denen es öffentlich anerkannt und thematisiert wird, wird es im öffentlichen Diskurs als Fehlverhalten der Polizei bezeichnet. Der Begriff Fehlverhalten ist jedoch völlig unzureichend, um die Anwendung von Gewalt zu beschreiben, die zwar illegal ist, aber von einer Berufskultur gefördert und von einer Justiz geschützt wird ; Polizeigewalt wird vor Gericht nur äußerst selten verurteilt. In einem Verhör wird diese Gewalt viel emotionaler sein als die zuvor beschrieben Strategien. Polizist:innen, die als Racheakt einen Gefangenen verprügeln oder um ihn:sie für ein freches Verhalten “bezahlen” zu lassen machen das eher aus einem emotionalen Impuls heraus, als aus taktischen Gründen, um konkrete Ergebnisse für ihre Ermittlungsakte zu erzielen. Auch hier geht es letztendlich darum, die Verhörte Person einzuschüchtern und ihren Widerstand zu brechen.

Sei es mit physischer und/oder psychischer Gewalt – bei einem Verhör geht es um ein Machtverhältnis mit dem Ziel, die befragte Person zu zwingen, den Interessen der Polizei zu folgen.

Durch persönliche Erfahrungen habe ich einen Einblick in die Auswirkungen der Anwendung körperlicher Gewalt in einem Verhörund Befragungsrahmen erhalten. Jedoch fehlt mir das Wissen, um an dieser Stelle Ratschläge zum Schutz und zum Widerstand gegen diese Praktiken zu entwickeln. Anstatt diese Leere mit riskanten Informationen zu füllen, überlasse ich lieber anderen, die Aufgabe, diese Seiten zu schreiben[^Ich nehme diese Schriften auch gerne in die nächsten Ausgaben dieses Buches auf.].

Um Aspekte dieser Thematik zu vertiefen, verweise ich auf die Bücher:

- Coco Fusco, A Field Guide for Female Interrogators, 2008
- Der CIA Folterbericht - Der offizielle Bericht des US-Senats zum Internierungs- und Verhörprogramm der CIA. Westend Verlag, 2015

\[Polizei-Zitat\]

> “In Situationen, in denen Personen in Gefahr sein könnten und wir unbedingt Informationen von einem Typen brauchen, könnte ich mir vorstellen, dass grenzwertige Techniken, die aber immer noch legal sind, verwendet werden könnten, Bedarf an Notwendigkeit”.

# Sich verteidigen

Nach einer Untersuchung der verschiedenen Angriffswinkel, die die Polizei bei der Durchführung von Vernehmungen einnimmt, befasst sich dieses Kapitel mit Hinweisen und Werkzeugen zur Verteidigung sowie mit falschen und gute Ideen zur Verteidigung.

## 15. Die Lüge als Falle

Angesichts einer Beschuldigung sind drei Arten von Reaktionen möglich:

- Die Anschuldigung anerkennen und akzeptieren. Entweder teilweise oder in vollem Umfang. 
- Leugnen, entweder durch Lügen oder durch Rechtfertigung. 
- Sich weigern, sich zu äußern und schweigen.

Eine Lüge kann als instinktiver Schutzmechanismus gegen eine Beschuldigung angesehen werden. Sie wird auf jeden Fall von der Polizei als der am weitesten verbreitete Verteidigungsmechanismus wahrgenommen. Lügen heißt, sich mit den Inspektor:innen auf ihrem eigenen Gebiet zu messen, da du dich auf eine Diskussion einlässt. Dies bedeutet, dass du nach Regeln zu kämpfen versuchst, die du nicht selbst aufgestellt hat, mit allen Risiken, die dies mit sich bringt.

Lügen erfordert viel Anstrengung. Vor allem, wenn du unter Druck gesetzt wirst und dich nicht vorbereitet fühlst, auf das, was dir gerade passiert. Lügen bedeutet, eine Geschichte zu erfinden, die in sich schlüssig ist und mit dem übereinstimmt, was die Polizei bereits gefunden - und von deren Existenz du nichts weißt. Das erfordert ein hohes Maß an Vorstellungskraft, Selbstbeherrschung und ein sehr gutes Gedächtnis - vor allem, wenn du die gleiche Geschichte einen Tag oder einen Monat später noch einmal genau erklären musst, ohne Fehler zu machen und ohne dir Notizen machen zu können.

In den Polizeiakademien werden angehende Ermittler:innen darin geschult, Lügen zu widerlegen. Sie lernen nicht nur, wie lügen, sondern auch, wie sie die Lüge gegen die lügende Person ausnutzen können. Mithilfe der nonverbalen Sprache können sie die körpersprachlichen Signale einer Person erkennen, die lügt. In der Strategie des “Treibsand” \[Seite 60\] gehen sie so weit, dass sie den Befragten zur Lüge verleiten, um sie dann zu destabilisieren, indem sie ihr zu verstehen geben, dass sie die Lügen erkannt haben und dass die Person nicht mehr glaubwürdig ist.

Bei der Ausbildung werden verschiedene Spiele verwendet, mit denen die Polizist:innen üben, wie es möglich ist eine Lüge zu erkennt. Hier sind zwei Beispiele:

- *Zwei Personen führen eine Diskussion.* Jede Person hat ein Thema, das er:sie nicht ansprechen darf. Während der Diskussion muss jede Person das Thema finden, das die andere Person zu verbergen versucht und verhindern, dass das eigene sensible Thema aufgedeckt wird. Mit diesem Spiel kann geübt werden, wie zu erkennen ist, wann eine Person versucht, einen Teil der Wahrheit zu verbergen oder einem bestimmten Thema auszuweichen. 
- *Mehrere Personen sitzen um einen Kartenstapel herum.* Auf einigen Karten ist ein Bild, auf anderen nur ein Fragezeichen. Reihum zieht jede Person eine Karte, ohne sie den anderen zu zeigen. Wenn die Karte ein Bild hat, muss es beschrieben werden, wenn die Karte ein Fragezeichen hat, muss ein Bild beschrieben werden, wobei zu verschleiern ist, dass es in diesem Moment erfunden wird. Die anderen Personen müssen erraten, ob die Person ein Bild beschreibt, das existiert oder nicht.[^Mit dem Ziel die Schwierigkeit des Lügen in einer Verhörsituation aufzeigen, wurde dieses Spiel als pädagogisches Werkzeug angepasst => Taceo #1, Project-Evasions, 2020.]

Angesichts der Ausbildung der Polizist:innen und des schwierigen Kontexts, in dem sich eine Befragung abspielt, empfehle ich das Lügen nicht als Verteidigungsstrategie. Ich halte es für ein unnötiges Risiko, das du für dich selbst und andere eingehst, sowie eine ineffiziente Strategie.

## 16. Falsche Überzeugungen, die zur Aussage drängen

In den Workshops zur Selbstverteidigung gegen die Polizei, die ich gebe, bin ich auf allerlei falsche Vorstellungen über die Verweigerung von Aussagen während eines Verhörs gestoßen. In diesem Kapitel, erkläre ich, warum ich diese Meinungen als falsche Annahmen betrachte.

### Ich bin verpflichtet, die Fragen der Polizei zu beantworten.

Diesen Satz habe ich schon oft gehört, und er scheint ein tief verwurzelter Glaube in der kollektiven Vorstellung zu sein. Ich erkläre mir dies auf dreierlei Weise: Erstens durch die Autoritätsposition, die die Polizei in unserer Gesellschaft genießt, zweitens der Gedanke, dass es eine rechtliche Verpflichtung gibt, Fragen der Polizei zu beantworten und drittens die Beeinflussung durch die fiktiven Welten von der westlichen Film- und Medienindustrie. Lassen Sie uns dies im Einzelnen betrachten.

*Autorität der Polizei:* In unseren autoritären Demokratien sind wir von klein auf an Autoritätspersonen, denen wir Respekt und Gehorsam entgegenbringen, gewöhnt: Eltern, Lehrer:innen, Ärzt:innen und alle Arten von “Spezialist:innen”[^Das herausragendste Experiment zu diesem Thema ist nach wie vor das Milgram-Experiment, das in zahlreichen Formen wieder aufgegriffen und wiedergegeben wurde. Siehe Das Milgram – Experiment, Zur Gehorsamsbereitschaft gegenüber Autorität, Rowohlt Verlag, 1982.]. Das Konzept der Autorität wird völlig banalisiert und wir werden eher zum Gehorsam gegenüber Autoritäten erzogen als zum Hinterfragen und kritischen Denken. Die Polizei (und Uniformen im Allgemeinen) vermittelt ein Gefühl der moralischen Autorität, der legitimen Überlegenheit.

Diese kulturelle und soziale Konstruktion erweckt den unbewussten Eindruck, dass du, wenn dir ein:e Polizist:in eine Frage stellt, diese beantworten musst, und dass wenn du das nicht tust, getadelt wirst, genau wie zu den Zeiten, wo du deinen Eltern oder Lehrer:innen nicht antworten wolltest.

Betrachten wir dies aus einer gewissen Distanz. Meiner Meinung nach haben wir alle die Legitimität, unser Leben so zu leben, wie wir es für richtig halten, in umfänglicher Freiheit unserer Entscheidungen. Allerdings unterliegen wir einer ganzen Reihe von sozialen Konstruktionen, Zwängen und Normen. Diese Autoritätspersonen und die damit verbundenen sozialen Normen zu erkennen und sich selbst die Legitimität zuzugestehen, sein Leben selbst zu bestimmen, sind erste nützliche Schritte zu ihrer Überschreitung. Eine solche Verweigerung gesellschaftlicher Normen und Vorstellungen ist oft schwierig und ungleichmäßig möglich je nach unserem Status in der Gesellschaft. Es bringt meist auch Folgen mit sich, aber dennoch ist es ein “Träger” der Emanzipation. Und was die polizeiliche Autoritätsfigur betrifft, so ist die Missachtung bzw die Verweigerung der Anerkennung dieser Figur eines unserer notwendigsten Verteidigungsinstrumente während eines Verhörs.

**Das Erkennen von Autoritätspersonen und sozialen Normen sind hilfreiche erste Schritte für deren Übertretung.**

*Fiktionale Vorstellungen:* Kriminalfälle[^Es wird von Krimis gesprochen, wenn die Zuschauer:innen in der Fiktion die Geschichte aus der Perspektive eines:r Polizist:in, Journalist:in, Detektivs oder einer anderen Person, die Ermittlungen durchführt, erleben.] sind eine große Inspirationsquelle für fiktionale Inhalte. Filme ,Serien, Comics ,Bücher, Theaterstücke, usw. Wir werden von der Ermittlungsarbeit, die Inspektor:innen zu lösen haben, sehr gut unterhalten. Auch wenn die meisten Menschen noch nie ein Verhör erlebt haben, hat jede:r eine Vorstellung und ein geistiges Bild davon, wie es aussieht, weil wir alle schon viele verschiedene fiktionale Darstellungen davon gesehen haben.

Seit ich mich für das Thema Verhörtechniken interessiere, sehe ich Kriminalfilme mit anderen Augen. Was mir auffällt, ist wie selten uns Verdächtige präsentiert werden, die sich weigern, mit der Polizei zusammenzuarbeiten. Ich habe noch nie in einem Kriminalroman oder einer Krimiserie ein “Ich habe nichts zu sagen” als Hauptverteidigungshaltung gegenüber einem Verhör gesehen oder gelesen. Wenn eine Person ihre Weigerung zum Ausdruck bringt, eine Aussage zu machen, wartet sie entweder auf ihre:n Anwält:in oder sie antwortet schließlich doch, aufgrund des Drucks der Inspektor:innen.

Und das ist durchaus verständlich. Um einen halbwegs interessanten Kriminalfilm zu produzieren, müssen die Ermittlungen voranschreiten, es muss eine Auflösung der Geschichte durch neue Elemente geben. Als Zuschauer:in haben wir ein gemeinsames Interesse mit den Ermittler:innen: Wir wollen, dass die Ermittlungen voranschreiten. Daher müssen die fiktionalen Elemente nacheinander auftauchen, größtenteils durch die Vernehmungen und Befragungen, die von den Inspektor:innen der Tatortserie gemacht werden.

Das ist das Bild, das uns die Fiktion der Polizei oftmals vermittelt: Ein Verhör besteht aus einem:r Polizisten:in der:die Fragen stellt, und einem:r Beschuldigten, der:die sie beantwortet. Ein Akt des Widerstands wird vielleicht durch den Versuch einer Lüge dargestellt, aber auf keinen Fall durch beharrliches Schweigen.

Im Bereich der Fiktion gibt es mehrere Werke, die ich für interessant halte, um die Art und Weise, wie Verhöre dargestellt werden sowie die Reaktionen der Befragten zu analysieren. Natürlich bleibt es fiktional. Es handelt sich um Autor:innen und Drehbuchautor:innen, die versuchen darzustellen, wie eine Ermittlung und ein Verhör aussehen. Aber wenn einen kritischen Blick bewahrt wird, kann die Lektüre dieser Bücher dabei helfen, sich mehr oder weniger realistisch spezifische Verhörmethoden vorzustellen und zu verstehen. 

- Klaus Kordon, Krokodil im Nacken, Beltz & Gelberg, 2008 (Buch)
- Pierre Schoeller, Les Anonymes, 2014 (Film)
- Erri de Luca, Impossibile, Feltrinelli Editore, 2019 (Buch)
- Vladimir Volkoff, Das Verhör, Fahren 1990 (Buch)

[Box]

> **Praktische Übung**
> 
> Wenn du dir das nächste Mal einen Krimi ansiehst oder liest, stell dir die drei folgenden Fragen :
> 
> 1. Wurden die Fragen der Inspektor:innen bei den Befragungen beantwortet?
> 1. Haben diese Antworten der Polizei geholfen, die Ermittlungen voranzutreiben, neue Erkenntnisse zu gewinnen oder neue Hypothesen aufzustellen?
> 1. Hätten sich die Ermittlungen weiterentwickelt, wenn diese Informationen nicht erhoben worden wären?

Aber es handelt sich um ein echtes Problem und nicht nur um ein intellektuelles Spiel.
Denn wir leben heute in einer Gesellschaft, in der die Medien, Regierungen, große Unternehmen, religiöse Gruppierungen und politische Parteien Pseudo-Wirklichkeiten herstellen und elektronische Geräte existieren, um diese Scheinwelten in den Kopf des Zuschauers, des Zuhörers und des Lesers zu bringen.

Die Polizei ist immer gut, sie gewinnt immer. Vernachlässigen Sie das nicht. Was für eine Lehre: Man soll nicht nicht gegen die Behörden kämpfen und wenn man es doch tut, ist einem die Niederlage sicher. Hier lautet die Botschaft: Bleiben Sie passiv und kooperieren Sie. Wenn Sergeant Baretta Sie um eine Information bittet, geben Sie sie ihm, denn Sergeant Baretta ist ein tapferer, vertrauenswürdiger Mann, er liebt Sie und Sie sollten ihn auch lieben”.[^Philip K. Dick, “Wie man ein Universum baut, das nicht zwei Tage später zusammenbricht” (1986, How to Build a Universe That Doesn’t Fall Apart Two Days Later)]

### Wenn ich nicht rede, wirke ich verdächtig

„Wenn Sie unsere Fragen nicht beantworten wollen, dann haben Sie etwas zu verbergen. Nur hartgesottene Kriminelle weigern sich, unsere Fragen zu beantworten.” Das ist die Art von Argument, die Polizist:innen lernen, Personen entgegenzubringen, die sich weigern, Aussagen zu machen. Niemand will verdächtig aussehen.

Verdächtig zu erscheinen bedeutet bereits, dass du quasi schuldig bist, oder? Nein, das ist etwas ganz anderes. Und in wessen Augen verdächtig erscheinen? Vor Inspektor:innen möchten wir oft unschuldig aussehen. Dabei sind es nicht sie, die ein Urteil fällen, sondern die Richter:innen. Verdächtig erscheinen ist weder ein rechtlicher Status noch ein Beweismittel.

Das Paradoxe ist, dass viele Menschen, die nicht verdächtig wirken wollen, sich rechtfertigen und damit Informationen liefern, die zu ihrer Verurteilung führen. Den Fokus auf das Bedürfnis nach Rechtfertigung, das du möglicherweise empfindest, ist eine gängige Polizeistrategie.

### Anstatt zu schweigen, wird mir Lügen aus der Patsche helfen

Ich habe ja bereits darüber geschrieben, warum ich der Meinung bin, dass Lügen eine unnötige Gefährdung ist \[Seite 112\].
Polizist:innen sind darauf trainiert, Lügen oder Fragen, die einen in Verlegenheit bringen, zu erkennen. Glaubwürdig zu bleiben und gleichzeitig zu lügen, erfordert ein hohes Maß an Konzentrationsfähigkeit, ein sehr gutes Gedächtnis, viel Vorstellungskraft und eine schnelle Auffassungsgabe. Du musst in der Lage sein, Stunden/Tage/Monate später genau die gleiche Lüge bis ins kleinste Detail noch einmal zu erzählen. Außerdem: Solange du dich in den Räumen der Polizei befindest, weißt du nicht, welche Informationen über dich in der Ermittlungsakte stehen und welche Hinweise sie vor Ort gesammelt haben. Eine der Strategien der Polizei besteht darin, dich bewusst dazu zu bringen, möglichst viele Behauptungen aufzustellen und dich dann zu entlarven, indem sie dich mit dem Beweis deiner Lüge konfrontieren.

### Ich habe mir nichts vorzuwerfen

Was ist mit den anderen? Deinen Freund:innen? Vielleicht liegt der Grund, warum du vorgeladen wurdest darin, dass die Polizei Informationen über eine:n Bekannte:n von dir suchen und nicht über dich. Manche Personen sagen, dass sie nur Aussagen über sich selbst machen wollen und nichts sagen wollen, was andere Personen betreffen würde. Dieser Gedanke entspricht jedoch nicht der Realität einer polizeilichen Untersuchung. Hier gibt es keinen Platz für verschiedene persönliche Geschichten oder für “meine eigene Geschichte”. Es gibt nur einzelne, aber miteinander verbundene Elemente, die ein Gesamtbild ergeben. Durch Addition oder Eliminierung ist jede Information ein Puzzleteil, die den Inspektor:innen ein vollständigeres und umfassenderes Bild vermittelt. Als Befragte:r bist du nicht in der Lage zu wissen, was die Polizei interessiert und was sie wie (und gegen wen) verwenden können.

Auch wenn du nicht willst, dass durch deine Aussagen andere gefährdet werden, ist das nichts, was du beeinflussen kannst. Dazu kommt noch das auch wenn du dir nichts vorzuwerfen hast, dein Protokoll aufgezeichnet wird und den Beamt:innen ermöglicht, dich besser kennenzulernen, wenn du das nächste Mal in ihr Büro kommst.

### Wenn ich wenigstens ein bisschen rede, wird der Druck aufhören

Dieser Gedanke ist verständlich. Wie bereits erwähnt, die Liste der Druckmittel die Polizist:innen haben ist lang, vor allem in Haftsituationen. Erpressung ist Teil ihres Manipulationsarsenals: “Je schneller du uns alles erzählst, desto schneller kannst du nach Hause gehen.” In der Praxis ist jedoch oft das Gegenteil der Fall. Wenn du anfängst zu reden, gibst du das Signal für eine Öffnung, welche die Inspektor:innen nutzen wollen, bis sie mit dem Ergebnis zufrieden sind. Es zeigt ihnen auch, dass ihre Druckmittel auf dich wirken. Warum also aufhören, wenn sie positive Ergebnisse bringen? Anstatt weniger Druck ausgesetzt zu sein, wirst du wie eine Zitrone ausgepresst.

**Eine Antwort führt fast immer zu einer neuen Frage.**

## 17. Sich durch Schweigen schützen

Fassen wir den Kontext in den du gestellt wirst, wenn du eine Verhörsituation erlebst, noch mal zusammen: Du hast nur lückenhafte Informationen über die Ermittlungen, die dich betreffen, und über das Material, das die Polizei in Händen hält. Du erlebst einen ungewöhnlichen und stressigen Moment. Du bist unsicher, was in Zukunft passieren wird. Wenn du in Haft genommen wirst, bist du einer ganzen Reihe von zusätzlichen Belastungen ausgesetzt, die abhängig von deinen Haftbedingungen und deinem Freiheitsentzug sind. Dir stehen Personen gegenüber, die in weitreichenden Manipulationstechniken ausgebildet sind, über viel professionelle Erfahrung in diesem Bereich verfügen und dein Profil mehr oder weniger gut kennen.

Alle Strategien und Techniken der Polizei haben eine Gemeinsamkeit: Um ihre Ziele zu erreichen, ist die Beteiligung der befragten Person notwendig, wenn nicht sogar unerlässlich. Dieses Element stellt deine beste Verteidigungsmöglichkeit dar, die du besitzt. Wenn du diese Zusammenarbeit verweigerst, zerstörst du die Waffen, die die Polizei gegen dich verwenden könnte. Ihnen nichts anderes anzubieten als ein ungerührtes “Ich möchte keine Aussage machen” bedeutet, ihnen keine Macht über dich anzubieten. Andererseits verhindert Schweigen, dass Ermittlungsakten mit deinen Aussagen – ob wahr oder falsch – gefüttert werden. Mensch bringt damit niemanden in Gefahr, weder andere noch sich selbst.

Um diese Verteidigungsachse bestmöglich zu halten, ist es am einfachsten, die Strategie der “ Sprung in der Platte” anzuwenden. Das bedeutet, dass du deine Entscheidung, keine Aussage zu machen, mit immer gleichem (nicht ansteigendem) Ton und in den gleichen Worten wiederholst, ohne dich weiter zu rechtfertigen. So kannst du deine Entschlossenheit unterstreichen, indem du immer im selben emotionalen Zustand bleibst.

 “Ich möchte keine Aussage machen”,
 “Ich möchte keine Aussage machen”,
 “Ich möchte keine Aussage machen”.

Je früher du zum ersten Mal deine Bereitschaft zum Schweigen zum Ausdruck bringst, desto leichter wird es sein, dich daran zu halten. Die Inspektor:innen werden versuchen, dich dazu zu bringen, deine Verteidigungsstrategie zu ändern, indem sie dir z.B. Schuldgefühle einreden, weil du nicht antwortest, oder versuchen, dich glauben zu lassen, dass dein Schweigen ein verdächtiges Bild von dir vermittelt, was sich zu deinen Ungunsten auswirken wird.

Wenn der Fall nach der Vernehmung an eine:n Richter:in weitergeleitet wird, hast du die Möglichkeit, die Ermittlungsakte einzusehen und damit in die Beweise, die die Polizei gegen dich gesammelt hat. Du wirst auch mit einem:r Anwält:in sprechen können und dich beraten lassen, wie du dich am besten verteidigen kannst. Wenn du dich dafür entscheidest, im Prozess Aussagen zu machen, kannst du dies in Kenntnis der Sachlage machen, wobei das Risiko deutlich geringer ist, dass du dich in Verlegenheit bringst.

**Das Schweigen ist mit Abstand die effizienteste Verteidigungsstrategie und die am wenigsten gefährliche.**

### Einige geschichtliche Hintergründe

Karl Victor Hase wurde am 23. November 1834 in Jena, Deutschland, geboren. Mit 19 Jahren beginnt er sein Jurastudium in Heidelberg, wo er zu sechs Tagen Gefängnis verurteilt wird, weil er einer religiösen Autorität widerspricht. Später tötet einer seiner Studienfreunde einen anderen Studenten bei einem Duell, das aus dem Ruder läuft. Karl Victor Hase gibt ihm seinen Pass, damit er nach Frankreich gehen kann. Nach dem Grenzübertritt wirft sein Freund den Pass weg, der schließlich gefunden und an die örtlichen Behörden in Heidelberg zurückgeschickt wird. Karl Victor, der inzwischen Jurist ist, wird der Beihilfe zur Flucht verdächtigt und verhört.

Bei seiner Anhörung wiederholt er immer wieder: “Mein Name ist Hase, ich verneine alle Generalfragen, ich weiß von nichts”. Dies verunmöglichte es den Ermittler:innen, die Wahrheit unter anderen möglichen Hypothesen zu unterscheiden: Hase ist ein Komplize und hat seinen Pass absichtlich weggegeben; Hase hat seinen Pass verloren, sein Freund hat ihn gefunden und für die Flucht verwendet; Hase hat sich seinen Pass von seinem Freund stehlen lassen.

Nachdem Hase aus Mangel an Beweisen freigesprochen wird, macht der Satz “Mein Name ist Hase, ich verneine alle Generalfragen, ich weiß von nichts” schnell die Runde an den juristischen Universitäten Deutschlands und der Niederlande. Gekürzt auf “Mein Name ist Hase, ich weiß von nichts” – konzeptualisiert dieser Satz die rechtliche Verteidigung, die das Schweigen im Angesicht einer Anklage darstellt. Bis heute ist dieser Satz in der deutschsprachigen Kultur berühmt geblieben und wurde unter anderem als populäre Redewendung und in Liedern verwendet. Karl Victor Hase wurde schließlich an der Universität Jena zum Doktor der Rechtswissenschaften promoviert.

Es dauerte bis 1966, bis das Recht zu Schweigen in Artikel 14 des Internationalen Pakts bezüglich bürgerlichen und politischen Rechten verankert wurde. “Jede Person, die einer Straftat angeklagt ist, hat Anspruch auf folgende Rechte, in gleicher Weise, und mindestens die folgenden Garantien: \[...\] nicht gezwungen zu werden, gegen sich selbst auszusagen.” Später griff der Europäische Gerichtshof für Menschenrechte dieses Recht auf und stellte es in den Mittelpunkt des Konzepts eines fairen Verfahrens. Laut dem Gerichtshof erlaubt das Recht auf Schweigen “zu verhindern, dass Beweismittel unter Zwang oder Druck erlangt werden und vermeidet somit “Justizirrtümer”. Die Geschichte zeigt uns, dass die Vermeidung von Justizirrtümern nicht erreicht wurde.

### Polizeiliche Gegenwehr

Während Verhörtechniken in Lehrbüchern ausführlich beschrieben werden, habe ich nur wenige Hinweise für den Umgang mit Personen gefunden, die eine Diskussion ablehnen. Ich interpretiere dies als sehr positiv; ich schließe daraus, dass ihnen die Mittel fehlen, um der Strategie des Schweigens wirksam zu begegnen. Sehen wir uns dennoch die wenigen Hinweise an, die sie vorbringen:

- Die Polizeibeamt:innen werden mit der befragten Person darüber sprechen, warum sie die Aussage verweigern will. Sie werden ihm:ihr versichern, dass es natürlich sein:ihr gutes Recht ist, die Aussage zu verweigern, dass sie aber aus Neugier oder um es in das Protokoll aufzunehmen, gerne wissen würden, warum. Wenn die Person antwortet, wird die Diskussion weitergehen und sie nach und nach zu den Fragen zurückführen. Dies ist eine Mischung aus der “Trichterstrategie” und der Strategie der “ Verpflichtung” \[Seite 62\]. Was du hier beachten solltest, ist, dass du dich nicht rechtfertigen solltest, warum du ihnen eine Antwort verweigerst, da dies bereits eine Form der Antwort darstellt und das Risiko enthält, dass du in die Diskussion abrutschst. 
- Sozial gesehen ist Schweigen unangenehm, besonders in einer Diskussion mit mehreren Personen. Die Interviewer:innen können damit spielen, indem sie eine Lücke hinterlassen und dich anstarren, nachdem du erklärt hast, dass du ihre Fragen nicht beantworten willst. Die Idee dahinter ist, dass du dich unwohl fühlst, um dich zu Antworten zu drängen. Wenn du dich dadurch verunsichert fühlst, kannst du deine Fantasie spielen lassen, um dich aus diesem Moment herauszuziehen, indem du an schöne Momente denkst, die du erlebt hast oder die du gerne erleben würdest. 
- Eine weit verbreitete Strategie ist, dass die Polizist:innen deine Weigerung zu Antworten “akzeptieren”, dir aber trotzdem alle Fragen vorlesen und dich bitten, sie einzeln zu beantworten um dich bei jeder Frage wiederholen zu lassen, dass du sie nicht beantworten möchtest. Die Idee dahinter ist, dir ein Gefühl des Verlangens nach Rechtfertigung vermitteln wollen, indem sie dir sehr spekulative und suggestive Fragen vorlesen. Lass dich nicht darauf ein. Ein einziges Mal zu sagen “Ich möchte Ihre Fragen nicht beantworten” ist völlig ausreichend. Bleibe stoisch und schweigsam, bis sie des Wartens müde sind und dich in Ruhe lassen. 
- Die Polizei versucht auch oft, die befragte Person einzuschüchtern, indem sie sie glauben lässt, dass die Weigerung, sich zu äußern, in einem möglichen Gerichtsverfahren gegen sie verwendet wird.
- Selbstverständlich ist die Anwendung von körperlicher Gewalt gegen dich im Rahmen der von mir erwähnten Lehrbücher grundsätzlich verboten. Deshalb wird sie hier auch nicht erwähnt. Wir wissen jedoch alle dass Polizist:innen auch innerhalb des “gesetzlichen” Rahmens, der ihnen zustehen sollte, manchmal über die Stränge schlagen. \[Seite 106\]

[Box]

> **Die Ausnahme, die die Regel bestätigt**
> 
> Wie jede Regel hat auch die Regel des Schweigens eine Ausnahme, die sie bestätigt. Es handelt sich um den speziellen Fall von Routinekontrollen. Nehmen wir als Beispiel eine Routinekontrolle durch Zollbeamt:innen in einem Zug, der über eine Grenze fährt. Die Zollbeamt:innen gehen durch den Zug, um ihre Kontrolle durchzuführen. Ihr Ziel ist es, Personen ohne gültige Aufenthaltbewilligung oder mit illegalen Waren anzuhalten. Die Grenzbeamt:innen haben weder die Zeit noch die Ressourcen, um bei jeder Person anzuhalten und eine Identitätskontrolle durchzuführen, geschweige denn, um das Gepäck zu überprüfen. Um zu arbeiten, verwenden sie Filter, um eine Auswahl zu treffen. Der erste Filter ist die berüchtigte Gesichtskontrolle, die auch als racial profiling[^Racial Profiling oder ethnische Profilerstellung bezeichnet das diskriminierende Verhalten der Polizei gegenüber einer Einzelperson oder einer Gruppe von Einzelpersonen aufgrund ihrer tatsächlichen oder wahrgenommenen rassischen oder religiösen Herkunft. Es ist ein Begriff, der mit Gesichtskontrolle gleichbedeutend ist, aber die inhärent rassistische Seite dieser Praxis hervorhebt. Weitere Informationen zu diesem Thema findest du auf der Website der Schweizer Allianz gegen Racial Profiling: stop-racial-profiling.ch] bezeichnet wird: gezielt überprüft werden Jugendliche, nichtweiße, arme oder alleinstehende Personen. Kurz gesagt alles, was von der weißen, bürgerlichen und sozialen Norm abweicht.
> Doch selbst bei diesem ersten Filter bleiben immer noch zu viele Personen für eine gründliche Kontrolle übrig. Der zweite Filter ist genau das, was als Routinekontrolle bezeichnet wird. Die Zöllner:innen halten kurz an und stellen dir 3-4 Fragen, die völlig banal klingen. Woher kommen Sie? Waren Sie auf einer Geschäftsoder Urlaubsreise? Wohin fahren Sie? Und so weiter und so fort.
> Sofern du dir nicht selbst widersprichst, sind die Antworten nicht wirklich wichtig. Es ist die Art und Weise, wie du antwortest, die beobachtet wird. Wirken Sie nervös? Wie ist deine Körpersprache?
> Zögerst du, bevor du antwortest? Zittern deine Hände? Genau wie bei einem Verhör können die Polizeibeamt:innen versuchen herauszufinden, ob du sie anlügst, ob du etwas verheimlichst oder ob es sich um ein sensibles Thema handelt. Wenn dies der Fall ist, werden deine Papiere und dein Gepäck überprüft und du wirst genauer befragt.
> 
> Diese Praktiken kommen auch bei Verkehrskontrollen oder am Rande von Demonstrationen vor. Wenn du in einer solchen Situation «Ich sage nichts und mache von meinem Recht zu schweigen Gebrauch» antwortest, ist die Wahrscheinlichkeit groß, dass du auf einer Polizeiwache für eine eingehende Kontrolle landest.
> 
> Diese Form des Kontakts mit den Ordnungskräften ist ein ganz besonderer Moment. Du bist bereits nicht mehr völlig frei und antwortest unter Zwang, aber du wirst auch nicht verhaftet oder in einem Ermittlungsverfahren angeklagt. Die Herausforderung besteht darin, ruhig zu bleiben und nur das Nötigste zu antworten: „In meinem Koffer? Oh, schmutzige Kleidung und Bücher, ja, ich habe Freunde besucht, jetzt bin ich auf dem Weg nach Hause “.

> Wenn du dich in die Enge getrieben fühlst und die Fragen zu präzise werden, ziehe deinen Schutzpanzer an: Ich möchte keine weiteren Aussagen machen.

>> [Buch]
>> 
>> “Die Polizei ist mit ausdrücklicher und gesetzlicher Genehmigung überall dort präsent, wo Arbeiter:innen und People of Colour wohnen. Die Gerichte bestätigen immer wieder die Interpretationen der Polizei. Jemanden verhaften, weil er in einem “Gebiet mit hoher Kriminalität” läuft? Völlig legal. Ein Auto nach Drogen durchsuchen, weil der schwarze Fahrer zu lange an einem Stoppschild angehalten hat? Völlig akzeptabel.
>>
>> Wie die Polizei oft scherzhaft zu Racial Profiling sagt: “Das gibt es nicht, funktioniert aber sehr gut.“
>> (Naomi Murakawa, Polizeireformen funktionieren... für die Polizei, 2020)

## 18. Mentale Verteidigung

### Stressquellen abmildern

Wenn du mit einem Gerichtsverfahren, einem Verhör und/oder Polizeigewahrsam konfrontiert wirst, kann das starke Stressgefühle auslösen, die deine Fähigkeit zur mentalen Selbstverteidigung schwächen. Die Inspektor:innen sind sich dessen bewusst und werden versuchen, dieses Phänomen noch zu verstärken. Glücklicherweise können einige Stressquellen antizipiert werden.

Ich unterscheide zwischen zwei Arten von Stressquellen:

*Stress vom jetzigen Moment.* Hierbei handelt es sich um den Druck, der durch die Bedingungen im Moment des Gerichtsverfahrens/ Verhörs/Polizeigewahrsams selbst hervorgerufen wird. Habe ich die Möglichkeit auf die Toilette zu gehen, langweile ich mich, ist mir kalt oder heiß, habe ich Zugang zu Medikamenten oder Hygieneartikeln? Bin ich mit systemischer Unterdrückung wie Rassismus, Antisemitismus oder Transphobie konfrontiert? Drohungen und Feindseligkeit von anwesenden Polizist:innen können ebenfalls Stress verursachen.

*Stress von der Außenwelt.* Diese Stressquellen sind Teil der Außenwelt des gegenwärtigen Zeitpunkts, der Inhaftierung oder des Verhörs: Der Druck, der dadurch entsteht, dass dein soziales Umfeld draußen sich Sorgen um dich macht, was die Polizei bereits an Informationen hat oder in den bevorstehenden Ermittlungen finden könnte und wie es deinen Freund:innen draussen geht, von denen du nichts gehört hast, sich Sorgen machen über die Strafe, die dir droht, und darüber, wie die Leute über dich denken werden, wenn du verurteilt wirst. Die Inspektor:innen wissen, wie sie diese Stressquelle schüren können. Indem sie Andeutungen machen, versuchen sie Zweifel in dir zu erzeugen.

**Sie wissen, dass du, wenn du wieder in der Zelle bist, das ganze Verfahren noch einmal Satz für Satz durchlaufen wirst und hoffen, damit deine Gedanken und deine Entscheidungsfindung zu beeinflussen.**

Auf der einen Seite gibt es also all das, womit du direkt konfrontiert wirst und was dich in Verlegenheit versetzt und in dir Stress und Ängste verursacht. Auf der anderen Seite sind es Stressquellen, die außerhalb deines Einflussbereichs liegen. Was dich stresst, ist ein Teufelskreis aus Kopfzerbrechen, Vorwegnahmen, Annahmen und Vorstellungen darüber, was draußen passiert und was in deiner nahen Zukunft passieren wird.

Mein Rat ist, diese beiden Stressherkünfte voneinander zu trennen. Nimm dir einen Moment Zeit darüber nachzudenken, welche Stressquellen des gegenwärtigen Augenblicks, mit denen du direkt konfrontiert bist, dich beeinflussen. Dann suche nach Antworten, die für dich umsetzbar sind. Ist das Alleinsein eine Belastung für dich? Vielleicht kann es helfen dich weniger einsam zu fühlen wenn du Briefe an deine Liebsten schreibst[^Auch wenn du keine Briefe verschicken kannst, weil es dir verboten ist, hindert dich nichts daran, einen Brief an eine Person deiner Wahl zu schreiben. Es ist eine schöne Art, sich die Zeit zu vertreiben und eine Verbindung zu einer entfernten Person herzustellen.]. Beängstigt dich die bedrohliche Haltung von Polizist:innen? Denke daran, dass ihre Macht begrenzt ist und dass sie bewusst versuchen, dich mit ihrem Verhalten einzuschüchtern. Beängstigt dich die Situation des Verhörs? Erfinde eine Geschichte oder erinnere dich an einen positiven Moment, um dein Gehirn abzulenken, damit du an etwas anderes denken kannst. Nimm dir Zeit, um zu verstehen, was eine Stressquelle ist und wie du sie minimieren kannst. Vielleicht musst du mehrere Strategien ausprobieren, bis du die Richtige findest.

Was die Dinge betrifft, auf die du keinen Einfluss hast, so lass sie wie Wassertropfen an dir abprallen. Wenn du in Haft bist oder verhört wirst, ist das eindeutig nicht der richtige Zeitpunkt, um dich damit zu konfrontieren. Spar dir deine Energie für das, was deine Gegenwart betrifft, und lass die Zukunft auf sich beruhen. Du wirst Zeit haben, dich auf deinen Prozess und die rechtlichen Konsequenzen vorzubereiten, dich mit der Perspektive der Anderen auseinanderzusetzen, um wiedergutzumachen, was wiedergutzumachen ist, um dich selbst, deine Nächsten, sowie die Opfer des Falls zu kümmern und Selbstkritik zu üben. In diesem Moment ist es wichtig, sich den Polizist:innen entgegenzustellen, die nach Lücken in deinen Verteidigungsachsen suchen.

Um dich nicht von angstauslösenden Gedanken überwältigen zu lassen, ist es gut, ihnen nur begrenzten Platz im Kopf einzuräumen. Zum Beispiel, indem du eine Stunde pro Tag dafür einplanst, dich mit allen äußeren Faktoren, die dich belasten und unter Druck setzen, auseinanderzusetzen und darüber nachzudenken. Öffne die Box und überlege dir, was du rechtlich und sozial riskierst, wie es dir geht und wie es deinen Angehörigen geht, welche Folgen deine Abwesenheit draussen hat, wie du dich verhalten sollst und wie du das Leid, dass du vielleicht bei Anderen verursacht hast, wieder gutmachen kannst. Lass die Gefühle zu, die diese Fragen in dir auslösen, erlaube dir, sie zu fühlen. Vielleicht tut es weh, ist schwierig und du fühlst dich verloren. Aber Gefühle wollen gehört werden und das ist der beste Weg, um zu lernen, konstruktiv mit ihnen umzugehen und den negativen Einfluss, den sie auf dich haben, zu verringern.

Um dich mit all diesen angstbesetzten Themen zu konfrontieren, kannst du Hypothesen darüber aufschreiben, was dir und was draußen passieren könnte. Einige Annahmen sind sehr optimistisch, einige sehr pessimistisch. Vergleiche sie und spüre die Gefühle, die sie hervorrufen. Findest du sie glaubwürdig? Was kannst du tun, um sie zu beeinflussen? Wenn die Zeit abgelaufen ist, schließe die Box, mach deinen Kopf frei und denke an etwas anderes. Ein kleines Ritual kann dir helfen, wieder in den gegenwärtigen Moment zu kommen. Zum Beispiel eine Sporteinheit, das Lesen eines Kapitel eines Buches oder das Aufschreiben deiner Gedanken. Schließe dann deinen Geist gegen alle Andeutungen von Inspektor:innen so hermetisch wie möglich ab. Wenn sie ein Thema ansprechen, das dir Angst macht, lass es an dir abprallen und konzentriere deine Gedanken auf etwas anderes. Du kannst in der Zeit darüber nachdenken, wenn du dich wieder diesen Themen widmest. Auf diese Weise verringerst du die Macht, die die Ermittler:innen über dich haben können, wenn sie versuchen, dich durch Themen unter Druck zu setzen, die dir Angst machen.

**Wenn du dich weigerst, dir deine Ängste und Befürchtungen einzugestehen, nehmen deine Emotionen einen Umweg und sie manifestieren sich in Form von Wut, irrationalem Verhalten, bedrohlichen Ängsten oder Weinkrämpfen.**

Wenn du deine Angst oder andere schwierige Emotionen verleugnest, verwehrst du dir die Chance, dir selbst gegenüber wohlwollend zu sein und dir die Unterstützung und den Trost zu geben, den du brauchst. In der patriarchalen Vorstellung wird uns Stärke als ein “männlicher” Wert auferlegt, der Schwäche ausschließt. Stark ist die Person, die keine Angst hat und keine Verletzlichkeit besitzt. Die Verinnerlichung dieses Wertesystems führt dazu, dass viele Menschen ihre Ängste vor sich selbst und vor anderen verbergen und damit einen angemessenen Umgang mit ihnen zu verhindern versuchen. Feministische Strömungen bieten uns eine andere Sichtweise. Wir sind alle verletzlich und jede:r hat Schwächen. Die Stärke liegt darin, sie anzunehmen und mit ihnen zu leben, ohne sich von ihnen beherrschen zu lassen.

Bleibe angesichts der drohenden Verhaftung nicht allein und isoliert. Ohne auf gefährdende und unnötige Details einzugehen, kannst du dieses Risiko mit deinem vertrauten Umfeld thematisieren. Sich im Vorfeld gemeinsam auf eine mögliche Verhaftung oder Polizeigewahrsam vorzubereiten, hilft, diesen Moment gelassener zu erleben. Je mehr du darüber gesprochen hast und je mehr du dir überlegt hast, was du im Fall der Fälle tun kannst, desto weniger musst du dir Sorgen darüber machen, was draußen passiert. Hinterlasse deinen Freund:innen konkrete Hinweise, wie sie reagieren, wen sie benachrichtigen (Familie, Arbeitgeber) und was sie mitteilen sollen, ob es Pflanzen zu gießen gibt oder eine Katze zu füttern, welche:n Anwält:in sie kontaktieren können usw. Eine sichere und effektive Möglichkeit, diese Informationen weiterzugeben, besteht darin, sie einer Vertrauensperson auf einem verschlüsselten USB-Stick[^Eine Anleitung zur Erstellung eines verschlüsselten USB-Sticks findest du in der Anleitung zur Nutzung des Tails-Live Betriebssystems von Capulcu - https://capulcu.blackblogs.org] weiterzugeben.

Distanzierung von der Polizei Wie wir gesehen haben, bauen viele Manipulationsstrategien von Polizist:innen auf einer emotionalen und menschlichen Bindung zwischen den Polizist:innen und dir auf. Je stärker diese Bindung ist, desto leichter ist es, sie als Hebel zu benutzen, um Schuldgefühle in dir auszulösen, dich zu beruhigen, dich zu beunruhigen, dich zu kritisieren, dir Hoffnungen zu machen und deine Aufmerksamkeit zu beanspruchen. Kurzum, desto leichter ist es, deine Gefühle zu beeinflussen.

Um dem entgegenzuwirken, erinnere dich an die Asymmetrie der Situation. Auf der einen Seite bist du Beschuldigte:r in einem Verfahren, in dem der Staat dich einsperrt, gegen dich ermittelt, um über deine Schuld zu entscheiden und wirst von der Polizei unter Druck gesetzt. Auf der anderen Seite, die Polizist:innen, die ihrer routinemäßigen Arbeit als Beamt:innen nachgehen. Menschen in deiner Situation sehen sie zu Hunderten. Gezielt versuchen sie, eine verständnisvolle und fürsorgliche Haltung zu zeigen, um bei dir ein Verpflichtungsgefühl zu erzeugen \[Seite 62\]. Wenn sie dir einen Gefallen tun, ist das, weil sie dir zuvor den Tag versaut haben, damit du nun für eine Flasche Wasser oder einen Kaffee dankbar bist. Sie sind dafür verantwortlich, dass du nicht in Ruhe zu Hause sitzen, deinen Kaffee trinken und ein Buch lesen kannst. Die Weigerung, sich auf ein Gespräch mit ihnen einzulassen, ist ein sehr wirksames Mittel, um eine emotionale Distanz zwischen dir und den Inspektor:innen aufrechtzuerhalten.

Eine goldene Regel, um sich vor jeder Art von Manipulation zu schützen, besteht darin, keine Kritik von Seiten eines:r Manipulator:in als würdige Reflektion zu betrachten. Wenn dein Verhalten oder deine Handlungen dein Umfeld stören, höre dir die Kritik derjenigen an, die dir gut gesinnt sind. Kritik und Selbstkritik, sofern sie konstruktiv und ehrlich sind, lassen uns wachsen und uns weiterentwickeln. Dies erfordert jedoch einen zwangsfreien Rahmen, in dem die kritisierten Personen und Diejenigen, die die Kritik äußern, sich auf Augenhöhe begegnen. Die Polizist:innen interessiert es nicht, wer du bist, was du dir wünschst, was für dich wichtig ist und welche Empfindlichkeiten du hast. Sie haben ihre eigene Agenda und ihre eigenen Interessen, die nichts mit deiner Person zu tun haben. Die Inspektor:innen können dir sagen, dass sie es gut mit dir meinen, dass sie es für dich tun, aber wer hat dich in diesen Raum gesperrt, dich bedroht und unter Druck gesetzt?

\[Polizei-Zitat\]

> Es ist mir schon passiert, dass ich um 22 Uhr wieder zur Arbeit gegangen bin, weil ich das Gefühl hatte, dass die Person sich etwas von der Seele reden wollte, sich anvertrauen will. Tagsüber war viel los, viele Menschen, viel Lärm. Um 22 Uhr komme ich in die Polizeistation, biete ihm einen Kaffee an und wir unterhalten uns. Plötzlich bin ich sein Psychiater. Endlich beginnen wir richtig zu diskutieren und ich bekomme ein Geständnis.

### Die Entscheidungsmacht behalten

Eine direkte Folge des Polizeigewahrsams oder der Untersuchungshaft ist, dass deine Macht beraubt wird, Entscheidungen zu fällen. Du entscheidest nicht mehr, wann du essen darfst, wann du soziale Interaktionen hast, wann das Licht in der Zelle ein- oder ausgeschaltet wird, wen du sehen kannst oder ob du etwas zu lesen bekommst. Dieses Gefühl ist schwierig zu bewältigen. Es ist besonders gefährlich, wenn es sich in das Verhör einschleicht und dir unbewusst den Eindruck vermittelt, dass dir, da du keine Entscheidungsmöglichkeit mehr in deinem Alltag hast, auch nicht möglich ist, die Zusammenarbeit mit der Polizei zu verweigern.

Einen Weg, dem zu widerstehen, besteht darin, sich - so gut es geht - eine Tagesstruktur zu schaffen. Entscheide dich zu Beginn des Tages, wie du deine Zeit einteilen willst. Entscheide zum Beispiel, dass du zwei Stunden Sport treibst, eine Stunde schreibst und dich zwei Stunden mit angstbesetzten Themen auseinandersetzt. Natürlich ist es durchaus möglich, dass du mitten in deiner Sporteinheit von den Polizist:innen unterbrochen wirst, die dich abholen und zu einem Verhör oder einem Spaziergang bringen. Merke dir, wie viel Zeit du dir noch eingeteilt hast, und wenn du zurückkommst, beende dein Training und mache deine zwei Stunden Sport, wie du es dir vorgenommen hast. Die Idee ist, sich Ziele zu setzen die du erreichen kannst und dabei das Gefühl der Wertschätzung zu genießen, das dir das Erreichen dieser Ziele verleiht. Übrigens: Sport beschleunigt die Herzfrequenz und verteilt Blut, Sauerstoff, Hormone und Neuromediatoren im gesamten Körper besser. In mancher Hinsicht hat Sport durch die Bildung von Serotonin chemische Effekte, die mit denen von Antidepressiva vergleichbar sind. Außerdem ist es eine sehr gute Möglichkeit sich abzulenken.

Eine weitere Strategie besteht darin, die Orte zu würdigen, an denen du erfolgreich Widerstand leistest, anstatt dich auf jene zu konzentrieren, in welchen du dem Druck erliegst. Du kannst dich nicht frei bewegen, du bist inhaftiert, die Polizei durchsucht dein Leben und jenes deiner Lieben. Du kannst nichts dagegen tun. Es gibt jedoch Dinge, die du tun kannst. Du kannst ihnen etwas verweigern. Deine Mitarbeit bei ihrer Arbeit zum Beispiel. Verweigerung der Herausgabe von Informationen, die sie verlangen, sich weigern, ihre Erpressungen und Feilschereien zu akzeptieren. Deinen Prinzipien treu zu sein, ist ein Akt des Widerstands, den du wertschätzen solltest und aus dem du viel Kraft ziehen kannst.

[Buch]

> Lenz blickte sie an, seine beiden Gegenüber, und freute sich: Sie konnten ihn einsperren, konnten ihn tage- wochen-, monatelang in Einzelhaft schmoren lassen, sie konnten ihm Hannah und die Kinder nehmen und über die weiter Zukunft seiner Familie bestimmen, zweierlei aber konnten sie nicht: Sie konnten ihn nicht zum Idioten und nicht zum Schmutzfinken machen. Da war ihre Macht am Ende. Wie befriedigte und bestärkte ihn dieses Gefühl, wie hob es sein Selbstbewusstsein.
> 
> Auszug aus dem Buch, Krokodil im Nacken, Klaus Kordon, 2008

### Loslassen

Neben der Arbeit an der Wiedererlangung deiner Entscheidungsmöglichkeiten macht es auch Sinn, das Loslassen zu lernen. Vom Zeitpunkt deiner Verhaftung an hast du keine Kontrolle mehr über die Geschehnisse und kannst sie nicht mehr beeinflussen: Wann die Haft endet, wie viele Verhöre du über dich ergehen lassen musst, welche Manipulationen die Polizist:innen vornehmen, was draußen passiert. Ob du dich aufregst oder ruhig bleibst, die Ereignisse werden ihren Lauf nehmen. Wenn du dich jedoch nervst, z.B. indem du versuchst, dich zu rechtfertigen oder Informationen zu liefern, um aus der Sache herauszukommen, kannst du Fehler machen und die Situation verschlimmern. Am besten ist es, loszulassen und die Zeit, den Druck und den Stress über dich ergehen zu lassen, ohne zu versuchen, dagegen anzugehen. Es ist eine schlimme Zeit, die du durchmachen musst, aber sie wird irgendwann vorbeigehen und es kommt darauf an, sie so gut wie möglich zu überleben. Das bedeutet, dass du deine Situation nicht verschlimmern darfst, indem du dem Druck nachgibst und Fehler machst.

Hier ist eine Technik zum Loslassen, die leicht in der Haft oder im Verhör angewendet werden kann[^Heutzutage gibt es viele mehr oder weniger gute Ratgeber und Handbücher zum Thema “Loslassen”. Zögere nicht einen Blick darauf zu werfen.]: Wenn du merkst, dass du deine angstauslösenden Gedanken nicht mehr auf Distanz halten kannst, höre auf mit dem, was du gerade tust! Nimm einen tiefen Atemzug und blockiere deine Atmung. Bleibe so lange wie möglich in der Apnoe, während du laut und so schnell wie möglich alle Gegenstände in deiner Umgebung aufzählst. Du wirst sehen, wie effektiv das ist, um trübe Gedanken zu vertreiben.

[Buch]

> Der Leutnant ließ ihn mal wieder längere Zeit schmoren. Aber das beunruhigte Lenz nicht mehr.
> 
> Er wusste inzwischen, dass er sich das Leben nur selbst schwer machte, wenn er auf jede Vernehmung wie ein Verdurstender auf ein Tropfen Wasser lauerte. Er konnte nichts tun, die Stasi führte hier Regie; Warten kann man lernen. Irgendwann würden sie ihn ja doch wieder holen müssen. Er war hier im Untersuchungsgefängnis, nicht im Strafvollzug; sie brauchten ihre Verwahrräume für neue Staatsverräter.

Auszug aus dem Buch, Krokodil im Nacken, Klaus Kordon, 2008

## 19. Projektion und heroische Haltung

Eine der starken und brutalen Auswirkungen von Repression ist ihr unerwarteter Charakter, ihr Überraschungseffekt, welche die Umwälzung des Alltags mit sich bringt. In den meisten Fällen trifft es dich, ohne dass du darauf vorbereitet bist. Bei diesem Thema kann dazu geneigt werden, zu denken, dass es nur den anderen passiert. Leider ist das nicht der Fall. Diese Unterdrückung kann jede:n, überall und jederzeit treffen. Sich allein oder mit seinem Umfeld gedanklich in eine solche Situation hineinzuversetzen kann helfen, sich besser darauf vorzubereiten. Projiziere deine Ängste und Schwächen, denke über deine Verwundbarkeiten nach. Für welche Strategien der Cops wärst du anfällig? Welche Gefühle würden Einsperrung und Isolation in dir hervorrufen?

Um sich in eine solche Situation hineinzuversetzen und deine eigene Reaktion zu antizipieren, kannst du dich von Erzählungen und von Personen inspirieren lassen, die von ihren eigenen Erfahrungen in diesem Bereich berichten:

- Thomas Braven, Vogelperspektiven, paranoia City, 1989
- Assoziation A, Wege durch den Knast, 2022
- Ihar Alinevich: Auf dem Weg nach Magadan, Anarchist Black Cross Dresden, 2019
- Andreas Krebs Salih, Der Taifun. Erinnerungen eines Rebellen, 2020
- Georges Jackson, In die Herzen ein Feuer, Bertelsmann, 1980
- Zeitschrift “Gefangenen Infos”
- Marco Camenisch, Lebenslänglich im Widerstand, Kurt Brandenberger, 2015
- Juan José Garfia, Adios Prison – Die Geschichte einiger spektakulärer Ausbrüche, München 2019

Angesichts der staatlichen Repression halte ich es für sehr wichtig, keine “männliche” Haltung einzunehmen, bei der versucht wird, sich mit der Polizei auf ihrem eigenen Terrain zu messen. Die Polizei ist eine brutale und gewalttätige Institution, die ein asymmetrisches Machtverhältnis aufbaut, um das Individuum zu brechen. Durch emotionale Ansteckung kann die Idee aufkommen, dass du deinerseits stark und heldenhaft sein musst, um Widerstand zu leisten. Das ist das Bild des starken Kriegers, der keine Schwäche zeigt und sich nicht von den Angriffen der Polizei treffen lässt. Bis alles Risse bekommt und in Stücke zerbrechen wird, sobald du merkst, dass dieses Bild von dir nur eine Fantasie war.

Ich möchte stattdessen zu einer feministischen und resilienten Haltung ermutigen. Unsere Verwundbarkeiten und Schwächen zu erkennen, zu akzeptieren und anzunehmen, scheint mir sehr wichtig zu sein. Die maskuline Haltung, die in jeder Schwäche etwas Abwertendes und Beschämendes sieht, erscheint mir idiotisch. Tatsache ist, dass das Erleben einer Inhaftierung oder eines Verhörs etwas Hartes, Unangenehmes (und potenziell Traumatisierendes) ist. Jeder Mensch wird es aufgrund seiner Empfindlichkeiten anders erleben, aber alle werden davon betroffen sein. Daher gilt: Je besser wir unsere eigenen Ängste und Schwächen kennen, desto besser können wir uns darauf vorbereiten. “Nichts kann mir etwas anhaben, ich bin zu krass” wird dich nicht schützen, sondern dazu führen, dass du überrascht sein wirst, deine Verwundbarkeiten in dem Moment zu entdecken, in dem du sie erlebst, was keine gute Grundlage ist, um sich ihnen zu stellen und sie zu überwinden.

Du kannst auch mit deinem Umfeld Fragen thematisieren, die mit Repression zusammenhängen. Wie würden sie es erleben, wenn du inhaftiert würdest und sie keine Möglichkeit hätten, mit dir in Kontakt zu treten. Seine eigenen Verletzlichkeiten und Ängsten zu empfangen, ist der erste Schritt, um ihnen anschliessend selbstbewusst begegnen und sie überwinden zu können.

## 20. Auf sich selber und andere aufpassen

> “Take care of each other so we can be dangerous together.”
-- Queer-anarchistischer Slogan

Einen Moment der Inhaftierung oder eine Konfrontation mit der Polizei zu erleben, kann als traumatischer Moment Spuren hinterlassen. Wenn du erst einmal draußen bist und dich freust, dass es vorbei ist, kann es passieren, dass die langfristigen Auswirkungen des Ereignisses leicht unterschätzt werden.

Es gibt einige Hinweise, um zu bemerken, ob sich ein solches Trauma in dir manifestiert hat. Zum Beispiel Panikattacken, Schuldund Schamgefühle, Selbstvorwürfe, Verlust der Lebensfreude, Gefühl von Einsamkeit und Verlassenheit, Wertlosigkeit, die Unfähigkeit Entscheidungen zu treffen, dein politisches und zwischenmenschliches Engagement in Frage zu stellen, ein Gefühl, dass das Leben keinen Sinn, keinen Wert oder keine Bedeutung mehr hat, und schließlich ein Wiederauftreten von Erinnerungen traumatischer Erlebnissen. Manchmal treten diese Reaktionen erst lange nach den Ereignissen auf - z. B. einige Wochen oder sogar Jahre später.

Wenn du solche Hinweise auf ein Trauma feststellst, rate ich dir, nicht allein zu bleiben, sondern dich mit kompetenten Freund:innen zu umgeben, denen du dich anvertrauen kannst und eine:n Traumatherapeut:in aufzusuchen. Wenn du über die Ereignisse schreibst und nachvollziehst, was passiert ist, wie du dich gefühlt hast und welche Gefühle in dir ausgelöst wurden, kann das eine gute Möglichkeit sein, das Geschehene zu externalisieren und so zu verarbeiten. Außerdem kann es hilfreich sein, eine vollständige Zeugenaussage zur Hand zu haben, wenn es um das weitere Vorgehen geht. Du könntest sie z. B. bei der Gerichtsverhandlung benötigen.

Wenn jemand, der dir nahe steht, gerade einen solchen Moment erlebt hat, erkundige dich nach ihrem:seinem Zustand und ihren:seinen Bedürfnissen. Es gibt nichts Besseres, wenn du aus einer feindseligen und gewalttätigen Umgebung kommst, als zu merken, dass sich Menschen um dich und deinen Zustand kümmern und da sind, um dich in dieser schwierigen Zeit zu unterstützen.

### Gesprächsrunde

Eine Gesprächsrunde[^Frei übernommen und angepasst aus dem Buch und Podcast “Le coeur sur la table, Victoire Tuaillon, Binge Audio, 2021”] ist ein gutes Instrument, um deine Erfahrungen nach außen zu tragen und dir bewusst zu machen, dass du mit dem was du erlebt hast, nicht alleine bist. Triff dich mit mehreren Personen, die Erfahrungen in Haft oder mit Verhören gemacht haben und mit denen du dich sicher fühlst. Jede:r kann abwechselnd erzählen, was er:sie über seine:ihre Erfahrungen teilen möchte. Wie ist es passiert, welche Ängste und Zweifel sind aufgetreten, Schwierigkeiten, Stress oder auch die mittel- bis langfristigen Auswirkungen auf den eigenen Alltag. Die anderen stellen keine Fragen, üben keine Kritik, beurteilen nicht, sondern hören ausschließlich zu. Und das ist schon eine ganze Menge.

Es ermöglicht uns zu erkennen, das wir nicht alleine sind, dass das was wir durchmachen, fühlen oder erleben, auch anderen bekannt ist. Wir erkennen, dass Gefühle und Erfahrungen, die wir für anekdotisch, beschämend oder unbedeutend hielten, mit denen anderer Menschen übereinstimmen. Und können dabei entdecken, dass es auch anderen helfen kann, darüber zu sprechen. In einer Gesprächsrunde können Tabus gebrochen werden und es wird Möglich aus der Einsamkeit herauszukommen, zu lernen anderen zuzuhören und sich selbst auszudrücken und vor allem zu lernen, seine Erfahrungen zu politisieren.

### Briefe an Gefangene schreiben

Die aktive Unterstützung von Gefangenen ist eine alte Tradition in verschiedenen anarchistischen Kreisen. Eine gängige Praxis ist es, Briefe an Gefangene zu schreiben. Die soziale Isolation ist ein integraler Bestandteil der Gefängnislogik. Inhaftierte sind bewusst von der Außenwelt, ihrem sozialen Umfeld und politischen Bewegungen abgeschnitten. Einen Teil dieser Isolation durch einen Briefkontakt zu durchbrechen, kann eine sehr konkrete Hilfe sein, um das Leben im Gefängnis zu ertragen. Dies kann im Postkartenformat geschehen, durch einem langfristigen Briefwechsel sowie das senden von Zeitschriften, Zeitungen oder gedruckte Artikel aus dem Internet oder durch gemeinsam Briefrollenspiele[^Für einen Leitfaden zu diesm Thema siehe den Artikel «Rollenspiele gegen die Gefängnishölle» auf projet-evasions.org.].

Es ist nicht immer einfach, mit Gefangenen in Kontakt zu treten. Einige wollen ihre Anonymität wahren, während andere so isoliert und abgeschnitten sind, dass sie keine Verbindung nach außen haben. Es gibt jedoch mehrere Liste von Gefangenen und sowie Anleitungen wie Briefe an Menschen im Knast geschrieben werden können.

Anleitung zum Briefe schreiben:

- https://www.abc-berlin.net/briefe
- https://mollysbg.noblogs.org/zine

Liste von Gefangenen:

- abc-wien.net
- solidarity.international
- political-prisoners.net

Die Stärke einer Gemeinschaft kann daran gemessen werden, wie sie sich um die Schwächsten kümmert. Menschen, die ihrer Freiheit beraubt sind, gehören eindeutig zu dieser Kategorie.


## 21. Und wenn alles schiefgeht?

Rufen wir uns in Erinnerung: Bei einem Verhöhr wirst du mit der Polizei konfrontiert und diese setzt viel Energie ein, um dich möglichst effektiv zu manipulieren, indem sie vor allem mit deinen Ängsten spielt und absichtlich auf wunde Punkte drückt. Es wird alles daran gesetzt, damit diese Momente für dich schlecht verlaufen. Die Idee dieses Buches ist es, dir Wissen und Werkzeuge an die Hand zu geben, damit du dich vor dem Druck eines Verhörs so effektiv wie möglich schützen kannst.

Manipulation funktioniert über Verhaltensweisen, die in unserer Gesellschaft allgemein akzeptiert sind. Das gilt z.B. für alle Prinzipien, die in unserem Moralkodex festgelegt sind (z.B. dass Mensch einander helfen muss, die Wahrheit zu sagen hat, Autoritäten zu gehorchen, antworten wenn du angesprochen wirst usw.). Desweiteren funktionieren Manipulationstrategien indem sie sich auf Schwächen stützen, die du in dir trägst und die sich aus deiner eigenen Biographie, sowie aus deinen Erfahrungen in der Vergangenheit ergeben haben. Es werden dir Fallen gestellt und vielleicht tappst du in einige davon hinein. Wenn das passiert, solltest du dich auf keinen Fall dafür schämen, dass du zum Opfer polizeilicher Manipulationsstrategien geworden bist.

Wenn ein Verhör schlecht verläuft, solltest du dich nicht schämen, Reue oder Schuld verspüren. Zusammenzubrechen, gebrochen werden, dem Druck nachgeben, das alles kommt vor.

Was nützt es, sich selbst in einem bereits feindseligen Umfeld durch Selbstgeißelung mit Schuldgefühlen zu schwächen? Das bringt dir keine Lösung im Hier und Jetzt, sondern nimmt stattdessen viel geistigen und emotionalen Raum ein, was dazu führt, dass du bei den nächsten Verhören noch weiter geschwächt wirst. Du kannst die Tür zu diesen Überlegungen im Nachhinein öffnen, wenn die Gefahr vorüber ist. Es ist sehr wichtig dies zu tun, genauso wie es wichtig ist, Fehler und Schwächen zu erkennen und zuzugeben.

Später, nach der Konfrontation mit der Polizei, wird es an der Zeit sein zu reparieren was repariert werden kann, zu verstehen und zu lernen. Auch hier kann ich nur dazu raten, empathisch mit sich selbst zu sein. Aber ich halte es auch für sehr wichtig, ehrlich und transparent in Bezug auf die eigenen Schwächen zu sein. Gegenüber sich selbst wie auch gegenüber anderen. Bei einer Befragung zu einer Ermittlung, die andere Personen betrifft, solltest du unbedingt transparent kommunizieren, was gesagt oder nicht gesagt wurde, da dies Auswirkungen auf alle betroffenen Personen hat. Das schlimmste Szenario, welches eintreten kann, ist, dass eine Person während einer Befragung zusammenbricht, der Polizei Informationen gibt, sich aber nicht traut, dies den anderen betroffenen Personen mitzuteilen. Dadurch geht einerseits wertvolle Zeit verloren, die auch hätte genutzt werden können, um sich vor den Konsequenzen dieser Informationen schützen, und andererseits löst es einen großen Vertrauensverlust aus.

„Jetzt ist eh nichts mehr zu retten”. Resignation ist ein Gefühl, zu welchem die Polizei dich zu bewegen versucht, und zwar genau wegen des Effekts der Selbstaufgabe, die es erzeugt. Wenn alles verloren ist, wirkt es zwecklos, sich in einem als sinnlos erscheinenden Widerstand zu ermüden. In Wirklichkeit ist nichts jemals völlig verloren und alles kann immer noch schlimmer werden. Wenn du z. B. eine Information preisgibst, die die Polizei gegen dich verwenden kann, ist das in der Konsequenz bereits um einiges besser als wenn du zwei Informationen preisgibst.

[Buch]

> Am nächsten Tag wurde ich in die untere Etage gebracht, um angehört zu werden. Es war ein junger Mann von etwas mehr als zwanzig Jahren, der mich verhörte. Er wollte Informationen über unsere heimliche bolschewistische Mission in Europa wissen, warum wir länger in Riga blieben, wen wir getroffen hatten und was mit den wichtigen Dokumenten geschehen war, die wir, wie er wusste, in den letzten Jahren illegal ins Land gebracht hatten. Ich versicherte ihm, dass er noch viel lernen müsse bis er den Ruf eines Ermittlers erlangt habe, der in der Lage sei, eine so große Kriminelle zu verhören, wie jene, die er vor sich hatte. Ich würde ihm nichts anvertrauen, sagte ich, selbst wenn ich Informationen besaß, die ihn interessieren könnten. Ich verriet ihm jedoch, dass ich keine Bolschewikin, sondern eine Anarchistin war.

Emma Goldman, Gelebtes Leben, Nautilus Verlag

**In Wirklichkeit ist nichts jemals völlig verloren und alles kann immer noch schlimmer werden.**

### Polizei und Justiz überwinden

> Die Polizei verhindert die Kriminalität nicht. Die Expert:innen wissen es, die Polizei weiss es, aber die Öffentlichkeit weiß es nicht. Dennoch behauptet die Polizei, sie sei die beste Verteidigung gegen Kriminalität, und betont immer wieder, wenn man ihr mehr Ressourcen zur Verfügung stelle, insbesondere mehr Personal, könne sie die Gesellschaft besser vor Kriminalität schützen. Das ist ein Mythos.

-- David Bayley, Police for the Future, 1996

Das Element, das die Polizei von einer bewaffneten Gang unterscheidet, ist, dass sie als Institution im Prozess der institutionellen Justiz verankert ist. Wo eine Straßengang gewalttätig für ihre eigenen Interessen (oder die ihrer Anführer:innen) handelt, handelt die Polizei mit Gewalt für die Strafjustiz. Die Infragestellung der Existenz der Polizei als soziale Institution ergibt, ohne die Existenz des gesamten Strafrechtssystems zu hinterfragen, keinen Sinn.

In repräsentativen Demokratien beruht das Strafsystem hauptsächlich auf der Logik der Bestrafung. Das Strafsystem bestraft, um eine Person dafür bezahlen zu lassen, dass sie gegen ein Gesetz verstoßen hat und um Andere davon abzuhalten, dasselbe zu tun. Das Leid, das der für schuldig befundenen Person zugefügt wird, soll ein Gleichgewicht wiederherstellen: Das Verbrechen soll ausgeglichen werden durch ein Leid, das die Richter:innen und das geltende Strafgesetzbuch als gleichwertig ansehen. Zu allen Zeiten und zu jeder Epoche wo es sie gab, bestraft die Institution, die sich Justiz nennt, die Übertretung der von der Regierung aufgestellten Gesetze. Ihre Funktion ist es, zu bestrafen, und sie bestraft immer abhängig vom historischen Kontext[^Weiterführend zur Thematik der modernen Justiz siehe: Michel Foucault, Überwachen und Strafen, Suhrkamp Verlag, 1975]. Ob es sich um: einen Steuerbetrug in der Schweiz im Jahr 2021, eine Papierfälschung in der Sowjetunion im Jahr 1955 oder eine Abtreibung in Irland im Jahr 2013 handelt, die Logik ist die gleiche. Eine Behörde legt einen gesetzlichen Rahmen fest, der eingehalten werden muss, die Polizei verfolgt jene, die sich nicht daran halten und die Justiz bestraft sie.

Da sie strafend ist, konzentriert sich diese Form der Justiz also hauptsächlich auf den:die “Schuldige:n” und so entwickelt der Staat ständig ein Arsenal an Strafen: Geldstrafe, Tagessätze, gemeinnützige Arbeit, Strafvollzug, Administrativhaft, Ersatzstrafe, therapeutische Strafe, Psychiatrisierung, Deportation, Eintrag ins Strafregister und so weiter.

Die Abschaffung des Strafsystems scheint mir eine zentrale Herausforderung zu sein, wenn wir die Entstehung einer Form des Zusammenlebens anstreben, die nicht auf Zwang und Autorität, sondern auf individueller Freiheit und care[^Vom Verb “to care” (sich kümmern). Von verschiedenen feministischen Strömungen konzeptualisiert, bezeichnet der Begriff Care eine Reihe von zwischenmenschlichen Praktiken, die auf Empathie und gegenseitiger Solidarität beruhen. Siehe: Care Revolution, Gabriele Winker, transcript, 2015] beruht.

Es gibt zahlreiche Ansätze, um die institutionelle Justiz zu überwinden. Viele politische Strömungen kritisieren sie und experimentieren auf konkrete Weise damit, wie eine Gesellschaft ohne Polizei, Gerichte und Gefängnisse aussehen könnte; der StrafrechtAbolitionismus natürlich, aber auch feministische, anarchistische und dekoloniale antirassistische Strömungen, sowie spezifische Bevölkerungsgruppen wie die Zapatistas in Chiapas, die Bewohner:innen Rojavas in Syrien oder verschiedener First Nation in Nordamerika.

## 22. Transformative Gerechtigkeit

Transformative Gerechtigkeit ist eine Praxis, die entwickelt wurde, um auf problematische, unterdrückerische und/oder konfliktträchtige Situationen innerhalb einer sozialen Gemeinschaft zu reagieren. Dieses Konzept der Gerechtigkeit sieht in jedem Konflikt ein einzigartiges Problem, das es zu lösen gilt, indem auf die Bedürfnisse des Opfers und die Einbeziehung der betroffenen Gemeinschaft fokussiert wird. Anstatt sich auf die Bestrafung des:r Täter:in zu konzentrieren, wird der Schwerpunkt auf die Veränderung des Schadens gelegt, insbesondere durch die Veränderung der sozialen Bedingungen, die den Schaden ermöglicht haben. Das Opfer nimmt eine aktive und zentrale Rolle im gesamten Prozess ein, während der:die Gewalttäter:in ermutigt wird, Verantwortung für seine Handlungen zu übernehmen und sich an der Wiedergutmachung der verursachten Schäden und Verletzungen zu beteiligen. Die Wiedergutmachung von erlittenen Verletzungen und Traumata, sowie die Kritik an den Mechanismen struktureller Unterdrückung sind ein wichtiger Teil davon.

Die in Oakland (USA) ansässige Organisation Generation Five, die sich mit den Themen Inzest und Kindesmissbrauch auseinandersetzt, stützt ihre Arbeit auf transformative Gerechtigkeit. Dabei setzt sie sich folgende Ziele:

- Sicherheit, Heilung und Wiedererlangung der Macht für die Überlebenden. 
- Kollektives Handeln, Erholung und Befähigung des gesamten betroffenen sozialen Umfelds. 
- Transformation der sozialen Bedingungen und der strukturellen Unterdrückungssysteme, die die begangene Gewalt ermöglichen und aufrechterhalten. 
- Befähigung und Transformation von inzestuösen Personen.

> **Die Menschen sprechen oft von Verantwortung, wenn sie “Strafe” meinen.
> 
> Sie können nicht jemand anderes zwingen eine Verantwortung zu übernehmen, sie können nur selbst Verantwortung übernehmen. Aber es ist möglich, Raum zu schaffen, um zu sehen, ob jemand Verantwortung übernehmen möchte.**

-- Mariame Keba

## 23. Ein langfristiger Prozess

**Achtung,** *in diesem Kapitel wird sexualisierte Gewalt thematisiert. Allerdings werden keine expliziten/detaillierten Handlungen beschrieben.*

Das Erlernen anderer Reaktionsformen auf Unterdrückung und zwischenmenschliche Gewalt - als die der institutionelle Justiz - ist ein Prozess, der über einen längeren Zeitraum hinweg stattfindet. Um die von den Staaten eingeführten Logiken der Kontrolle, Bestrafung und Unterdrückung zu überwinden, müssen wir wieder lernen, auf Situationen der Aggression mit anderen Begriffen zu reagieren, als mit den Konzepten von “Schuld”, “Strafe”, “Wahrheit”, “Recht” und dem gesamten Erbe des Strafsystems.

**Eine große Schwierigkeit ist, dass es uns an alternativen Konfliktlösungsmodellen mangelt, obwohl viele zwischenmenschliche Konflikte ohne Einmischung der Justiz gelöst werden.**

Wir haben nur wenige Bespiele von Menschen oder Gruppen, die sich mit Fragen der Konfliktlösung beschäftigen, anstatt sie an die Justiz zu delegieren, die sich mit Selbstverteidigung befassen, anstatt sich an die Polizei und damit an den Staat zu wenden. Fiktionen, die Konfliktlösungen zeigen, bei denen die Betroffenen von sich aus handeln, sind fast ausschließlich Geschichten, in denen eine - meist männliche - Person selbst für Gerechtigkeit sorgt und das meist in einem Blutbad. Hier geht es um Rache und nicht um kollektive Transformation und der Verantwortungsübernahme von Personen, die Gewalt ausüben. Ein solches Umdenken muss in unseren Gemeinschaften, Netzwerken, Kollektiven und Gruppierungen Gestalt annehmen. Es wird zwangsläufig von Fehlern, Misserfolgen, Infragestellung und Selbstkritik begleitet werden.

Um einen solchen Prozess zu veranschaulichen, nehmen wir ein spezifisches Beispiel[^Dieses Beispiel ist, wie auch der Rest dieses Kapitels, ist stark von dem Text “Verantwortung für uns selbst übernehmen” inspiriert, der von dem anarchistischen Kollektiv Crimethinc in 2013 veröffentlicht wurde.]: Die anarcho-punk Community in Nordamerika. Es handelt sich um eine Mischung aus einer Gesinnungsgemeinschaft und einer Underground-Kultur, in der Menschen, die sich dieser Gemeinschaft zugehörig fühlen, gemeinsame Ideen (Ablehnung und Widerstand gegenüber den Behörden, Ökologie, Antikapitalismus, Feminismus), Praktiken (DIY, Aufstände, Lebensmittelrettung) und geteilte geografische Orte (Konzerte, Demonstrationen, besetzte Häuser und selbstverwaltete Sozialzentren) teilen.

Im Laufe der 1990er Jahre reagierten Frauen und Überlebende auf verschiedene Weise gegen das Problem sexualisierter Übergriffe, welche in dieser sozialen Gruppe weit verbreitet ist: Zines[^Das Wort Zine wird als Synonym von Fanzine gebraucht, einer von Fans für Fans hergestellten Zeitschrift. In politischen Kontexten aber schlicht als “Zine” bezeichnet], in welchen sexuelle Übergriffe thematisiert sowie Zusammenfassungen der Schriften der erwähnten Autor:innen auf Konzerten verteilt werden; Diskussionsgruppen von Personen, die von Übergriffen betroffen sind, werden gegründet; die anarcho-punk Communities in benachbarten Städten werden vor rückfälligen Tätern gewarnt und in einigen Fällen werden Personen gewaltsam aus dieser Subkultur vertrieben. In Portland war das Hysteria eines der ersten Kollektive, die versuchten, eine strukturelle Antwort auf sexuelle Übergriffe zu geben. Dazu gehörten die Erstellung und Verbreitung von Inhalten zu diesem Thema, und die Organisation von Konferenzen zum Thema und das Aufbauen von Unterstützungsnetzwerken. In anderen Städten wurden Girl-Gangs mit dem Ziel der Selbstverteidigung und einer Konzentration auf offensive Aktionen gegründet.

Die meisten dieser Aktionen blieben jedoch isoliert und Betroffene sexueller Übergriffe, die versuchten ihre Erlebnisse zu thematisieren, wurden fortwährend ignoriert, stigmatisiert und ausgegrenzt, mit dem Vorwurf, dass sie die Aufmerksamkeit von wichtigeren Themen abziehen und für die interne Spaltung der Gemeinschaft verantwortlich sind.

Als Reaktion darauf haben einige Anarchist:innen daran gearbeitet, ihre Gemeinschaften für das Thema sexueller Übergriffe zu sensibilisieren, insbesondere durch die Entwicklung des Konzepts der Zustimmung. In Zeitschriften, Workshops und Konferenzen wurde darüber diskutiert, wie Menschen unterstützt werden können, die Opfer sexueller Übergriffe geworden sind, was Zustimmung und Sexpositivität bedeutet. In Gruppen kritischer Männlichkeit organisieren sich Männer, um ihr sexistisches Verhalten sowie das in ihrem eigenen Umfeld, zu erkennen und zu verändern. Ein Beispiel ist hier das Kollektiv “Dealing With Our Shit” in Minneapolis im Jahr 2002.

Ein wichtiger Moment war das Pointless Fest 2004 in Philadelphia, bei dem die Organisator:innen öffentlich bekannt gaben, dass drei Frauen während des Festivals vergewaltigt worden waren. Es wurden Gruppen gegründet, um die Betroffenen zu unterstützen und nach Wegen zu suchen, wie die Täter in den Transformationsprozess einbezogen werden können. Von dieser Bewegung existieren heute noch zwei Kollektive (Philly’s Pissed und Philly Stands Up). Ihre Aktivitäten konzentrieren sich auf die Unterstützung von Betroffenen sexueller Übergriffe und die Intervention bei Tätern.

Mit der Zeit und diesen verschiedenen Initiativen wurden sexuelle Übergriffe, Zustimmung, Gerechtigkeit und kollektive Rechenschaftspflicht, bei den meisten anarchistischen Veranstaltungen und Anlässen diskutiert und vertieft. Viele PunkLabels begannen mit der Verteilung von Zines und Broschüren zu diesen Themen, Musikgruppen sprachen auf der Bühne und in ihren Texten darüber, in vielen Städten bildeten sich Gruppen, die sich für die Unterstützung der Betroffenen und transformative Gerechtigkeit einsetzten. Ein Beispiel dafür ist der G20-Gegengipfel in Pittsburgh im Jahr 2009, wo die Organisator:innen von Massenveranstaltungen in ihrer Infrastruktur Awarnessgruppen einbauten. Konzepte wie “Awareness”-Gruppen sollen eine direkte Reaktion auf einen Angriff ermöglichen.

Bis heute werden die Konzepte der Zustimmung, der Unterstützungsgruppen und der transformativen Gerechtigkeit in der nordamerikanischen Anarchist:innen- und Punk-Gemeinde häufig verwendet. Das Sprechen über sexuelle Übergriffe ist freier geworden, viele Fälle von Übergriffen wurden öffentlich gemacht, sowie Experimente, wie eine nicht-institutionelle Antwort auf diese Problematik aussehen könnte. In einigen Fällen wurden Täter, die sexuelle Übergriffe begangen haben in einem langfristigen Prozess der Reflexion über ihre Taten begleitet, andere wurden von den Räumen ausgeschlossen. Broschüren und Konferenzen boten die Möglichkeit, sich kollektiv über verschiedene Erfahrungen von Reaktionen auf Übergriffe sowie ihre Erfolge und Misserfolge auszutauschen, Bücher über transformative Gerechtigkeit wurden veröffentlicht und das Konzept der Zustimmung wurde in andere Länder exportiert und fand in anarchistischen Kreisen weite Verbreitung. Das Kollektiv Philly Stands Up bietet heute an Hochschulen und Universitäten Schulungen zu transformativer Gerechtigkeit an.

Mit diesem Beispiel möchte ich aufzeigen, dass der Ersatz für die Inanspruchnnahme der Polizei und des gesamten Strafsystems, sowie die Einführung neuer Reaktionsformen auf zwischenmenschliche Konflikte und Unterdrückung von denen wir betroffen sind, eine Dynamik in ständiger Bewegung ist. Es geht darum, die Logik der Bestrafung und der Autorität zu verlernen und uns unsere eigene Handlungsmacht wieder anzueignen, ohne Vermittlung von außen. Die Kontrolle über unser Leben zurückzugewinnen, ohne jemandem zu gehorchen oder zu befehlen, ist ein langfristiger Prozess, der uns unser ganzes Leben lang begleitet.

Die Ansätze sind da. Wir müssen sie nur noch aufgreifen und unsere Autonomie auch in der Art und Weise wieder aufbauen, wie wir mit unseren Mitmenschen sowie mit Konflikte umgehen.

Um weiter zu gehen:

- Melanie Brazzel, Was macht uns wirklich sicher? Toolkit für Aktivist_innen, edition assemblage, 2018
- Ending Child Sexual Abuse. Transformative Justice Handbook, Generation Five, 2017
- Zine: Das Risiko wagen, Communities Against Rape and Abuse, 2014
- Ching-In Chen, Jai dulani, Leah Lakshmi PiepznaSamarasinha, The revolution starts at home. Confronting partner abuse in activist communities, 2016
- Sarah Schulman, Conflict Is Not Abuse: Overstating Harm, Community Responsibility, and the Duty of Repair, 2016

[Box]

> **Die Parabel von den fünf Student:innen**
> 
>> Das Strafsystem kann lediglich bestrafen, während es so viele andere mögliche - und meist auch bessere - Arten gibt, auf ein unangenehmes oder schmerzhaftes Ereignis zu reagieren.
> 
> -- Louk Hulsman & Jacqueline Bernat de Celis
> 
> Um zu verdeutlichen, dass es viele verschiedene Reaktionen und Antworten auf ein und denselben Konflikt geben kann, hat der Abolitionist Louk Hulsman[^Louk Hulsman, Critical criminology and the concept of crime, 1986] das Gleichnis von den fünf Student:innen aufgestellt:
>
> Fünf Student:innen leben in einer Wohngemeinschaft. Eines Tages stürzt sich eine:r von ihnen auf den Fernseher und zerschlägt ihn sowie einige Teller. Die Mitbewohner:innen reagieren unterschiedlich auf das Ereignis: Eine:r ist wütend, will nicht mehr mit ihm zusammenleben und schlägt vor, ihn aus der WG auszuschließen. Ein:e Andere:r schlägt vor, dass er:sie die Sachen, die er:sie kaputt gemacht hat, ersetzen soll. Die dritte Person schlägt eine medizinische Lösung vor, da er:sie glaubt, dass ihr: Freund:in krank ist. Die letzte Person schlägt vor, dass alle gemeinsam tiefer darüber nachdenken, was in ihrer WG nicht funktioniert und warum es so weit gekommen ist. Es gibt also mehrere Lösungsvorschläge für denselben Konflikt. Die strafende, die kompensatorische, die therapeutische und die versöhnliche Lösung. In Wahrheit werden die meisten zwischenmenschlichen Konflikte außerhalb des Strafsystems gelöst, durch Vereinbarungen, Vermittlungen und private Entscheidungen zwischen den Betroffenen.

# Ein paar abschließende Worte, Aufruf zur Verbreitung

Dieses Buch ist als Werkzeug zur Selbstverteidigung gedacht. Damit wir kollektiv und individuell lernen können, uns vor der Polizei zu schützen, bis wir diese Institution auf den Müllhaufen der Geschichte werfen.

Anfangs wurde der Inhalt mündlich in Workshops und auf Konferenzen weitergegeben. Durch das Schreiben hoffe ich nun, ein breiteres Publikum zu erreichen. Aber es gibt noch viele andere Übertragungswege, die es sich zu erforschen lohnt: Podcast, VideoTutorials, Comics, Hörbuch usw. Wenn Träger:innen solcher Projekte daran interessiert sind, diese Inhalte zu übernehmen, nehmt doch bitte Kontakt mit mir oder dem Verlag Projet-Evasions auf. Gerne könnt ihr uns auch Kontaktieren um ein Workshop oder Vorstellung des Buches zu organisieren.

Um die Reichweite dieses Buches zu vergrößern, rufen wir alle motivierten Personen auf, sich an der Übersetzung zu beteiligen. Alle Sprachen sind willkommen.

Bei Interesse,

Feedback oder Kritik an dem Inhalt: `saynothing@riseup.net`.

Um das Buch in gedruckter Form zu bestellen oder sich an seiner Verbreitung zu beteiligen: Projet-evasions.org / `evasions@riseup.net`.

## Glossar

Abolitionismus: Der Abolitionismus ist im kriminalsoziologischen Sinne ein Ansatz, der den Verzicht auf die totale Institution des Gefängnisses, der Polizei, oder – in einem noch umfassenderen Sinne – die Abschaffung des Strafrechts fordert. Er hat seine Ursprünge in der Bewegung zur Abschaffung der Sklaverei.

Alibi: Ein Mittel zur Verteidigung einer Person, das beweist, dass sie sich zum besagten Zeitpunkt an einem anderen Ort als dem Tatort eines Verbrechens oder einer Straftat befunden hat.

Straftat: Im Recht stellt ein Verbrechen einen Verstoß gegen Gesetze dar, das mit einer strafrechtlichen Strafe geahndet wird.

Delikt: Synonym für Verbrechen. In einigen Rechtsordnungen kann es je nach Schwere und Strafmaß eine leichte Nuance geben.

Forensik: Der Begriff Forensik umfasst die Gesamtheit der verschiedenen Analysemethoden, die sich auf die Naturwissenschaften stützen (Chemie, Physik, Biologie, Neurowissenschaften, Informatik, Mathematik, Bildgebung, Statistik, Psychologie), um der Arbeit der Polizei und der Sicherheitsmaßnahmen zu dienen.

Informant:in: Person, die aufgrund ihrer Zugehörigkeit zu einer ethnischen, sprachlichen Gemeinschaft oder sozialen Gruppe ausgewählt wird, um Informationen an eine:n Ermittler:in weiterzugeben. Informant:innen werden also von der Polizei rekrutiert, sind jedoch keine Mitglieder der Polizei. Das Gehalt oder die Vergünstigungen, die sie für ihre Arbeit erhalten, ist abhängig von der Rechtsprechung und kann aus Geld oder Strafmilderung bestehen.

Verhör: Der Begriff “Verhör” bezeichnet die Fragen, die einer:m Verdächtigen oder Beschuldigten gestellt werden, und die Antworten, die er darauf gibt.

Modus Operandi: Im polizeilichen Umfeld besteht der Modus Operandi aus einer detaillierten Beschreibung der Handlungen, die zur Begehung eines Verbrechens erforderlich sind. Der Vergleich von Vorgehensweisen kann ein Indiz dafür sein, dass zwei Verbrechen von derselben Person oder denselben Personen begangen wurden.

Hausdurchsuchung: Im Rahmen einer Ermittlung besteht die Hausdurchsuchung darin, dass Polizist:innen einen privaten Ort nach Beweisen durchsuchen.

Verdächtigte:r: Person, die von der Polizei verdächtigt wird, ein Verbrechen begangen zu haben.

Strafsystem: Die Gesamtheit der Institutionen (Polizei, Justiz, Gerichte, Gefängnisse), die dafür zuständig sind, das zu bestrafen, was im Strafrecht als Straftat bezeichnet wird (Ordnungswidrigkeiten, Vergehen und Verbrechen

## Ressourcen

### Polizei

- Ekman P., Ich weiss, dass du lügst, Rowohlt Taschenbuch, Berlin, 2011
- Diane Boszormenyi, Der Einfluss polizeilicher Verhörtechniken auf den Wert eines Geständnisses. Studie im Lichte der Theorie der drei Dimensionen der öffentlichen Gewalt von Monjardet, Rechts- und Kriminologiefakultät, katholische Universität Louvain, 2019
- Kollaborative Forschung Racial Profiling, Racial Profiling, 2019

### Folter & Gewalt

- Coco Fusco, A Field Guide for Female Interrogators, 2008
- Kubark, Der CIA Folterbericht - Der offizielle Bericht des US-Senats zum Internierungs- und Verhörprogramm der CIA, Westend Verlag, 2015
- Laila Abdul-Rahman, Hannah Espín Grau, Luise Klaus, Tobias Singelnstein, Zwischenbericht zum Forschungsprojekt “Körperverletzung im Amt durch Polizeibeamt\*innen”, 2020

### Knasterfahrung

- Thomas Braven, Vogelperspektiven, paranoia City, 1989
- Assoziation A, Wege durch den Knast, 2022
- Anarchist Black Cross Dresden, Ihar Alinevich: Auf dem Weg nach Magadan, 2019
- Andreas Krebs Salih, Der Taifun, Erinnerungen eines Rebellen, 2020
- Georges Jackson, In die Herzen ein Feuer, Bertelsmann, 1980
- Zeitschrift “Gefangenen Infos” Marco Camenisch, Lebenslänglich im Widerstand, Kurt Brandenberger, 2015
- Juan José Garfia, Adios Prison – Die Geschichte einiger spektakulärer Ausbrüche, München 2019

### Anleitung zum Briefe schreiben

- https://www.abc-berlin.net/briefe
- https://mollysbg.noblogs.org/zine

### Liste von Gefangenen

- abc-wien.net
- solidarity.international
- political-prisoners.net

### Verhör in der Litterature

- Klaus Kordon, Krokodil im Nacken, Beltz & Gelberg, 2008
- Vladimir Volkoff, Das Verhör, Fahren 1990

### Justiz & Selbstverteidigung

- Elsa Dorlin, Selbstverteidigung: Eine Philosophie der Gewalt, Suhrkamp Verlag, 2020
- Michel Foucault, Überwachen und Strafen, Suhrkamp Verlag, 1975

## Transformative Gerechtigkeit und überwindung der Polizei

- Melanie Brazzel, Was macht uns wirklich sicher? Toolkit für Aktivist:innen. edition assemblage, 2018
- Generation Five, Ending Child Sexual Abuse, Transformative Justice Handbook, 2017
- Zine: Das Risiko wagen, Communities Against Rape and Abuse, 2014
- Ching-In Chen, Jai dulani, Leah Lakshmi Piepzna-Samarasinha, The revolution starts at home. Confronting partner abuse in activist communities, 2018
- Sarah Schulman, Conflict Is Not Abuse: Overstating Harm, Community Responsibility, and the Duty of Repair, 2016

# Rückseite

Ein Verhör ist kein harmonischer Austausch zwischen zwei Menschen. Es ist ein Konflikt.

In diesem Konflikt macht unsere Unwissenheit ihre Stärke aus. Unwissenheit darüber, wie die Polizei arbeitet, Unwissenheit über die zur Anwendung kommenden Manipulationsstrategien, Unwissenheit über den juristischen Rahmen und schlussendlich, die Unwissenheit über unsere Verteidigungsmöglichkeiten.

Als Antwort auf diese Feststellung ist dieses Buch als ein Werkzeug zur Selbstverteidigung gegen die polizeiliche Verhörpraxis gedacht.

Projet Evasions projet-evasions.org evasions@riseup.net ISBN 978-2-8399-3629-3

