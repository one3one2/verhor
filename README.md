# What's this?

A markdown formatted German version of “How the police interrogate and how to defend against it”.

„Wie die Polizei verhört und wie wir uns dagegen verteidigen können“ <https://projet-evasions.org/verhor/>

Since this was a lot of work I am sure I won't do this again for following/updated/revised versions, as it is only a partly automated process.

I think this is fine because people should read the whole thing anyways and the snippets/excerpts/quotes are only there to remind people of that. I honestly don't know how the impact of the missing context will be but I'll do my best to include disclaimers, trigger warnings and tags.

## Status

- I formatted the whole thing and I guess it is pretty useable
- I added an initial `book.yaml` file as an example how the datastructure could look like in the end. A snippet can span multiple pages, so pages should be a list. Also each snippet should have a slug or checksum and a sequence number
- I added an html version by using `pdf2htmlEX` from here: <https://github.com/pdf2htmlEX/pdf2htmlEX/releases> that way i can link to pages :D and the layouting stays the same. Also this is easily done for future versions/updates, perhaps I can update the page numbers in `book.yaml` automatically as well
- maybe derive tags from the glossary?
- check if you need a trigger/content warning for each snippet and place it accordingly

## Overview

i found some markdown replacements for the pdf formatting

- for a graphic i used ascii art as a placeholder
- i used labels for media types/annotations, like: `[Buch] [Film] [Polizei-Zitat]`
- i used blockquotes for `[Situationsbeschreibung]`
- it is not quite clear how `footnotes[^like this]` formatted like this: footnoes[^like this]
  will behave, if they contain a square bracket `footnotes[^like [this]]` (codeberg/gitea doesn't render footnotes it seems)
- also there is a footnote in a blockquote because it was added to explain a naming choice
- this is how a basic yaml entry looks (content is a string, pages is a list of ints, while the others are lists of strings):
```
- content: ""
  pages: []
  labels: ["Box", "Film", "Buch", "Polizei-Zitat", "Situationsbeschreibung"]
  glossary: ["Abolitionismus", "Alibi", "Straftat", "Delikt", "Forensik", "Informant", "Verhör", "Modus Operandi", "Hausdurchsuchung", "Verdächtigte", "Strafsystem"]
  tags: [""]
  chapter: ["vor", "während", "drumerhum", "verteidigen", "überwinden", "anhang"]
  warnings: ["spoiler", "sexual", "torture", "violence"]
```


## Mistakes

- some mistakes I found are in ./mistakes as screenshots
- some of the quotation marks are wrong, at least for German
- some other mistake i forgot but it was a minor thing (I am planning to read this more thorough after i finished formatting)

# Disclaimer

This project is currently work in progress.

I am not a lawyer and this is not legal advice. Of course the original license still applies. I am not one of the authors. 

I contacted the authors at `evasions@@riseup.net` and they are fine with creative ways of redistribution. Thanks for that and the awesome work :)

